﻿using UnityEngine;
using System.Collections;

public static class DataMessages {

    public const string SERVER_RESPONSE_FAIL = "No hubo respuesta del servidor, ¿Reintentar?";
    public const string SERVER_DOWNLOAD_FAIL = "No se lograron descargar los archivos necesarios, ¿Reintentar?";

    public const string DEVICE_DETAILS_NO_COSTO = "Sin pago inicial";
    public const string DEVICE_DETAILS_MONTHS = "meses";
    public const string GENERIC_COMING_SOON = "Sección en construcción";
    public const string GENERIC_PLAN_EXPIRATION_VALUE_A = "Vigencia de: ";
    public const string GENERIC_PLAN_EXPIRATION_VALUE_B = " días";
    public const string GENERIC_PLAN_MB_LABEL_B = " MB para navegar";

	public const string GENERIC_CLAUSE = "*Precios sujetos a cambios sin previo aviso \n * Imágenes ilustrativas. Equipos y colores sujetos a disponibilidad en punto de venta.";
	public const string PROMOTION_MB_PLAN = "";
	public const string PROMOTION_MB_AMIGO = "";
	public const string PROMOTION_SMS_PYME = "";
	public const string UBER_GENERIC_CLAUSE = "*El uso de la aplicación UBER (Conductor y Usuario) es dentro de México";
	public const string GENERIC_PROMOTION = "*Promoción platino con cargo mensual por servicio al renovar con un plazo mínimo de contratación de 24 ó 36 meses \n";
#if DEUR
    public const string DEVICE_DETAILS_CONTINUE_LABEL = "Ir a planes >";

    public const string AMIGO_SIN_LIMITE_CLAUSE_LOW = "*Precios sujetos a cambios sin previo aviso\n" +
													  "*Tarifa de $0.85 por minuto, SMS o MB adicional\n" +
                                                      "*Redes Sociales para uso nacional, sin costo de navegación los primeros 500 MB";

    public const string AMIGO_SIN_LIMITE_CLAUSE_HIGH = "*Precios sujetos a cambios sin previo aviso\n" +
														"*Tarifa de $0.85 por minuto, SMS o MB adicional\n" +
                                                        "*Redes Sociales para uso nacional, sin costo de navegación los primeros 1000 MB";
#elif R9
    public const string DEVICE_DETAILS_CONTINUE_LABEL = "IR A ESQUEMAS DE COBRO";
	public const string DEVICE_DETAILS_COTINUE_PLAN_LABEL = "IR A RESUMEN";

    public const string AMIGO_SIN_LIMITE_CLAUSE_LOW = "*Precios sujetos a cambios sin previo aviso\n" +
		"*Tarifa de $0.75 por minuto y $0.85 por SMS o MB adicional\n" +
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros 500 MB\n";

    public const string AMIGO_SIN_LIMITE_CLAUSE_HIGH = "*Precios sujetos a cambios sin previo aviso\n" +
		"*Tarifa de $0.75 por minuto y $0.85 por SMS o MB adicional\n" +
                                                      "*Redes Sociales para uso nacional, sin costo de navegación los primeros 1000 MB";
	public const string MAX_SIN_LIMITE_CLAUSE =         "*Precios sujetos a cambios sin previo aviso\n" +
														"*Minutos SMS, MB y WhatsApp para uso en México, E.U.A y Canadá\n" +
													    "*Redes Sociales para uso nacional";


	//#
	public const string MAX_SIN_LIMITE_MB_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A y Canadá\n"+
	"*Redes Sociales para uso nacional \n"+
	"*Redes sociales sin costo los primeros {MB_DATA} \n"+
	"*Costo MB adicional a $0.25";

	public const string MAX_SIN_LIMITE_MB_SC_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" +
		"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A y Canadá\n"+
		"*Redes Sociales para uso nacional \n"+
		"*Redes Sociales sin costo de navegacion \n"+
		"*Costo MB adicional a $0.25";

	//#
	public const string MAX_SIN_LIMITE_RESUME_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A y Canadá\n" +
	"*Redes Sociales para uso nacional";



	//#
	public const string AMIGO_FACIL_SIN_LIMITE_RECARGA_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" + 
	"*Tarifa de $0.75 por minuto y $0.85 por SMS o MB adicional\n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A. y Canadá\n" +
	"*Incluye 3 Números gratis para hablar o mensajear sin costo en México, E.U.A. y Canadá \n" +
	"{MB_DATA}";


	//# 
	//ojo
	public const string AMIGO_FACIL_SIN_LIMITE_RESUME_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" + 
	"*Tarifa de $0.75 por minuto y $0.85 por SMS o MB adicional \n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A. y Canadá \n" +
	"*Incluye 3 Números gratis para hablar o mensajear sin costo en México, E.U.A. y Canadá  \n" +
	"{MB_DATA}\n"+
	"*Sujeto a aprobación\n"+
	"*Aplican términos y condiciones";


	public const string AMIGO_KIT_SIN_LIMITE_RECARGA_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" + 
	"*Tarifa de $0.75 por minuto y $0.85 por SMS o MB adicional\n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A. y Canadá\n" +
	"*Incluye 3 Números gratis para hablar o mensajear sin costo en México, E.U.A. y Canadá \n" +
	"{MB_DATA}";

	//OJo 2
	public const string AMIGO_KIT_SIN_LIMITE_RESUME_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" + 
	"*Tarifa de $0.75 por minuto y $0.85 por SMS o MB adicional\n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A. y Canadá\n" +
	"*Incluye 3 Números gratis para hablar o mensajear sin costo en México, E.U.A. y Canadá \n" +
	"{MB_DATA}";


	//#
	public const string AMIGO_KIT_SIN_LIMITE_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" +
	"*Las tarifas de los minutos, SMS, MB cuestan igual en México E.U.A. y Canadá \n" +
	"*Todas las recargas ofrecen saldo promocional que se puede usar en los tres países\n" +
	"*Incluye 5 números (Nacionales) gratis para hablar (hasta 5 minutos) o mensajear sin costo en México, E.U.A. y Canadá\n" +
	"*Incluye 9 números frecuentes (nacionales) Todo destino con una tarifa de $0.98 México, E.U.A. y Canadá.\n" +
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros {MB_DATA} MB";


	//#
	public const string AMIGO_FACIL_SIN_FRONTERA_RECARGA_CLAUSE = "*Precios sujetos a cambios sin previo aviso \n"+
	"*Las tarifas de los minutos, SMS, MB cuestan igual en México E.U.A. y Canadá \n"+
	"*Todas las recargas ofrecen saldo promocional que se puede usar en los tres países \n"+
	"*Incluye 5 números (Nacionales) gratis para hablar (hasta 5 minutos) o mensajear sin costo en México, E.U.A. y Canadá \n"+
	"*Incluye 9 números frecuentes (nacionales) Todo destino con una tarifa de $0.98 México, E.U.A. y Canadá.\n"+
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros {MB_DATA} MB";

	//#
	public const string AMIGO_FACIL_SIN_FRONTERA_CLAUSE = "*Precios sujetos a cambios sin previo aviso \n"+
	"*Las tarifas de los minutos, SMS, MB cuestan igual en México E.U.A. y Canadá \n"+
	"*Todas las recargas ofrecen saldo promocional que se puede usar en los tres países \n"+
	"*Incluye 5 números (Nacionales) gratis para hablar (hasta 5 minutos) o mensajear sin costo en México, E.U.A. y Canadá \n"+
	"*Incluye 9 números frecuentes (nacionales) Todo destino con una tarifa de $0.98 México, E.U.A. y Canadá.\n"+
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros 1,000 MB";

	//#
	public const string AMIGO_FACIL_SIN_FRONTERA_RESUME_CLAUSE = "*Precios sujetos a cambios sin previo aviso \n"+
	"*Las tarifas de los minutos, SMS, MB cuestan igual en México E.U.A. y Canadá\n"+
	"*Todas las recargas ofrecen saldo promocional que se puede usar en los tres países\n"+
	"*Incluye 5 números (Nacionales) gratis para hablar (hasta 5 minutos) o mensajear sin costo en México, E.U.A. y Canadá\n"+
	"*Incluye 9 números frecuentes (nacionales) Todo destino con una tarifa de $0.98 México, E.U.A. y Canadá.\n"+
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros {MB_DATA} MB\n"+
	"*Sujeto a aprobación\n"+
	"*Aplican términos y condiciones";



	public const string AMIGO_KIT_SIN_FRONTERA_RESUME_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" +
	"*Las tarifas de los minutos, SMS, MB cuestan igual en México E.U.A. y Canadá \n" +
	"*Todas las recargas ofrecen saldo promocional que se puede usar en los tres países\n" +
	"*Incluye 5 números (Nacionales) gratis para hablar (hasta 5 minutos) o mensajear sin costo en México, E.U.A. y Canadá\n" +
	"*Incluye 9 números frecuentes (nacionales) Todo destino con una tarifa de $0.98 México, E.U.A. y Canadá.\n" +
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros {MB_DATA}";


	//#
	public const string AMIGO_KIT_SIN_FRONTERA_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n" +
	"*Las tarifas de los minutos, SMS, MB cuestan igual en México E.U.A. y Canadá \n" +
	"*Todas las recargas ofrecen saldo promocional que se puede usar en los tres países\n" +
	"*Incluye 5 números (Nacionales) gratis para hablar (hasta 5 minutos) o mensajear sin costo en México, E.U.A. y Canadá\n" +
	"*Incluye 9 números frecuentes (nacionales) Todo destino con una tarifa de $0.98 México, E.U.A. y Canadá.\n" +
	"*Redes Sociales para uso nacional, sin costo de navegación los primeros 1,000 MB\n" +
	"*Sujeto a aprobación\n" +
	"*Aplican términos y condiciones";

	//#
	public const string MAX_SIN_LIMITE_PYME_MB_CLAUSE =  "*Precios sujetos a cambios sin previo aviso\n" +
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A y Canadá\n" +
	"*Redes Sociales para uso nacional\n" +
	"*Redes sociales sin costo los primeros {MB_DATA}\n" +
	"*Costo MB adicional a $0.25";

	//#
	public const string MAX_SIN_LIMITE_PYME_RESUME_CLAUSE = "*Precios sujetos a cambios sin previo aviso\n"+
	"*Minutos, SMS, MB y WhatsApp para uso en México, E.U.A y Canadá\n"+
	"*Redes Sociales para uso nacional\n"+
	"*Cargo mensual por equipo ${PAYMENT}/mes\n"+
	"*Costo MB adicional a $0.25";

	public const string INTERNET_HOME_CLAUSE = "*Los MB podrán ser utilizados en una ubicación fija dentro del Territorio Nacional\n"+
	"*Plan Internet en tu casa 100 topado a 100,000 MB\n"+
	"* Plan Internet en tu casa 150 topado a 150,000 MB\n"+
	"*Precios sujetos a cambios sin previo aviso";

	public const string MAX_SIN_LIMITE_EXPANDED_CLAUSE = "*El cargo sólo aplica en caso de realizar una contratación con equipo celular (plazos 12, 18, 24 ó 36 meses). No aplica para planes en Plazo Libre. Si el usuario lleva su “Pago al corriente”  se realizará un descuento por el monto que corresponda al Cargo Mensual por Equipo aplicado. Entiéndase como “ Pago al Corriente” liquidar el 100% del Cargo Mensual Total dentro de los 30 días naturales posteriores a su fecha de corte.";

#endif

}
