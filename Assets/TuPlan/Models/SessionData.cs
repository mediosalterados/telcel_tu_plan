﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Carry all values the user has modified in activities during a Session
/// </summary>
public class SessionData
{

    public enum DeviceMainFilterSelectionValues {
        NO_FILTER = -1,
        LTE = 0,
        SNAPDRAGON = 1,
        NO_ADVANCE_PAYMENT = 2,
		LTE_SNAPDRAGON = 3
    }

    public enum PaymentSchemeSelectionValues {
        NONE = 0,
        PREPAID = 1,
        POSTPAID = 2
    }
			
	public enum PaymentPrepaidSelectionValues {
		NONE = 0,
		AMIGO_FACIL = 1,
		AMIGO_KIT = 2,
	}

	public enum AmigoSchemeSelectionValues {
		NONE = 0,
		AMIGO_SIN_LIMITE = 1,
		AMIGO_OPTIMO_PLUS = 2,
	}

	public enum InitialStepsValues {
		EQUIPO = 0,
		TU_PLAN = 1,
		TU_COSTO = 2
	}
        
    public DeviceMainFilterSelectionValues SelectedDeviceMainFilter { get; set; }

    public string SelectedBrandId { get; set; }
    public string SelectedDeviceId { get; set; }
	public bool byMb { get; set; }

    public PaymentSchemeSelectionValues SelectedPaymentScheme { get; set; }

    public const string AMIGO_SIN_LIMITE_PLAN_TYPE = "AMIGO";
    public const string AMIGO_OPT_PLUS_PLAN_TYPE = "AMIGO_OPT_PLUS";

	public const string AMIGO_FACIL_TYPE = "AMIGO_FACIL";
	public const string AMIGO_KIT_TYPE = "AMIGO_KIT";

    public string SelectedPlanType { get; set; }
	public InitialStepsValues SelectedInitialStep { get; set; }

    public string SelectedPlanId { get; set; }
	public string ShareMessage { get; set; }
	public string ShareEmail { get; set; }
	public PaymentPrepaidSelectionValues SelectedPrepaidPaymentScheme { get; set; }
	public AmigoSchemeSelectionValues SelectedAmigoScheme { get; set; }

}

