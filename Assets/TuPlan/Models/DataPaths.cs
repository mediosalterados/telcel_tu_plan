using UnityEngine;
using System.Collections;
using System.IO;

using TuPlan.Controllers.DataManagement;

public static class DataPaths {


    public const string DB_NAME = "db_r9.sqlite";
    public const string WS_URL = "http://r9.local/";
    public const string WS_API_URL =  WS_URL + "/api";
    public const string WS_HEADER = "TuPlanAuth";
    public const string WS_AUTH = "4cb11fa0cbcc6db89accc1fa43fd4bace77081004ff1af484dc72fa6044c2a097929cefc9065ee971709057a3ca1e71af24234010317774b31a68394c03c56ba";
    public const string WS_BRANDS_URL = WS_API_URL + "/brands";
    public static string WSDevicesURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/devices"; } }
    public static string WSDevicePlansURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/devices_plans/3"; } }
    public const string WS_MOBILEOSS_URL = WS_API_URL + "/os";
    public static string WSPlansURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/plans/3"; } }
    public static string WSPlanTypesURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/plan_type"; } }
    public static string WSPrepaidPlansURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/amigo_recargas"; } }
    public static string WSPrepaidsURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/amigo"; } }
    public static string WSPrepaidsOptPlusURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/amigo_opt_plus_recargas"; } }
    public static string WSAmigoFacilPlansURL { get { return WS_API_URL + "/" + DataManager.Instance.RemoteDataVersion + "/amigo_renta?debug=true"; } }
    public const string WS_PROMOTIONS_URL = WS_API_URL + "/promotions";
    public static string WSAmigoSinLimiteImgURL { get { return WS_URL + "/images/drawable-xxhdpi/plans_button_amigosinlimite.png";  } }
    public static string WSAmigoOptPlusImgURL { get { return WS_URL + "/images/drawable-xhdpi/plans_button_amigo_op_sf.png"; } }
	public static string WSAmigoFacilImgURL { get { return WS_URL + "images/drawable-xxxhdpi/select_plans_button_amigofacil.png"; } }
	public static string WSAmigoKitImgURL { get { return WS_URL + "images/drawable-xxxhdpi/select_plans_button_amigokit.png"; } }


    public static string DatabasePath
    {

        get
        {


#if UNITY_EDITOR

            var dbPath = string.Format(@"Assets/StreamingAssets/localdata/{0}", DB_NAME);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DB_NAME);
		
        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DB_NAME);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
             var loadDb = Application.dataPath + "/Raw/" + DB_NAME;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
			if(File.Exists(loadDb))
            	File.Copy(loadDb, filepath);
			// Set no backup flag to the database.
			UnityEngine.iOS.Device.SetNoBackupFlag (filepath);
#elif UNITY_WP8
            var loadDb = Application.dataPath + "/StreamingAssets/" + DB_NAME;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DB_NAME;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DB_NAME;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }


            var dbPath = filepath;
#endif
            return dbPath;
        }

    }

    public static string DataPath
    {
#if DEUR
        get { return "/localdata/deur/assets"; }
#elif R9
        get { return "/localdata/r9/assets"; }
#endif
    }

    public static string BrandsAssetsPath {
        get { return DataPath + "/brands"; }
    }

    public static string DevicesAssetsPath
    {
        get { return DataPath + "/devices"; }
    }

    public static string MobileOSsAssetsPath {
        get { return DataPath + "/mobile_oss"; }
    }

    public static string PlanTypesAssetsPath
    {
        get { return DataPath + "/plan_types"; }
    }

    public static string PromotionsAssetsPath
    {
        get { return DataPath + "/promotions"; }
    }
	public static string OffersPdfPath
	{
		get { return DataPath + "/offers"; }
	}

	public const bool ACTIVITY_RELATED_DEVICES_ENABLED = true;
	public const bool ACTIVITY_EXTRA_FEATURES_ENABLED = true;

    public const string ACTIVITY_HOME_LAYOUT_PATH = "UI/Layouts/Activities/Home";
	public const string ACTIVITY_PASSCODE_LAYOUT_PATH = "UI/Layouts/Activities/Passcode";
	public const string ACTIVITY_INFO_LAYOUT_PATH = "UI/Layouts/Activities/Info";

	public const string ACTIVITY_QUALCOM_LAYOUT_PATH = "UI/Layouts/Activities/Qualcom";
	public const string ACTIVITY_PROMOTIONS_LAYOUT_PATH = "UI/Layouts/Activities/PromotionsScreen";
	public const string ACTIVITY_PROMOTIONS_PAGE_LAYOUT_PATH = "UI/Layouts/Fragments/SliderPage";

    public const string ACTIVITY_DEVICEMAINFILTER_LAYOUT_PATH = "UI/Layouts/Activities/DeviceMainFilter";
    public const string ACTIVITY_DEVICEBRANDFILTER_LAYOUT_PATH = "UI/Layouts/Activities/DeviceBrandFilter";
    public const string ACTIVITY_DEVICESELECTION_LAYOUT_PATH = "UI/Layouts/Activities/DeviceSelection";
    public const string ACTIVITY_DEVICEDETAILS_LAYOUT_PATH = "UI/Layouts/Activities/DeviceDetails";
    public const string ACTIVITY_PLANSELECTION_LAYOUT_PATH = "UI/Layouts/Activities/PlanSelection";
    public const string ACTIVITY_PAYMENTSCHEMESELECTION_LAYOUT_PATH = "UI/Layouts/Activities/PaymentSchemeSelection";
    public const string ACTIVITY_AMIGOSINLIMITE_QUOTATIONRESULT_LAYOUT_PATH = "UI/Layouts/Activities/AmigoSinLimiteQuotationResult";
	public const string ACTIVITY_AMIGOFACIL_QUOTATIONRESULT_LAYOUT_PATH = "UI/Layouts/Activities/AmigoFacilQuotationResult";
	public const string ACTIVITY_MAXSINLIMITE_PLANADJUSTMENT_LAYOUT_PATH = "UI/Layouts/Activities/MaxSinLimitePlanAdjustment";
	public const string ACTIVITY_AMIGO_OPTIMO_PLANADJUSTMENT_LAYOUT_PATH = "UI/Layouts/Activities/AmigoOptimoAdjustment";
	public const string ACTIVITY_OFFERS_LAYOUT_PATH = "UI/Layouts/Activities/Offers";
	public const string ACTIVITY_FULLIMAGE_LAYOUT_PATH = "UI/Layouts/Activities/FullImage";


    public const string FRAGMENT_MAINMENU_LAYOUT_PATH = "UI/Layouts/Fragments/MainMenu";
	public const string FRAGMENT_SHARE_MENU_LAYOUT_PATH = "UI/Layouts/Fragments/ShareMenuOptions";
    public const string FRAGMENT_SIDEMENU_LAYOUT_PATH = "UI/Layouts/Fragments/SideMenu";
	public const string FRAGMENT_LEGALES_EXPANDED_LAYOUT_PATH = "UI/Layouts/Fragments/LegalesExpanded";
    public const string FRAGMENT_GRIMOIRECUSTOMNOTIFICATION_LAYOUT_PATH = "UI/Layouts/Fragments/GrimoireCustomNotification";
	public const string FRAGMENT_PROMOTIONS_PAGE_ROW_PATH = "UI/Layouts/Fragments/PageRow";
	public const string FRAGMENT_SEPARATOR = "UI/Layouts/Fragments/SeparatorImage";

	public const string ELEMENT_DEVICE_RELATED_LAYOUT_PATH = "UI/Layouts/Elements/RelatedDevice";
    public const string ELEMENT_BRANDENTRY_LAYOUT_PATH = "UI/Layouts/Elements/BrandEntry";
    public const string ELEMENT_DEVICEENTRY_LAYOUT_PATH = "UI/Layouts/Elements/DeviceEntry";
    public const string ELEMENT_PLANENTRY_LAYOUT_PATH = "UI/Layouts/Elements/PlanEntry";


}
