﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;

namespace TuPlan.Models.Database.Tables
{

    public class TableTest
    {

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("[AugmentableEntity: ID={0}, Name={1}]",
                ID.ToString(),
                Name
            );

        }

    }

}