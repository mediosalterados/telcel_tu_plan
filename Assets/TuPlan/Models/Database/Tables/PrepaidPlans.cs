﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrepaidPlans
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public int id_amigo_recarga { get; set; }
#if R9
    public string prepaid_type { get; set; }
#endif
    public string nombre_amigo_recarga { get; set; }
    public int minutos { get; set; }
    public int sms { get; set; }
    public int megabytes { get; set; }
    public int vigencia { get; set; }
    public string recarga { get; set; }
    //public int version_id { get; set; }
}

