﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Promotions
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public int promotion_id { get; set; }
    public string title { get; set; }
    public string subtitle { get; set; }
    public string detail { get; set; }
    public string button_text { get; set; }
    public string button_url { get; set; }
    public string button_url_filepath { get; set; }
    public string promotion_img { get; set; }
    public string promotion_img_filepath { get; set; }
	public string offer_pdf { get; set; }
	public string offer_pdf_filepath { get; set; }
    //public List<PromotionImage> promotion_img { get; set; }
}

