﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*public class BrandImage {
    public string type { get; set; }
    public string normal { get; set; }
    public string hover { get; set; }
}*/

public class Brands
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public string brand_id { get; set; }
    public string brand_name { get; set; }
    //public BrandImage brand_img { get; set; }
    public string normal_image_url { get; set; }
    public string normal_image_filepath { get; set; }
    public string hover_image_url { get; set; }
    public string hover_image_filepath { get; set; }
    public bool visible { get; set; }
}

