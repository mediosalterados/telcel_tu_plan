﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmigoFacilPlans
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    
    public int _id_reg { get; set; }
    public string anticipo { get; set; }
    public string renta_mensual { get; set; }
    public string renta_anual { get; set; }
    public string kit_credito { get; set; }
    public string id_device { get; set; }
    public int version_id { get; set; }
   
}

