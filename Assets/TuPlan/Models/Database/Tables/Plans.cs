﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plans
{

    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public string plan_id { get; set; }
    public string plan_name { get; set; }
    public string monthly_cost { get; set; }
    public int minutes_inc { get; set; }
    public int free_numbers { get; set; }
    public int sms_inc { get; set; }
    public int megabytes_inc { get; set; }
    public string megabytes_extra_cost { get; set; }
    public string minutes_extra_cost { get; set; }
    public string sms_extra_cost { get; set; }
    public bool social_networks { get; set; }
    public string additional_cost { get; set; }
#if R9
    public string additional_cost_iphone { get; set; }
#endif
    public string sin_frontera { get; set; }
    public int version_id { get; set; }
    public string tipo_plan { get; set; }
	public string promotion { get; set; }
	public string legals { get; set; }
    
}
