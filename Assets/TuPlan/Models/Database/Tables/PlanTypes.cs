﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*public class PlanTypeImage
{
    public string resolution_id { get; set; }
    public string location { get; set; }
}*/

public class PlanTypes
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public string nombre_plan { get; set; }
    public string type_plan { get; set; }
    public bool eu_canada { get; set; }
    public string prioridad { get; set; }
    public string plan_img { get; set; }
    public string plan_img_filepath { get; set; }
}

