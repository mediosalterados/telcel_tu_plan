﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Prepaids
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public int _id_reg_amigo { get; set; }
    public int free_numbers { get; set; }
    public string minute_carrier_cost { get; set; }
    public string minute_others_cost { get; set; }
    public string minute_world_cost { get; set; }
    public string minute_land_cost { get; set; }
    public string megabyte_cost { get; set; }
    public string sms_cost { get; set; }
    public int frequent_numbers { get; set; }
    public string cost_frequent_numbers { get; set; }
#if R9
    public string prepaid_name { get; set; }
    public string prepaid_type { get; set; }
    public string value { get; set; }
#endif
    //public int version_id { get; set; }
    //public string fecha { get; set; }

}

