﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*public class MobileOSImage
{
    public string type { get; set; }
    public string location { get; set; }
}*/

public class MobileOSs
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public string os_id { get; set; }
    public string os_name { get; set; }
    public string os_version { get; set; }
    public string os_img { get; set; }
    public string os_img_filepath { get; set; }
    //public List<MobileOSImage> os_img { get; set; }
}

