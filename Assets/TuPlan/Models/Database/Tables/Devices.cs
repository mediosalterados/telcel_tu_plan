﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*public class DeviceImage {
    public string type { get; set; }
    public string location { get; set; }
}*/

public class Devices  {

    /*DEUR
    private static final String CREATE_DEVICES_TABLE = "CREATE TABLE " + DEVICES_TABLE + " ("
            + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "device_id TEXT, "
            + "brand_id TEXT, "
            + "os_id TEXT, "
            + "model TEXT, "
            + "name TEXT, "
            + "cpu TEXT, "
            + "snapdragon INTEGER, "
            + "lte INTEGER, "
            + "cores INTEGER, "
            + "speed TEXT, "
            + "ram TEXT, "
            + "storage TEXT, "
            + "back_camera TEXT, "
            + "front_camera TEXT, "
            + "display TEXT, "
            + "url_image TEXT, "
            + "local_image TEXT, 
            + "visible INTEGER, "
            + "submodel_cpu TEXT)";

    */

    /* R9
    public static final String CREATE_DEVICES_TABLE = "CREATE TABLE " + DEVICES_TABLE + " ("
            + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
           *** + "device_id_on_server INTEGER, "
            + "device_id TEXT, "
            + "brand_id TEXT, "
            + "os_id TEXT, "
            + "model TEXT, "
            + "name TEXT, "
            + "cpu TEXT, "
            + "snapdragon INTEGER, "
            + "lte INTEGER, "
            + "cores INTEGER, "
            + "speed TEXT, "
            + "ram TEXT, "
            + "storage TEXT, "
            + "back_camera TEXT, "
            + "front_camera TEXT, "
            + "display TEXT, "
            + "url_image TEXT, "
            + "local_image TEXT, "
            + "visible INTEGER, "
			+ "submodel_cpu TEXT)";
    */

    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public int _id { get; set; }
#if DEUR
    public int device_id { get; set; }
#elif R9
    public string device_id { get; set; }
#endif
    public string brand_id { get; set; }
    public string os_id { get; set; }
    public string model_name { get; set; }
    public string device_name { get; set; }
    public string cpu { get; set; }
    public bool snapdragon { get; set; }
    public int cores { get; set; }
	public string submodel_cpu { get; set; }
    public string speed { get; set; }
    public string ram { get; set; }
    public string storage { get; set; }
	public int storage_int { get; set; }
    public string back_camera { get; set; }
    public string front_camera { get; set; }
	public int back_camera_float { get; set; }
	public int front_camera_float { get; set; }
    public string display { get; set; }
	public int display_int { get; set; }
	public string description { get; set; }
    public int prepaid_cost { get; set; }
    //public List<DeviceImage> device_img { get; set; }
    public string img_url { get; set; }
    public string img_filepath { get; set; }
    public bool lte { get; set; }
    public int version_id { get; set; }
	public int position { get; set; }
	public string network { get; set; }
    
}
