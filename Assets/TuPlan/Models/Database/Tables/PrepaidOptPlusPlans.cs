﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrepaidOptPlusPlans
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public int _id_reg { get; set; }
    public int recarga_saldo { get; set; }
    public int saldo_promocional { get; set; }
    public int saldo_total { get; set; }
    public bool redes_sociales { get; set; }
    public bool whatsapp { get; set; }
    public int vigencia_saldo { get; set; }
    public int vigencia_redes_sociales { get; set; }
    // ID de la tarifa
    public int rate_id { get; set; }   
    //public int version_id { get; set; }
}

