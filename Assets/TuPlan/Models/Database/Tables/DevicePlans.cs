﻿using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DevicePlans
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public string device_id { get; set; }
    public string plan_id { get; set; }
    public int months { get; set; }
    public string cost { get; set; }
    public string additional_cost { get; set; }
    public int version_id { get; set; }
    
}

