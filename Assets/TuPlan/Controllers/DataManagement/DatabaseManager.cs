using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GLIB.Core;
using GLIB.Net;
using System;
using TuPlan.Models.Database.Tables;
using SQLite4Unity3d;
using LitJson;
using System.Text.RegularExpressions;
using GLIB.Utils;
using System.Globalization;

using System.IO;
using System.Text;

namespace TuPlan.Controllers.DataManagement
{

#if OLD
    public class NoUpdatesAvailableException : Exception
    {

        public NoUpdatesAvailableException() : base()
        {
            
        }

        public NoUpdatesAvailableException(string message) : base(message)
        {

        }

    }

    sealed class RemoteDatabaseResponse {

        public int ErrorType { get; set; }

        public string Message { get; set; }

        // TODO implement Data here

    }

#region Remote data classes
        
    sealed class RemoteDatabase {

        List<TableTest> _tableTest;
        public List<TableTest> TableTest { get { if (_tableTest == null) _tableTest = new List<TableTest>(); return _tableTest; } set { _tableTest = value; } }
              

    }

#endregion

    sealed class UpdateDates {

        public string UpdateDate { get; set; }

    }
#endif

    public class DatabaseManager : BackModule<DatabaseManager>
    {

       
        public SQLiteConnection localDBConnection {get { return _mainSQLiteConnection; }}
        SQLiteConnection _mainSQLiteConnection;

        List<SQLiteConnection> _sqliteConnections;

        string _lastUpdateDate;

        string _webRequest = "";
        
        /// <summary>
        /// Please use this to include tables
        /// </summary>
        List<Type> _tables {
            
            get
            {
                List<Type> tables = new List<Type>();

#if DEUR || R9
                tables.Add(typeof(Brands));
                tables.Add(typeof(Devices));
                tables.Add(typeof(DevicePlans));
                tables.Add(typeof(MobileOSs));
                tables.Add(typeof(Plans));
                tables.Add(typeof(PlanTypes));
                tables.Add(typeof(Prepaids));
                tables.Add(typeof(PrepaidPlans));
                tables.Add(typeof(Promotions));
#endif
#if R9                
                tables.Add(typeof(PrepaidOptPlusPlans));
                tables.Add(typeof(AmigoFacilPlans));
#endif

                return tables;      
                          
            }

        }

        protected override void ProcessInitialization()
        {

            _sqliteConnections = new List<SQLiteConnection>();


        }

        protected override void ProcessUpdate()
        {

        }

        protected override void ProcessTermination()
        {
            for (int s = 0; s < _sqliteConnections.Count; s++)
            {
                _sqliteConnections[s].Close();
                _sqliteConnections[s] = null;
                
            }

            _sqliteConnections.Clear();
            _sqliteConnections = null;
            
        }

		public void UpdateDB(SQLiteConnection sqliteConnection)
		{
			Debug.Log("Database => " + "Updating Database");
			this.CreateDB (sqliteConnection);
		}

        public void CreateDB(SQLiteConnection sqliteConnection)
        {
            Debug.Log("Database => " + "Creating Database");

            foreach (Type table in _tables) {
                sqliteConnection.CreateTable(table);
                Debug.Log("Database => " + "Creating Table " + table.Name);
            }
            
            Debug.Log("Database => " + "Tables have been created");
        }
                
        public bool DatabaseCheck(SQLiteConnection sqliteConnection) {

            Debug.Log("Database => Checking database consistency.");

            bool tableMissing = false;

            foreach (Type tableName in _tables) {

                var tb = sqliteConnection.GetTableInfo(tableName.Name);

                if (tb.Count == 0)
                {
                    tableMissing = true;
                    break;
                }
            }

            if (tableMissing)
                return false;
            else
                return true;

        }

        public void LoadLocalDB() {

            if (!isRunning) {
                Debug.LogError("Database not initialized, please run Database.Instace.Initialize()");
                return;
            }
            
            if (_mainSQLiteConnection == null) {


                _mainSQLiteConnection = new SQLiteConnection(DataPaths.DatabasePath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);

                _sqliteConnections.Add(_mainSQLiteConnection);
            }

            Debug.Log("Database => " + "Database loaded successfully");
                        

        }

#if OLD
        public void UpdateDB(SQLiteConnection sqliteConnection, Action onDBUpdate = null, Action onDBUpdateFail = null) {

            if (sqliteConnection == null) {
                Debug.LogError("Null reference when accesing sqliteConnection argument, aborting update.");
                return;
            }

            // Start updating Tables.
            // Check if there was a DB previously saved by getting the last AugmentableEntity Update Stamp
            try
            {

                bool needUpdate = true;

                // TODO check for updates

                /*string query = "SELECT * FROM (" +
                                   "SELECT UpdateDate FROM " + typeof(AugmentableEntities).Name +
                               ")" +
                               " ORDER BY datetime(UpdateDate) DESC LIMIT 1";

                Debug.Log("Database => " + "Query: \n" + query);

                List<UpdateDates> updateDates = sqliteConnection.Query<UpdateDates>(query);

                _lastUpdateDate = updateDates[0].UpdateDate;*/


                if (/*updateDates != null && updateDates.Count > 0 && updateDates[0].UpdateDate != ""*/needUpdate)
                {
                    _webRequest = _lastUpdateDate;
                    Debug.Log("Database => " + "Last update date: " + _webRequest);
                    Debug.Log("Database => " + "Updating Database");
                }
                else
                    throw new Exception("Database not found, creating one from scratch");

            }
            catch (Exception e)
            {
                Debug.Log(e.Message + "\n" + e.StackTrace);

                CreateDB(sqliteConnection);

                _webRequest = "";
            }
            finally
            {

                Action<string> onWebResponse = delegate (string response)
                {
                    try
                    {
                        RemoteDatabase remoteDB = BuildRemoteDB(response);

                        SyncDB(sqliteConnection, remoteDB);

                    }
                    catch (NoUpdatesAvailableException dbUpdate_e)
                    {
                        Debug.Log(dbUpdate_e.Message);
                        throw dbUpdate_e; // Useful for manual updates.
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e.Message);
                        throw e;
                    }

                    if (onDBUpdate != null)
                        onDBUpdate();

                };

                Action onWebResponseFail = delegate
                {

                    if (onDBUpdateFail != null)
                        onDBUpdateFail();

                };

                FetchDatabase(_webRequest, onWebResponse, onWebResponseFail);
            }

        }

        void SyncDB(SQLiteConnection sqliteConnection, RemoteDatabase remoteDatabase) {

            if (remoteDatabase == null)
                throw new NullReferenceException("Unable to update database with a null reference of the remoteDatabase.");

            // TODO implement local / remote databases syncing
            
            /*foreach (AugmentableEntities augmentableEntity in remoteDatabase.AugmentableEntities)               
                sqliteConnection.InsertOrReplace(augmentableEntity, typeof(AugmentableEntities));

            foreach (AugmentableEntityTypes augmentableEntityType in remoteDatabase.AugmentableEntityTypes)
                sqliteConnection.InsertOrReplace(augmentableEntityType, typeof(AugmentableEntityTypes));

            // Remove deleted target content relation entries
            foreach (TargetContentRelations targetContentRelToDelete in remoteDatabase.TargetContentRelationsToDelete)
                sqliteConnection.Delete<TargetContentRelations>(targetContentRelToDelete.ID);

            // Add new target content relation entries
            foreach (TargetContentRelations targetContentRelation in remoteDatabase.TargetContentRelations)
                sqliteConnection.InsertOrReplace(targetContentRelation, typeof(TargetContentRelations));

            foreach (Taxonomy tag in remoteDatabase.Taxonomy)
                sqliteConnection.InsertOrReplace(tag, typeof(Taxonomy));

            // Remove deleted taxonomy relations
            foreach (TaxonomyRelations tagRelationToDelete in remoteDatabase.TaxonomyRelationsToDelete)
                sqliteConnection.Delete<TaxonomyRelations>(tagRelationToDelete.ID);
            
            // Add new taxonomy relation entities
            foreach (TaxonomyRelations tagRelation in remoteDatabase.TaxonomyRelations)
                sqliteConnection.InsertOrReplace(tagRelation, typeof(TaxonomyRelations));*/

        }

#endif

        public void SyncDataBase(
            List<Brands> brands, 
            List<Devices> devices, 
            List<DevicePlans> devicePlans, 
            List<MobileOSs> mobileOSs, 
            List<Plans> plans, 
            List<PlanTypes> planTypes,
            List<PrepaidPlans> prepaidPlans,
            List<Prepaids> prepaids,
#if R9
            List<PrepaidOptPlusPlans> prepaidOptPlusPlans,
            List<AmigoFacilPlans> amigoFacilPlans,
#endif
            List<Promotions> promotions,
            SQLiteConnection sqliteConnection) {

            // To be able to hide old brands not in the server anymore we update all to no visible, so that the fetched ones remain visible
            List<Brands> currentBrandsToHide = GetAllBrands(sqliteConnection);
                        
            foreach (Brands brandToHide in currentBrandsToHide)
                brandToHide.visible = false;            

            sqliteConnection.RunInTransaction(delegate {

                // Sync Brands
                foreach (Brands brandToHide in currentBrandsToHide)
                    sqliteConnection.InsertOrReplace(brandToHide, typeof(Brands));

                foreach (Brands brand in brands)
                    sqliteConnection.InsertOrReplace(brand, typeof(Brands));

				foreach (Devices device in devices)
					sqliteConnection.InsertOrReplace(device, typeof(Devices));

                foreach (DevicePlans devicePlan in devicePlans)
                    sqliteConnection.Insert(devicePlan, typeof(DevicePlans));

                foreach (MobileOSs mobileOS in mobileOSs)
                    sqliteConnection.InsertOrReplace(mobileOS, typeof(MobileOSs));

                foreach (Plans plan in plans)
                    sqliteConnection.InsertOrReplace(plan, typeof(Plans));

                foreach (PlanTypes planType in planTypes)
                    sqliteConnection.InsertOrReplace(planType, typeof(PlanTypes));

#if R9
                foreach (PrepaidOptPlusPlans prepaidOptPlusPlan in prepaidOptPlusPlans)
                    sqliteConnection.InsertOrReplace(prepaidOptPlusPlan, typeof(PrepaidOptPlusPlans));

                foreach (AmigoFacilPlans amigoFacilPlan in amigoFacilPlans)
                    sqliteConnection.Insert(amigoFacilPlan, typeof(AmigoFacilPlans));
#endif

                foreach (PrepaidPlans prepaidPlan in prepaidPlans)
                    sqliteConnection.InsertOrReplace(prepaidPlan, typeof(PrepaidPlans));

                foreach (Prepaids prepaid in prepaids)
                    sqliteConnection.InsertOrReplace(prepaid, typeof(Prepaids));

                foreach (Promotions promotion in promotions)
                    sqliteConnection.Insert(promotion, typeof(Promotions));

            });

            Debug.Log("DatabaseManager => Database Synced");

            //NotificationSystem.Instance.NotifyMessage("Database synced");

        }

#region Database Queries 

        /// <summary>
        /// Returns all AugmentableEntities, by default returns both active and unactive entities
        /// </summary>
        /// <param name="sqliteConnection"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        /*public List<AugmentableEntities> GetAugmentableEntities(SQLiteConnection sqliteConnection, bool? active = null, AREntityType arType = AREntityType.ANY, double appVersionForContent = -1, AREntityOrderBy order = AREntityOrderBy.NONE) {

            try
            {

                string contentType = ((int)arType >= 0 ? ((int)arType == 0 ? "Type!=1" : "Type=" + ((int)arType).ToString()) : "");

                string activeContent = ((active != null)?(active == true?"Active=1":"Active=0") :"");

                string appVersion = appVersionForContent >= 0 ? "SupportedAppVersion<=" + appVersionForContent : "";

                string orderBy = order == AREntityOrderBy.APP_VERSION_DESC ? " ORDER BY SupportedAppVersion DESC" : "";

                string query = "SELECT * FROM " + typeof(AugmentableEntities).Name + (contentType != "" || activeContent != "" || appVersion != ""?" WHERE ":"") + contentType + 
                                ((contentType != "" && activeContent != "") || 
                                 (contentType != "" && appVersion != "") || 
                                 (activeContent != "" && appVersion != "")?" AND ":"") + activeContent + 
                                (contentType != "" && activeContent != "" && appVersion != ""?" AND ":"") + appVersion + orderBy;

                Debug.Log("Database => Query: \n" + query);

                List<AugmentableEntities> augmentableEntitiesToDownloadContent = sqliteConnection.Query<AugmentableEntities>(query);

                return augmentableEntitiesToDownloadContent;

            }
            catch (Exception e)
            {
                Debug.LogError(e.Message + "\n" + e.StackTrace.ToString());
                Debug.LogError("Database => " + "Aborting Request");
                return null;
            }

        }*/

#region Brands Queries

        public Brands GetBrandByBrandId(string brandId, SQLiteConnection sqliteConnection) {

            string query = "SELECT * FROM " + typeof(Brands).Name + " WHERE brand_id='" + brandId+"'";
            
            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Brands> brands = sqliteConnection.Query<Brands>(query);

            Brands brand = null;

            if (brands != null && brands.Count > 0)
                brand = brands[0];

            return brand;
        }

        public List<Brands> GetAllBrands(SQLiteConnection sqliteConnection) {

            string query = "SELECT * FROM " + typeof(Brands).Name;            

            Debug.Log("DatabaseManager => Executing Query: " + query);

            return sqliteConnection.Query<Brands>(query);

        }

		public List<Brands> GetBrandsByTerm(SQLiteConnection sqliteConnection, bool? areVisible = null, bool? haveLTE = null, bool? haveSnapDragon = null,bool? fee = null,string plan_id = null)
        {

            string query = "SELECT DISTINCT b.* FROM " + typeof(Brands).Name + " as b " +
                    "INNER JOIN " + typeof(Devices) + " as d " +
                    "ON b.brand_id = d.brand_id";

			if(plan_id != null){

				query+= " INNER JOIN " + typeof(DevicePlans) + " as dp "+
					"ON d.device_id = dp.device_id";
			}

			if (areVisible == true || haveLTE == true || haveSnapDragon == true || plan_id != null || fee == true)
            {

                query += " WHERE ";

                if (areVisible == true)
					query += "b.visible=1" + (haveLTE == true || haveSnapDragon == true || plan_id != null ? " AND " : "");

                if (haveLTE == true)
					query += "d.lte=1" + (haveSnapDragon == true || plan_id != null ? " AND " : "");

                if (haveSnapDragon == true)
					query += "d.snapdragon=1"+ (plan_id != null ? " AND " : "");

				if (plan_id != null)
					query += "dp.plan_id='" + plan_id+"'" + (fee == true ? " AND " : "");

				if (fee == true) {
					query += "dp.cost = 0 AND dp.months = 24";
				}
            }

			query += " ORDER BY b.brand_name ASC";

             Debug.Log("DatabaseManager => Executing Query: " + query);

	

            return sqliteConnection.Query<Brands>(query);

        }

        #endregion

        #region Devices Queries

#if DEUR

        public Devices GetDeviceByDeviceId(int deviceId, SQLiteConnection sqliteConnection) {

            string query = "SELECT * FROM " + typeof(Devices).Name + " WHERE device_id=" + deviceId + " ORDER BY version_id DESC";

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Devices> devices = sqliteConnection.Query<Devices>(query);

            Devices device = null;

            if (devices != null && devices.Count > 0)
                device = devices[0];

            return device;

        }

#elif R9

        public Devices GetDeviceByDeviceId(string deviceId, SQLiteConnection sqliteConnection) {

            string query = "SELECT * FROM " + typeof(Devices).Name + " WHERE device_id='" + deviceId + "' ORDER BY version_id DESC";

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Devices> devices = sqliteConnection.Query<Devices>(query);

            Devices device = null;

            if (devices != null && devices.Count > 0)
                device = devices[0];

            return device;

        }

#endif

		public List<Devices> GetDevicesByBrand(string brandId,bool lte, bool snap,bool fee,string planId,int versionId, SQLiteConnection sqliteConnection)
        {

			string query;

			if(planId != null)
			query = "SELECT DISTINCT d.* FROM " + typeof(Devices).Name + " as d INNER JOIN "+typeof(DevicePlans).Name+" as dp ON d.device_id = dp.device_id WHERE d.brand_id='" + brandId + "' AND d.version_id="+versionId;
			else
				query = "SELECT DISTINCT d.* FROM " + typeof(Devices).Name + " as d WHERE d.brand_id='" + brandId + "' AND d.version_id="+versionId;	
			if (snap == true) {
				query += " AND d.snapdragon=1";
			}

			if (lte == true) {
				query += " AND d.lte=1";
			}

			if (fee == true && planId != null) {
				query += " AND dp.cost = 0 AND dp.months = 24 AND dp.plan_id = '" + planId + "'";
			} else if (fee == false && planId != null) {
				query += " AND dp.plan_id ='"+planId+"'";
			}

if(SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.PREPAID || SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO){
            query+=" AND d.prepaid_cost > 0";
}

			query+=" ORDER BY cast(d.position as INTEGER) ASC";

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Devices> devices = sqliteConnection.Query<Devices>(query);

            return devices;

        }

        public List<Devices> GetAllDevices(SQLiteConnection sqliteConnection) {

			string query = "SELECT * FROM " + typeof(Devices).Name;

            return sqliteConnection.Query<Devices>(query);

        }

#endregion

#region DevicePlans Queries

        public List<DevicePlans> GetAllDevicePlans(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(DevicePlans).Name;

            Debug.Log("Database Manager => Executing query: " + query);

            return sqliteConnection.Query<DevicePlans>(query);

        }

		public List<DevicePlans> GetPlanFromDevice(Devices device,Plans plan,int version,SQLiteConnection sqliteConnection){
			if (plan != null && device != null) {

				String sql = "SELECT DISTINCT * FROM " + typeof(DevicePlans).Name + " WHERE version_id=" + version + " AND device_id='" + device.device_id + "' AND plan_id='" + plan.plan_id + "' and months != 36 GROUP BY months ORDER BY months";
				Debug.Log ("Query for iphone" + sql);
				return sqliteConnection.Query<DevicePlans> (sql);
			} else {
				return null;
			}
		}

		public Plans getBestPlanFromAmigo(Devices device,int months,int version, SQLiteConnection sqliteConnection){
			string sql ="SELECT p.* FROM "+ typeof(Plans).Name+" AS p "+
				"INNER JOIN "+typeof(DevicePlans)+" AS dp ON p.plan_id=dp.plan_id "+
				"INNER JOIN "+typeof(PlanTypes)+" AS pt ON p.tipo_plan=pt.type_plan WHERE dp.version_id="+version+" AND dp.device_id='"+device.device_id+"' AND dp.months="+months+
				" ORDER BY pt.prioridad, p.plan_id, dp.cost";

			List<Plans> plans = sqliteConnection.Query<Plans>(sql);

			return plans.Count > 0 ? plans[0]:null;
			
		}
			

        public DevicePlans GetBestPlanFromDevice(Devices device, int months, int version, SQLiteConnection sqliteConnection)
        {

            /*String sql = "SELECT p.plan_id, p.name, p.monthly_cost, p.device_tax, " +
                    "p.mins, p.sms, p.megabytes, p.free_numbers, p.extra_min, " +
                    "p.extra_sms, p.extra_kb, p.social_networks, p.sin_frontera, p.plan_type_id, " +
                    "p.visible, dp.cost " +
                    "FROM " + TuPlanSqliteHelper.PLANS_TABLE + " AS p INNER JOIN " +
                    TuPlanSqliteHelper.DEVICES_PLANS_TABLE + " AS dp ON " +
                    "p.plan_id = dp.plan_id " + " INNER JOIN " + TuPlanSqliteHelper.PLAN_TYPES_TABLE + " AS pt ON " +
                    "p.plan_type_id = pt.plan_type_id " +
                    "WHERE dp.version_id=" + mVersionId + " AND dp.device_id='" + device.getDeviceId() + "' " +
                    "AND dp.months=" + months + " AND p.visible=1 ORDER BY pt.priority, p.plan_id, dp.cost";*/


			string extra_sql = "";
			if (device.brand_id == "APP") {
				extra_sql = " AND pt.type_plan == 'MAX_SL'";
			}

            string sql ="SELECT p.* FROM "+ typeof(Plans).Name+" AS p "+
                        "INNER JOIN "+typeof(DevicePlans)+" AS dp ON p.plan_id=dp.plan_id "+
				"INNER JOIN "+typeof(PlanTypes)+" AS pt ON p.tipo_plan=pt.type_plan WHERE dp.version_id="+version+" AND dp.device_id='"+device.device_id+"' AND dp.months="+months+ extra_sql+
                        " ORDER BY pt.prioridad, p.plan_id, dp.cost";
			
			Debug.Log ("Primer query best Plan" + sql);
		
            List<Plans> plans = sqliteConnection.Query<Plans>(sql);

	

            Dictionary<DevicePlans, Plans> devicePlansRelationWithPlan = new Dictionary<DevicePlans, Plans>();

            foreach (Plans plan in plans) {
                string subSQL = "SELECT * FROM " + typeof(DevicePlans) + " WHERE plan_id='" + plan.plan_id + "' AND months="+months+" AND device_id='" + device.device_id + "' ORDER BY plan_id";
				Debug.Log ("Segundo query best Plan" + subSQL);
				List<DevicePlans> devicePlans = sqliteConnection.Query<DevicePlans>(subSQL);
				Debug.Log (subSQL);
				devicePlansRelationWithPlan.Add(devicePlans[0], plan);

            }

            DevicePlans bestPlan = null;

            foreach (KeyValuePair<DevicePlans, Plans> entry in devicePlansRelationWithPlan) {
				
                if (entry.Key.cost == "0") {
                    bestPlan = entry.Key;
                    break;
                }
            }

            if (bestPlan == null)
            {

                foreach (KeyValuePair<DevicePlans, Plans> entry in devicePlansRelationWithPlan)
                {
                    if (
#if DEUR
                        entry.Value.plan_name.Contains("7000") 
#elif R9
                        entry.Value.plan_name.Contains("MAX 9000") 
                        || entry.Value.plan_name.Contains("PRO 1500")
#else
                        true
#endif
                        )
                    {
                        bestPlan = entry.Key;
                        break;
                    }
                }
                                
            }

            // Este método asegura que siempre haya un best plan, aunque el plan no sea MAX ni PRO
			if (bestPlan == null && plans.Count > 0)
            {

				Debug.Log("DatabaseManager => Getting device fallback best plan");

				string subSQL = "SELECT * FROM " + typeof(DevicePlans) + " WHERE plan_id='" + plans[plans.Count-1].plan_id + "' AND device_id='" + device.device_id + "' AND months="+months;
				Debug.Log ("Tecer query best Plan" + subSQL);
				List<DevicePlans> lastDevicePlans = sqliteConnection.Query<DevicePlans>(subSQL);

                bestPlan = lastDevicePlans[0];
            }

            return bestPlan;
        }
                           

        public List<DevicePlans> GetDevicesPlans(Devices device, DevicePlans plan, int dataVersion, SQLiteConnection sqliteConnection)
        {
                        
            List<DevicePlans> devicePlans = new List<DevicePlans>();

            String query = "SELECT DISTINCT * FROM " + typeof(DevicePlans) +
                    " WHERE version_id=" + dataVersion + " AND device_id='" + device.device_id + "'" +
                    " AND plan_id='" + plan.plan_id + "' and months != 36 ORDER BY months";

            Debug.Log("Database Manager => Executing Query: " + query);

            devicePlans = sqliteConnection.Query<DevicePlans>(query);
            
            return devicePlans;
        }


        #endregion

        #region MobileOSs Queries

        public MobileOSs GetMobileOSByOSId(string mobileOSId, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(MobileOSs).Name + " WHERE os_id='" + mobileOSId + "'";

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<MobileOSs> mobileOSs = sqliteConnection.Query<MobileOSs>(query);

            MobileOSs mobileOS = null;

            if (mobileOSs != null && mobileOSs.Count > 0)
                mobileOS = mobileOSs[0];

            return mobileOS;
        }

        public List<MobileOSs> GetAllMobileOSs(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(MobileOSs).Name;

            return sqliteConnection.Query<MobileOSs>(query);

        }

#endregion

#region Plans Queries

        public Plans GetPlanByPlanId(string planId, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(Plans).Name + " WHERE plan_id='" + planId + "'";

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Plans> plans = sqliteConnection.Query<Plans>(query);

            Plans plan = null;

            if (plans != null && plans.Count > 0)
                plan = plans[0];

            return plan;
        }

        public List<Plans> GetAllPlans(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(Plans).Name;

            return sqliteConnection.Query<Plans>(query);

        }

		public List<Plans> GetAllPlansByType(string planTypeSKU,int versionId, SQLiteConnection sqliteConnection)
		{

			string query = "SELECT * FROM " + typeof(Plans).Name + " WHERE tipo_plan='" + planTypeSKU + "' AND version_id="+versionId + " ORDER BY CAST(monthly_cost AS INT) ASC"; 
			return sqliteConnection.Query<Plans>(query);

		}

		public List<Plans> GetAllPlansByTypeAndByDevice(string planTypeSKU,int versionId,string device, SQLiteConnection sqliteConnection)
		{

			string query = "SELECT p.* FROM " + typeof(Plans).Name + " AS p"
				+ " INNER JOIN " + typeof(DevicePlans) + " AS dp ON p.plan_id=dp.plan_id" 
				+ " WHERE p.tipo_plan='" + planTypeSKU + "' AND p.version_id="+versionId 
				+ " AND dp.device_id ='" + device + "'"
				+ " ORDER BY CAST(monthly_cost AS INT) ASC"; 



			return sqliteConnection.Query<Plans>(query);

		}

		public List<PlanTypes>GetAllPlansTypeAndByDevice(string device,int version, SQLiteConnection sqliteConnection){

			string query = "SELECT DISTINCT t.* FROM " + typeof(PlanTypes).Name + " AS t " +
			             "INNER JOIN " + typeof(Plans) + " AS p ON t.type_plan=p.tipo_plan " +
			             "INNER JOIN " + typeof(DevicePlans) + " AS dp ON p.plan_id=dp.plan_id " +
				"WHERE dp.version_id=" + version + " AND dp.device_id='" + device+"' ORDER BY t.type_plan ASC";

			return sqliteConnection.Query<PlanTypes>(query);

		}


#endregion

#region PlanTypes Queries

        public PlanTypes GetPlanType(string planTypeSKU, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(PlanTypes).Name + " WHERE type_plan='" + planTypeSKU + "'";

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<PlanTypes> planTypes = sqliteConnection.Query<PlanTypes>(query);

            PlanTypes planType = null;

            if (planTypes != null && planTypes.Count > 0)
                planType = planTypes[0];

            return planType;
        }

        public List<PlanTypes> GetAllPlanTypes(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(PlanTypes).Name + " ORDER BY prioridad ASC";

            return sqliteConnection.Query<PlanTypes>(query);

        }

#endregion

#region PrepaidPlans Queries

        public PrepaidPlans GetPrepaidPlanByPrepaidPlanId(int prepaidPlanId, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(PrepaidPlans).Name + " WHERE id_amigo_recarga=" + prepaidPlanId;

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<PrepaidPlans> prepaidPlans = sqliteConnection.Query<PrepaidPlans>(query);

            PrepaidPlans prepaidPlan = null;

            if (prepaidPlans != null && prepaidPlans.Count > 0)
                prepaidPlan = prepaidPlans[0];

            return prepaidPlan;
        }

        public List<PrepaidPlans> GetAllPrepaidPlans(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(PrepaidPlans).Name;

            return sqliteConnection.Query<PrepaidPlans>(query);

        }

		public string GetLimitCostoValueFromPlan(bool getMin, Plans plan,SQLiteConnection sqliteConnection) {
			string query = "SELECT * FROM "+typeof(Plans).Name +" WHERE tipo_plan = '"+plan.tipo_plan+"' ORDER BY monthly_cost "+(getMin? "ASC":"DESC");

			List<Plans> plans = sqliteConnection.Query<Plans>(query);

			string value = "";

			if (plans != null && plans.Count > 0)
				value = plans[0].monthly_cost;

			return value;
		}

		public float GetLimitMBValueFromPlan(bool getMin, Plans plan,SQLiteConnection sqliteConnection) {
			string query = "SELECT * FROM "+typeof(Plans).Name +" WHERE tipo_plan = '"+plan.tipo_plan+"' ORDER BY megabytes_inc "+(getMin? "ASC":"DESC");

			List<Plans> plans = sqliteConnection.Query<Plans>(query);

			float value = 0;

			if (plans != null && plans.Count > 0)
				value = plans [0].megabytes_inc;

			return value;
		}

        /// <summary>
        /// Get the limit value of recarga from all PrepaidPlans
        /// </summary>
        /// <param name="getMin">If true gets the minimum value else returns the maximum</param>
        /// <param name="sqliteConnection">The local SQLiteConnection</param>
        /// <returns></returns>
        public string GetLimitRecargaValueFromAllPrepaidPlans(bool getMin, SQLiteConnection sqliteConnection) {
            
            string query = "SELECT * FROM PrepaidPlans ORDER BY recarga "+(getMin? "ASC":"DESC");

            List<PrepaidPlans> prepaidPlans = sqliteConnection.Query<PrepaidPlans>(query);

            Debug.Log("Database Manager => Executing Query: " + query);

            string value = "";

            if (prepaidPlans != null && prepaidPlans.Count > 0)
                value = prepaidPlans[0].recarga;

            return value;
                        
        }

        /// <summary>
        /// Get the limit value of megabytes from all PrepaidPlans
        /// </summary>
        /// <param name="getMin">If true gets the minimum value else returns the maximum</param>
        /// <param name="sqliteConnection">The local SQLiteConnection</param>
        /// <returns></returns>
        public int GetLimitMegabytesValueFromAllPrepaidPlans(bool getMin, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM PrepaidPlans ORDER BY megabytes " + (getMin ? "ASC" : "DESC");

            List<PrepaidPlans> prepaidPlans = sqliteConnection.Query<PrepaidPlans>(query);

            Debug.Log("Database Manager => Executing Query: " + query);

            int value = 0;

            if (prepaidPlans != null && prepaidPlans.Count > 0)
                value = prepaidPlans[0].megabytes;

            return value;

        }

        #endregion

        #region Prepaid Queries

        public Prepaids GetPrepaidByPrepaidId(int prepaidId, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(Prepaids).Name + " WHERE _id_reg_amigo=" + prepaidId;

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Prepaids> prepaids = sqliteConnection.Query<Prepaids>(query);

            Prepaids prepaid = null;

            if (prepaids != null && prepaids.Count > 0)
                prepaid = prepaids[0];

            return prepaid;
        }

        public List<Prepaids> GetAllPrepaids(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(Prepaids).Name;

            return sqliteConnection.Query<Prepaids>(query);

        }

#endregion

#region PrepaidOptPlusPlans Queries

     
		public PrepaidOptPlusPlans GetPrepaidOptPlusPlanByPrepaidOptPlusPlanId(int prepaidOptPlusPlanId, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(PrepaidOptPlusPlans).Name + " WHERE _id_reg=" + prepaidOptPlusPlanId;

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<PrepaidOptPlusPlans> prepaidOptPlusPlans = sqliteConnection.Query<PrepaidOptPlusPlans>(query);

            PrepaidOptPlusPlans prepaidOptPlusPlan = null;

            if (prepaidOptPlusPlans != null && prepaidOptPlusPlans.Count > 0)
                prepaidOptPlusPlan = prepaidOptPlusPlans[0];

            return prepaidOptPlusPlan;
        }

        public List<PrepaidOptPlusPlans> GetAllPrepaidOptPlusPlans(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(PrepaidOptPlusPlans).Name;

            return sqliteConnection.Query<PrepaidOptPlusPlans>(query);

        }

#endregion

#region AmigoFacilPlans Queries

        public AmigoFacilPlans GetAmigoFacilPlanByAmigoFacilPlanId(int amigoFacilPlanId, SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(AmigoFacilPlans).Name + " WHERE _id_reg=" + amigoFacilPlanId;

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<AmigoFacilPlans> amigoFacilPlans = sqliteConnection.Query<AmigoFacilPlans>(query);

            AmigoFacilPlans amigoFacilPlan = null;

            if (amigoFacilPlans != null && amigoFacilPlans.Count > 0)
                amigoFacilPlan = amigoFacilPlans[0];

            return amigoFacilPlan;
        }

        public List<AmigoFacilPlans> GetAllAmigoFacilPlans(SQLiteConnection sqliteConnection)
        {

            string query = "SELECT * FROM " + typeof(AmigoFacilPlans).Name;

            return sqliteConnection.Query<AmigoFacilPlans>(query);

        }

		public List<Devices> GetRelatedDevices(string deviceId,SQLiteConnection sqliteConnection)
		{
			Devices device = GetDeviceByDeviceId (deviceId,sqliteConnection);

			float percent = device.prepaid_cost * 0.10f;
			float costMore = device.prepaid_cost + percent;
			float costLess = device.prepaid_cost - percent;
			float cost = device.prepaid_cost;


			string query = "SELECT * FROM " + typeof(Devices).Name + " WHERE display_int ="+device.display_int+ " AND storage_int ="+ device.storage_int  +" AND prepaid_cost >= "+device.prepaid_cost+" AND device_id != '"+device.device_id+"' " + " ORDER BY prepaid_cost ASC LIMIT 1";

			List<Devices> devices = sqliteConnection.Query<Devices>(query);

			if(devices.Count > 0){
				string query2 = "SELECT * FROM " + typeof(Devices).Name + " WHERE display_int ="+device.display_int+ " AND storage_int ="+ device.storage_int  +" AND prepaid_cost > "+ costLess +" AND prepaid_cost < "+ device.prepaid_cost +  " AND device_id != '"+device.device_id+"' " + " ORDER BY prepaid_cost DESC LIMIT 1";
				List<Devices> devices2 = sqliteConnection.Query<Devices>(query2);

				if (devices2.Count > 0 && devices2[0].ID != devices[0].ID) {
					devices.Add (devices2 [0]);
				}

			}

			return devices;

		}

		public AmigoFacilPlans GetAmigoDevicesPlan(string deviceId,int versionId,SQLiteConnection sqliteConnection)
		{
			string query = "SELECT * FROM " + typeof(AmigoFacilPlans).Name + " WHERE  id_device='" + deviceId+"' AND version_id="+versionId;

			Debug.Log("DatabaseManager => Executing Query: " + query);

			List<AmigoFacilPlans> amigoFacilPlans = sqliteConnection.Query<AmigoFacilPlans>(query);

			AmigoFacilPlans amigoFacilPlan = null;

			if (amigoFacilPlans != null && amigoFacilPlans.Count > 0)
				amigoFacilPlan = amigoFacilPlans[0];

			return amigoFacilPlan;

		}

#endregion

#region Promotion Queries

        public Promotions GetPromotionByPromotionId(int promotionId, SQLiteConnection sqliteConnection)
        {
            string query = "SELECT * FROM " + typeof(Promotions).Name + " WHERE promotion_id=" + promotionId;

            Debug.Log("DatabaseManager => Executing Query: " + query);

            List<Promotions> promotions = sqliteConnection.Query<Promotions>(query);

            Promotions promotion = null;

            if (promotions != null && promotions.Count > 0)
                promotion = promotions[0];

            return promotion;
        }

        public List<Promotions> GetAllPromotions(SQLiteConnection sqliteConnection)
        {
            string query = "SELECT * FROM " + typeof(Promotions).Name;
            return sqliteConnection.Query<Promotions>(query);
        }

#endregion

#endregion

#region Database Actions       

        /// <summary>
        /// Drop all tables
        /// </summary>
        /// <param name="sqliteConnection"></param>
        public void ClearDatabase(SQLiteConnection sqliteConnection)
        {

            try
            {

                string command = "";

                foreach (Type table in _tables)                
                    command += "DROP TABLE IF EXISTS " + table.Name+"; ";

                Debug.Log("DatabaseManager => Clearing Database.");// Command: " + command);               
                sqliteConnection.Execute(command);

            }
            catch (SQLiteException e)
            {

                Debug.LogError("An error ocurring while droping a table.\nCause: " + e.Message + "\n" + e.StackTrace);

            }

        }

        /// <summary>
        /// Clears entries of a given version from database
        /// </summary>
        public void CleanDatabase(SQLiteConnection sqliteConnection, int entryVersionToDelete) {
            
            // Cleaning DevicePlans
            string devicePlansQuery = "DELETE FROM " + typeof(DevicePlans) + " WHERE version_id<" + entryVersionToDelete + ";";

#if R9
            string amigoFacilQuery = "DELETE FROM " + typeof(AmigoFacilPlans) + " WHERE version_id<" + entryVersionToDelete + ";";
#endif

            sqliteConnection.RunInTransaction(delegate {
                sqliteConnection.Query<DevicePlans>(devicePlansQuery);
#if R9
                sqliteConnection.Query<AmigoFacilPlans>(amigoFacilQuery);
#endif
            });

            Debug.Log("DatabaseManager => Cleaned Database successfully, all entries with the given version to delete have been deleted.");
        }

        public void ClearAllEntriesFromTable<T>(SQLiteConnection sqliteConnection) where T : class {
            sqliteConnection.DeleteAll<T>();
        }

#endregion

    }

}
