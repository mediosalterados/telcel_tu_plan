﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

using GLIB.Core;
using GLIB.Net;
using GLIB.Utils;

namespace TuPlan.Controllers.DataManagement
{

    public class DataManager : BackModule<DataManager>
    {

        public int DataVersion { get { return _dataVersion; } }
        int _dataVersion {
            get {
                int version = PlayerPrefs.GetInt("data_version", 0);
                return version;
            }
            set {
                PlayerPrefs.SetInt("data_version", value);
            }
        }

        public int RemoteDataVersion { get { return _remoteDataVersion; } }
        int _remoteDataVersion = -1;
        
        bool _brandsUpdated = false;
        List<Brands> _fetchedBrands = new List<Brands>();

        bool _devicesUpdated = false;
        List<Devices> _fetchedDevices = new List<Devices>();

        bool _devicePlansUpdated = false;
        List<DevicePlans> _fetchedDevicePlans = new List<DevicePlans>();

        bool _mobileOSsUpdated = false;
        List<MobileOSs> _fetchedMobileOSs = new List<MobileOSs>();

        bool _plansUpdated = false;
        List<Plans> _fetchedPlans = new List<Plans>();

        bool _planTypesUpdated = false;
        List<PlanTypes> _fetchedPlanTypes = new List<PlanTypes>();

        bool _prepaidPlansUpdated = false;
        List<PrepaidPlans> _fetchedPrepaidPlans = new List<PrepaidPlans>();

        bool _prepaidsUpdated = false;
        List<Prepaids> _fetchedPrepaids = new List<Prepaids>();

        bool _prepaidOptPlusPlansUpdated = false;
        List<PrepaidOptPlusPlans> _fetchedPrepaidOptPlusPlans = new List<PrepaidOptPlusPlans>();

        bool _amigoFacilPlansUpdated = false;
        List<AmigoFacilPlans> _fetchedAmigoFacilPlans = new List<AmigoFacilPlans>();

        bool _promotionsUpdated = false;
        List<Promotions> _fetchedPromotions = new List<Promotions>();
        
        protected override void ProcessInitialization()
        {
			

            DatabaseManager.Instance.Initialize();
            DatabaseManager.Instance.LoadLocalDB();

			if (!DatabaseManager.Instance.DatabaseCheck (DatabaseManager.Instance.localDBConnection)) {
				DatabaseManager.Instance.ClearDatabase (DatabaseManager.Instance.localDBConnection);
				DatabaseManager.Instance.CreateDB (DatabaseManager.Instance.localDBConnection);
			} else {
				//BUSCO ACTUALIZAR TABLAS	
				DatabaseManager.Instance.UpdateDB (DatabaseManager.Instance.localDBConnection);
			}

            Debug.Log("DataManager => Database is ready, checking for updates");

			this.ForceUpdate ();
            
        }

		public void ForceUpdate(){
			CheckForUpdates(delegate {
				Debug.Log("DataManager => Update needed!");
				FetchData();
			});
		}

		public void setUpdated(){
			PlayerPrefs.SetInt("first_time", 1);
			PlayerPrefs.SetInt("version_1_4", 1);
		}

        protected override void ProcessUpdate()
        {
            if (NetClient.Instance.IsDownloading)
                NotificationSystem.Instance.NotifyProgress("Descargando Datos... " + NetClient.Instance.allFilesDownloadProgress);
        }

        protected override void ProcessTermination()
        {


        }

        public void CheckForUpdates(Action onUpdateNeed) {
            // Do these to test
            Debug.Log("DataManager => Data Version: " + _dataVersion);
           
            NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                int remoteDataVersion = DataDeserializer.Instance.DeserializeDataVersion(webResponse);
                Debug.Log("DataManager => Remote Data Version: " + remoteDataVersion);
                _remoteDataVersion = remoteDataVersion;
                if (remoteDataVersion > _dataVersion)
                    onUpdateNeed();                
            };

            if (NetClient.Instance.ActiveWebClient != null)
                NetClient.Instance.ActiveWebClient.Headers[DataPaths.WS_HEADER] = DataPaths.WS_AUTH;

            NetClient.OnWebRequestFailDelegate onWebRequestFail = delegate
            {
                NotificationSystem.Instance.PromptAction(DataMessages.SERVER_RESPONSE_FAIL, delegate { CheckForUpdates(onUpdateNeed); }, delegate { NotificationSystem.Instance.Terminate(); });
            };

            StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WS_API_URL, null, onWebRequestDone, onWebRequestFail));

        }

        public void FetchData() {

            Debug.Log("Data Manager => Updating Data");

            _brandsUpdated = false;
            _fetchedBrands.Clear();

            _devicesUpdated = false;
            _fetchedDevices.Clear();

            _devicePlansUpdated = false;
            _fetchedDevicePlans.Clear();

            _mobileOSsUpdated = false;
            _fetchedMobileOSs.Clear();

            _plansUpdated = false;
            _fetchedPlans.Clear();

            _planTypesUpdated = false;
            _fetchedPlanTypes.Clear();

            _prepaidPlansUpdated = false;
            _fetchedPrepaidPlans.Clear();

            _prepaidsUpdated = false;
            _fetchedPrepaids.Clear();

#if R9
            _prepaidOptPlusPlansUpdated = false;
            _fetchedPrepaidOptPlusPlans.Clear();

            _amigoFacilPlansUpdated = false;
            _fetchedAmigoFacilPlans.Clear();
#endif

            _promotionsUpdated = false;
            _fetchedPromotions.Clear();

            CheckFetchedData();

            NotificationSystem.Instance.NotifyProgress("Actualizando...");

        }

        public void CheckFetchedData() {
                        
            NetClient.OnWebRequestFailDelegate onWebRequestFail = delegate
            {
                NotificationSystem.Instance.PromptAction(DataMessages.SERVER_RESPONSE_FAIL, delegate { NotificationSystem.Instance.Terminate(); CheckFetchedData(); }, delegate { NotificationSystem.Instance.Terminate(); });
            };

            if (!_brandsUpdated)
            {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedBrands = DataDeserializer.Instance.DeserializeBrands(webResponse);
                    _brandsUpdated = true;
                    Debug.Log("DataManager => Updated Brands.");
                    CheckFetchedData();                                     
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WS_BRANDS_URL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WS_BRANDS_URL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_devicesUpdated)
            {
				Debug.Log ("Intento");
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedDevices = DataDeserializer.Instance.DeserializeDevices(webResponse);
                    _devicesUpdated = true;
                    Debug.Log("DataManager => Updated Devices.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSDevicesURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSDevicesURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_devicePlansUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedDevicePlans = DataDeserializer.Instance.DeserializeDevicePlans(webResponse);
                    _devicePlansUpdated = true;
                    Debug.Log("DataManager => Updated Device Plans.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSDevicePlansURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSDevicePlansURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_mobileOSsUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedMobileOSs = DataDeserializer.Instance.DeserializeMobileOSs(webResponse);
                    _mobileOSsUpdated = true;
                    Debug.Log("DataManager => Updated Mobile OSs.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WS_MOBILEOSS_URL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WS_MOBILEOSS_URL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_plansUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedPlans = DataDeserializer.Instance.DeserializePlans(webResponse);
                    _plansUpdated = true;
                    Debug.Log("DataManager => Updated Plans.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSPlansURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSPlansURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_planTypesUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedPlanTypes = DataDeserializer.Instance.DeserializePlanTypes(webResponse);
                    _planTypesUpdated = true;
                    Debug.Log("DataManager => Updated Plan Types.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSPlanTypesURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSPlanTypesURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_prepaidPlansUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedPrepaidPlans = DataDeserializer.Instance.DeserializePrepaidPlans(webResponse);
                    _prepaidPlansUpdated = true;
                    Debug.Log("DataManager => Updated Prepaid Plans.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSPrepaidPlansURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSPrepaidPlansURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_prepaidsUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedPrepaids = DataDeserializer.Instance.DeserializePrepaids(webResponse);
                    _prepaidsUpdated = true;
                    Debug.Log("DataManager => Updated Prepaids.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSPrepaidsURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSPrepaidsURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

#if R9

            if (!_prepaidOptPlusPlansUpdated)
            {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedPrepaidOptPlusPlans = DataDeserializer.Instance.DeserializePrepaidOptPlusPlans(webResponse);
                    _prepaidOptPlusPlansUpdated = true;
                    Debug.Log("DataManager => Updated PrepaidOptPlusPlans.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSPrepaidsOptPlusURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSPrepaidsOptPlusURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            if (!_amigoFacilPlansUpdated)
            {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedAmigoFacilPlans = DataDeserializer.Instance.DeserializeAmigoFacilPlans(webResponse);
                    _amigoFacilPlansUpdated = true;
                    Debug.Log("DataManager => Updated AmigoFacilPlans.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WSAmigoFacilPlansURL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WSAmigoFacilPlansURL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

#endif

            if (!_promotionsUpdated) {
                NetClient.OnWebRequestDoneDelegate onWebRequestDone = delegate (string webResponse) {
                    _fetchedPromotions = DataDeserializer.Instance.DeserializePromotions(webResponse);
                    _promotionsUpdated = true;
                    Debug.Log("DataManager => Updated Promotions.");
                    CheckFetchedData();
                };

                Debug.Log("DataManager => Sending Web Request: " + DataPaths.WS_PROMOTIONS_URL);

                StartCoroutine(NetClient.Instance.MakeWebRequest(DataPaths.WS_PROMOTIONS_URL, null, onWebRequestDone, onWebRequestFail));
                return;
            }

            // Prepare all fetched data for database sync
            _fetchedBrands = PrepareDataForDatabaseSync(_fetchedBrands);
            _fetchedDevices = PrepareDataForDatabaseSync(_fetchedDevices);
            _fetchedDevicePlans = PrepareDataForDatabaseSync(_fetchedDevicePlans);
            _fetchedMobileOSs = PrepareDataForDatabaseSync(_fetchedMobileOSs);
            _fetchedPlans = PrepareDataForDatabaseSync(_fetchedPlans);
            _fetchedPlanTypes = PrepareDataForDatabaseSync(_fetchedPlanTypes);
            _fetchedPrepaidPlans = PrepareDataForDatabaseSync(_fetchedPrepaidPlans);
            _fetchedPrepaids = PrepareDataForDatabaseSync(_fetchedPrepaids);
#if R9
            _fetchedPrepaidOptPlusPlans = PrepareDataForDatabaseSync(_fetchedPrepaidOptPlusPlans);
            _fetchedAmigoFacilPlans = PrepareDataForDatabaseSync(_fetchedAmigoFacilPlans);
#endif
            _fetchedPromotions = PrepareDataForDatabaseSync(_fetchedPromotions);

            UpdateData(
                _fetchedBrands, 
                _fetchedDevices, 
                _fetchedDevicePlans, 
                _fetchedMobileOSs, 
                _fetchedPlans, 
                _fetchedPlanTypes,
                _fetchedPrepaidPlans,
                _fetchedPrepaids,
#if R9
                _fetchedPrepaidOptPlusPlans,
                _fetchedAmigoFacilPlans,
#endif
                _fetchedPromotions
                );

        }

        public void UpdateData(
            List<Brands> newBrands, 
            List<Devices> newDevices, 
            List<DevicePlans> newDevicePlans, 
            List<MobileOSs> newMobileOSs, 
            List<Plans> newPlans, 
            List<PlanTypes> newPlanTypes,
            List<PrepaidPlans> newPrepaidPlans,
            List<Prepaids> newPrepaids,
#if R9
            List<PrepaidOptPlusPlans> newPrepaidOptPlusPlans,
            List<AmigoFacilPlans> newAmigoFacilPlans,
#endif
            List<Promotions> newPromotions
            ) {

            // Download all assets
            Debug.Log("DataManager => Downloading Assets...");
            foreach (Brands brand in newBrands)
            {
                if (String.IsNullOrEmpty(brand.normal_image_filepath)) {

                    string imageUrl = DataPaths.WS_URL + brand.normal_image_url;
                    string imageName = null;

                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);
                    
                    string savePath = DataPaths.BrandsAssetsPath + "/" + brand.brand_id + "/normal";

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath+savePath, imageName);
                    brand.normal_image_filepath = savePath + "/" + imageName;
                }

                if (String.IsNullOrEmpty(brand.hover_image_filepath)) {

                    string imageUrl = DataPaths.WS_URL + brand.hover_image_url;
                    string imageName = null;

                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

                    string savePath = DataPaths.BrandsAssetsPath + "/" + brand.brand_id + "/hover";

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath+savePath, imageName);
                    brand.hover_image_filepath = savePath + "/" + imageName;
                }

            }

            foreach (Devices device in newDevices) {

                if (String.IsNullOrEmpty(device.img_filepath))
                {

                    string imageUrl = DataPaths.WS_URL + device.img_url;
                    string imageName = null;

                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

                    string savePath = DataPaths.DevicesAssetsPath + "/" + device.device_id;

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath+savePath, imageName);
                    device.img_filepath = savePath + "/" + imageName;
                    
                }

            }

            foreach (MobileOSs mobileOS in newMobileOSs) {

                if (String.IsNullOrEmpty(mobileOS.os_img_filepath))
                {

                    string imageUrl = DataPaths.WS_URL + mobileOS.os_img;
                    string imageName = null;

                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

                    string savePath = DataPaths.MobileOSsAssetsPath + "/" + mobileOS.os_id;

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath + savePath, imageName);
                    mobileOS.os_img_filepath = savePath + "/" + imageName;

                }

            }

            foreach (PlanTypes planType in newPlanTypes) {

                if (String.IsNullOrEmpty(planType.plan_img_filepath))
                {

                    string imageUrl = DataPaths.WS_URL + planType.plan_img;
                    string imageName = null;

                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

                    string savePath = DataPaths.PlanTypesAssetsPath + "/" + planType.type_plan;

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath + savePath, imageName);
                    planType.plan_img_filepath = savePath + "/" + imageName;

                }

            }

            foreach (Promotions promotion in newPromotions) {

                if (String.IsNullOrEmpty(promotion.button_url_filepath))
                {

                    string imageUrl = promotion.button_url;
                    string imageName = null;
                    
                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

                    string savePath = DataPaths.PromotionsAssetsPath + "/" + promotion.promotion_id + "/content";

                    // Check that we only download images! from promotion urls
                    string imageExtension = Path.GetExtension(imageName);
                    if (imageExtension == ".jpg" || imageExtension == ".jpeg" || imageExtension == ".png" || imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                    {
						NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath + savePath, imageName);
                        promotion.button_url_filepath = savePath + "/" + imageName;
                    }

                }

                if (String.IsNullOrEmpty(promotion.promotion_img_filepath))
                {

                    string imageUrl = DataPaths.WS_URL + promotion.promotion_img;
                    string imageName = null;

                    Uri uri = new Uri(imageUrl);
                    imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

                    string savePath = DataPaths.PromotionsAssetsPath + "/" + promotion.promotion_id + "/preview";

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath + savePath, imageName);
                    promotion.promotion_img_filepath = savePath + "/" + imageName;
                }
				if (String.IsNullOrEmpty(promotion.offer_pdf_filepath))
				{

					string imageUrl = DataPaths.WS_URL + promotion.offer_pdf;
					string imageName = null;

					Uri uri = new Uri(imageUrl);
					imageName = System.IO.Path.GetFileName(uri.AbsolutePath);

					string savePath = DataPaths.OffersPdfPath + "/" + promotion.promotion_id;

					Debug.Log ("**************Descargo"+Application.persistentDataPath+savePath);

					NetClient.Instance.AddDownloadFile(imageUrl, Application.persistentDataPath+savePath, imageName);
					promotion.offer_pdf = savePath + "/" + imageName;

				}

            }

            Action onAssetsDownload = delegate {
                // Do a database sync

                NotificationSystem.Instance.NotifyProgress("Sincronizando base de datos...");

                DatabaseManager.Instance.SyncDataBase(
                    newBrands, 
                    newDevices, 
                    newDevicePlans, 
                    newMobileOSs, 
                    newPlans, 
                    newPlanTypes,
                    newPrepaidPlans, 
                    newPrepaids,
#if R9
                    newPrepaidOptPlusPlans,
                    newAmigoFacilPlans,
#endif
                    newPromotions,
                    DatabaseManager.Instance.localDBConnection
                    );
                _dataVersion = _remoteDataVersion;
                DatabaseManager.Instance.CleanDatabase(DatabaseManager.Instance.localDBConnection, _dataVersion - 2);

                NotificationSystem.Instance.NotifyMessage("¡Datos actualizados!");
            };

            Action onAssetsDownloadFail = delegate {
                NotificationSystem.Instance.PromptAction(DataMessages.SERVER_DOWNLOAD_FAIL, 
                    delegate {
                        NotificationSystem.Instance.Terminate();
                        UpdateData(
                            newBrands, 
                            newDevices, 
                            newDevicePlans, 
                            newMobileOSs, 
                            newPlans, 
                            newPlanTypes,
                            newPrepaidPlans,
                            newPrepaids,
#if R9
                            newPrepaidOptPlusPlans,
                            newAmigoFacilPlans,
#endif
                            newPromotions
                            );
                    }, 
                    delegate {NotificationSystem.Instance.Terminate(); });
            };

            StartCoroutine(NetClient.Instance.DownloadFiles(onAssetsDownload, onAssetsDownloadFail));                                    

        }

#region DataPreparation for DatabaseUpdate

        List<Brands> PrepareDataForDatabaseSync(List<Brands> brands) {

            Debug.Log("DataManager => Preparing Brands for Database Sync");

            List<Brands> currentBrands = DatabaseManager.Instance.GetAllBrands(DatabaseManager.Instance.localDBConnection);
                        
            int currentBrandsCount = currentBrands != null ? currentBrands.Count : 0;
            
            foreach (Brands brand in brands) {

                Brands localBrand = DatabaseManager.Instance.GetBrandByBrandId(brand.brand_id, DatabaseManager.Instance.localDBConnection);

                brand.visible = true;

                if (localBrand != null)
                {

                    // if remote paths are equal then make brand local file paths equal
                    if (brand.normal_image_url == localBrand.normal_image_url)
                        brand.normal_image_filepath = localBrand.normal_image_filepath;

                    if (brand.hover_image_url == localBrand.hover_image_url)
                        brand.hover_image_filepath = localBrand.hover_image_filepath;

                    // Update the registered database id in order to allow to insert or replace
                    brand.ID = localBrand.ID;

                }
                else
                {
                    // About to insert a new brand
                    currentBrandsCount++;
                    brand.ID = currentBrandsCount;                    
                }

            }

            return brands;

        }

        List<Devices> PrepareDataForDatabaseSync(List<Devices> devices)
        {

            Debug.Log("DataManager => Preparing Devices for Database Sync");

            List<Devices> currentDevices = DatabaseManager.Instance.GetAllDevices(DatabaseManager.Instance.localDBConnection);

            int currentDevicesCount = currentDevices != null ? currentDevices.Count : 0;


            foreach (Devices device in devices)
            {

                Devices localDevice = DatabaseManager.Instance.GetDeviceByDeviceId(device.device_id, DatabaseManager.Instance.localDBConnection);

                if (localDevice != null)
                {

                    // if remote paths are equal then make local file paths equal
                    if (device.img_url == localDevice.img_url)
                        device.img_filepath = localDevice.img_filepath;

                    // Update the registered database id in order to allow to insert or replace
                    device.ID = localDevice.ID;
                }
                else
                {
                    // About to insert a new device
                    currentDevicesCount++;
                    device.ID = currentDevicesCount;
                }


            }


            return devices;

        }

        List<DevicePlans> PrepareDataForDatabaseSync(List<DevicePlans> devicesPlans)
        {

            Debug.Log("DataManager => Preparing DevicePlans for Database Sync");

            /*List<DevicePlans> currentDevicePlans = DatabaseManager.Instance.GetAllDevicePlans(DatabaseManager.Instance.localDBConnection);

            int currentDevicesCount = currentDevicePlans != null ? currentDevicePlans.Count : 0;

            foreach (DevicePlans devicePlan in devicesPlans)
            {
                               
                currentDevicesCount++;
                devicePlan.ID = currentDevicesCount;
                
            }*/

            return devicesPlans;

        }

        List<MobileOSs> PrepareDataForDatabaseSync(List<MobileOSs> mobileOSs)
        {

            Debug.Log("DataManager => Preparing MobileOSs for Database Sync");

            List<MobileOSs> currentMobileOSs = DatabaseManager.Instance.GetAllMobileOSs(DatabaseManager.Instance.localDBConnection);

            int currentMobileOSsCount = currentMobileOSs != null ? currentMobileOSs.Count : 0;

            foreach (MobileOSs mobileOS in mobileOSs)
            {

                MobileOSs localMobileOS = DatabaseManager.Instance.GetMobileOSByOSId(mobileOS.os_id, DatabaseManager.Instance.localDBConnection);

                if (localMobileOS != null)
                {

                    // if remote paths are equal then make local file paths equal
                    if (mobileOS.os_img == localMobileOS.os_img)
                        mobileOS.os_img_filepath = localMobileOS.os_img_filepath;

                    // Update the registered database id in order to allow to insert or replace
                    mobileOS.ID = localMobileOS.ID;
                }
                else
                {
                    // About to insert a new device
                    currentMobileOSsCount++;
                    mobileOS.ID = currentMobileOSsCount;
                }

            }

            return mobileOSs;

        }

        List<Plans> PrepareDataForDatabaseSync(List<Plans> plans)
        {

            Debug.Log("DataManager => Preparing Plans for Database Sync");

            List<Plans> currentPlans = DatabaseManager.Instance.GetAllPlans(DatabaseManager.Instance.localDBConnection);

            int currentPlansCount = currentPlans != null ? currentPlans.Count : 0;

            foreach (Plans plan in plans)
            {

                Plans localPlan = DatabaseManager.Instance.GetPlanByPlanId(plan.plan_id, DatabaseManager.Instance.localDBConnection);

                if (localPlan != null)
                {                                        
                    // Update the registered database id in order to allow to insert or replace
                    plan.ID = localPlan.ID;
                }
                else
                {
                    // About to insert a new device
                    currentPlansCount++;
                    plan.ID = currentPlansCount;
                }

            }

            return plans;

        }

        List<PlanTypes> PrepareDataForDatabaseSync(List<PlanTypes> planTypes)
        {

            Debug.Log("DataManager => Preparing PlanTypes for Database Sync");

            List<PlanTypes> currentPlanTypes = DatabaseManager.Instance.GetAllPlanTypes(DatabaseManager.Instance.localDBConnection);

            int currentPlanTypesCount = currentPlanTypes != null ? currentPlanTypes.Count : 0;

            foreach (PlanTypes planType in planTypes)
            {

                PlanTypes localPlanType = DatabaseManager.Instance.GetPlanType(planType.type_plan, DatabaseManager.Instance.localDBConnection);

                if (localPlanType != null)
                {

                    // if remote paths are equal then make local file paths equal
                    if (planType.plan_img == localPlanType.plan_img)
                        planType.plan_img_filepath = localPlanType.plan_img_filepath;

                    // Update the registered database id in order to allow to insert or replace
                    planType.ID = localPlanType.ID;
                }
                else
                {
                    // About to insert a new device
                    currentPlanTypesCount++;
                    planType.ID = currentPlanTypesCount;
                }

            }

            return planTypes;

        }

        List<PrepaidPlans> PrepareDataForDatabaseSync(List<PrepaidPlans> prepaidPlans)
        {

            Debug.Log("DataManager => Preparing PrepaidPlans for Database Sync");

            List<PrepaidPlans> currentPrepaidPlans = DatabaseManager.Instance.GetAllPrepaidPlans(DatabaseManager.Instance.localDBConnection);

            int currentPrepaidPlansCount = currentPrepaidPlans != null ? currentPrepaidPlans.Count : 0;

            foreach (PrepaidPlans prepaidPlan in prepaidPlans)
            {

                PrepaidPlans localPrepaidPlan = DatabaseManager.Instance.GetPrepaidPlanByPrepaidPlanId(prepaidPlan.id_amigo_recarga, DatabaseManager.Instance.localDBConnection);

                if (localPrepaidPlan != null)
                {
                    // Update the registered database id in order to allow to insert or replace
                    prepaidPlan.ID = localPrepaidPlan.ID;
                }
                else
                {
                    // About to insert a new device
                    currentPrepaidPlansCount++;
                    prepaidPlan.ID = currentPrepaidPlansCount;
                }

            }

            return prepaidPlans;

        }

        List<Prepaids> PrepareDataForDatabaseSync(List<Prepaids> prepaids)
        {

            Debug.Log("DataManager => Preparing Prepaids for Database Sync");

            List<Prepaids> currentPrepaids = DatabaseManager.Instance.GetAllPrepaids(DatabaseManager.Instance.localDBConnection);

            int curretPrepaidsCount = currentPrepaids != null ? currentPrepaids.Count : 0;

            foreach (Prepaids prepaid in prepaids)
            {

                Prepaids localPrepaid = DatabaseManager.Instance.GetPrepaidByPrepaidId(prepaid._id_reg_amigo, DatabaseManager.Instance.localDBConnection);

                if (localPrepaid != null)
                {                   
                    // Update the registered database id in order to allow to insert or replace
                    prepaid.ID = localPrepaid.ID;
                }
                else
                {
                    // About to insert a new device
                    curretPrepaidsCount++;
                    prepaid.ID = curretPrepaidsCount;
                }

            }

            return prepaids;

        }

        List<PrepaidOptPlusPlans> PrepareDataForDatabaseSync(List<PrepaidOptPlusPlans> prepaidOptPlusPlans)
        {

            Debug.Log("DataManager => Preparing PrepaidOptPlusPlans for Database Sync");

            List<PrepaidOptPlusPlans> currentPrepaidOptPlusPlans = DatabaseManager.Instance.GetAllPrepaidOptPlusPlans(DatabaseManager.Instance.localDBConnection);

            int currentPrepaidOptPlusPlansCount = currentPrepaidOptPlusPlans != null ? currentPrepaidOptPlusPlans.Count : 0;

            foreach (PrepaidOptPlusPlans prepaidOptPlusPlan in prepaidOptPlusPlans)
            {

                PrepaidOptPlusPlans localPrepaidOptPlusPlan = DatabaseManager.Instance.GetPrepaidOptPlusPlanByPrepaidOptPlusPlanId(prepaidOptPlusPlan._id_reg, DatabaseManager.Instance.localDBConnection);

                if (localPrepaidOptPlusPlan != null)
                {
                    // Update the registered database id in order to allow to insert or replace
                    prepaidOptPlusPlan.ID = localPrepaidOptPlusPlan.ID;
                }
                else
                {
                    // About to insert a new device
                    currentPrepaidOptPlusPlansCount++;
                    prepaidOptPlusPlan.ID = currentPrepaidOptPlusPlansCount;
                }

            }

            return prepaidOptPlusPlans;

        }

        List<AmigoFacilPlans> PrepareDataForDatabaseSync(List<AmigoFacilPlans> amigoFacilPlans)
        {

            Debug.Log("DataManager => Preparing AmigoFacilPlans for Database Sync");

            /*List<DevicePlans> currentDevicePlans = DatabaseManager.Instance.GetAllDevicePlans(DatabaseManager.Instance.localDBConnection);

            int currentDevicesCount = currentDevicePlans != null ? currentDevicePlans.Count : 0;

            foreach (DevicePlans devicePlan in devicesPlans)
            {
                               
                currentDevicesCount++;
                devicePlan.ID = currentDevicesCount;
                
            }*/

            return amigoFacilPlans;

        }

        List<Promotions> PrepareDataForDatabaseSync(List<Promotions> promotions)
        {

            Debug.Log("DataManager => Preparing Promotions for Database Sync");

            List<Promotions> currentPromotions = DatabaseManager.Instance.GetAllPromotions(DatabaseManager.Instance.localDBConnection);

            int currentPromotionsCount = currentPromotions != null ? currentPromotions.Count : 0;

            foreach (Promotions promotion in promotions)
            {

                Promotions localPromotion = DatabaseManager.Instance.GetPromotionByPromotionId(promotion.promotion_id, DatabaseManager.Instance.localDBConnection);

                if (localPromotion != null)
                {

                    // if remote paths are equal then make promotion local file paths equal
                    if (promotion.button_url == localPromotion.button_url)
                        promotion.button_url_filepath = localPromotion.button_url_filepath;

                    if (promotion.promotion_img == localPromotion.promotion_img)
                        promotion.promotion_img_filepath = localPromotion.promotion_img_filepath;
                   
                }
                
            }

            // Previous promotions must be deleted
            DatabaseManager.Instance.ClearAllEntriesFromTable<Promotions>(DatabaseManager.Instance.localDBConnection);

            return promotions;

        }

        #endregion


    }

}