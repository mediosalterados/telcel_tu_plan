using UnityEngine;
using System.Collections;
using System.Reflection;

using GLIB.Core;
using GLIB.Interface;
using GLIB.Utils;

using KiiCorp.Cloud.Storage;

using System;
using System.Collections.Generic;

public class SessionManager : BackModule<SessionManager> {
    
	public GoogleAnalyticsV3 googleAnalytics;

    List<IActivity> _activityViewHistory = new List<IActivity>();
    
    IActivity _lastActivity {
        get {
            if (_activityViewHistory.Count > 0)
                return _activityViewHistory[_activityViewHistory.Count - 1];
            else
                return null;
        }
    }

    SessionData _currentSessionData;
    public SessionData CurrentSessionData {
        get {

            if (_currentSessionData == null)
                _currentSessionData = new SessionData();

            return _currentSessionData;

        }
    }

    public void OpenActivity(IActivity activity) {

        if (!isRunning)
        {
            Debug.LogWarning("SessionManager wasn't running. Initializing.");
            Initialize();
        }

        activity.StartActivity();
        _activityViewHistory.Add(activity);
        Debug.Log("Opening Activity: " + activity.GetType().Name);
		googleAnalytics.LogScreen (activity.GetType().Name);

    }

    public void TerminateActivity(IActivity activity) {

        if (!isRunning)
        {
            Debug.LogWarning("SessionManager wasn't running. Initializing.");
            Initialize();
        }

        activity.EndActivity();
        _activityViewHistory.Remove(activity);
        Debug.Log("Terminating Activity: " + activity.GetType().Name);

    }

    protected override void ProcessInitialization()
    {
        Debug.Log("SessionManager => Started");

        // Set kii push notification handlers
        if (Application.isMobilePlatform) {
            KiiPushService.Instance.OnPushMessageReceived = delegate(ReceivedMessage receivedMessage) {
				string kiiKey= "";
				if(receivedMessage.GetString("MsgBody") != null){
					//Android
					kiiKey= receivedMessage.GetString("MsgBody");
				}else if(receivedMessage.GetString("body") != null){
					//iOS
					kiiKey= receivedMessage.GetString("body");
				}
                
                if(receivedMessage.GetString("deviceId") != null && receivedMessage.GetString("deviceId") != ""){
                    // ventana de notificación
                    NotificationSystem.Instance.PromptAction(kiiKey, 
                    delegate { 
                        SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_PLAN;
                        if(SessionManager.Instance.IsCurrentlyOpened(DeviceDetails.Instance.name) == false){
                        SessionManager.Instance.TryShuttingDownAllOpenedActivities();
                        SessionManager.Instance.SetHomeAsRoot();
                        SessionManager.Instance.CurrentSessionData.SelectedDeviceId = receivedMessage.GetString("deviceId");
                        StartCoroutine(OpenActivity());
                        }else{
                            SessionManager.Instance.TryShuttingDownAllOpenedActivities();
                            SessionManager.Instance.SetHomeAsRoot();
                            SessionManager.Instance.CurrentSessionData.SelectedDeviceId = receivedMessage.GetString("deviceId");
                            SessionManager.Instance.OpenActivity(DeviceDetails.Instance);
                        }
                        NotificationSystem.Instance.Terminate();
                    }, delegate { NotificationSystem.Instance.Terminate(); });

                }else{
                    // Notificación normal, en caso de no traer ID del dispositivo
                    NotificationSystem.Instance.NotifyMessage(kiiKey);
                }

            //fin de instancia    
            };

            KiiPushService.Instance.OnBackgroundPushMessageReceived = delegate (ReceivedMessage receivedMessage)
            {
                string kiiKey= "";
                if(receivedMessage.GetString("MsgBody") != null){
                    //Android
                    kiiKey= receivedMessage.GetString("MsgBody");
                }else if(receivedMessage.GetString("body") != null){
                    //iOS
                    kiiKey= receivedMessage.GetString("body");
                }
                
                if(receivedMessage.GetString("deviceId") != null && receivedMessage.GetString("deviceId") != ""){
                    // ventana de notificación
                    NotificationSystem.Instance.PromptAction(kiiKey, 
                    delegate { 
                        SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_PLAN;
                        if(SessionManager.Instance.IsCurrentlyOpened(DeviceDetails.Instance.name) == false){
                        SessionManager.Instance.TryShuttingDownAllOpenedActivities();
                        SessionManager.Instance.SetHomeAsRoot();
                        SessionManager.Instance.CurrentSessionData.SelectedDeviceId = receivedMessage.GetString("deviceId");
                        StartCoroutine(OpenActivity());
                        }else{
                            SessionManager.Instance.TryShuttingDownAllOpenedActivities();
                            SessionManager.Instance.SetHomeAsRoot();
                            SessionManager.Instance.CurrentSessionData.SelectedDeviceId = receivedMessage.GetString("deviceId");
                            SessionManager.Instance.OpenActivity(DeviceDetails.Instance);
                        }
                        NotificationSystem.Instance.Terminate();
                    }, delegate { NotificationSystem.Instance.Terminate(); });

                }else{
                    // Notificación normal, en caso de no traer ID del dispositivo
                    NotificationSystem.Instance.NotifyMessage(kiiKey);
                }
            };
        }
    }

    IEnumerator OpenActivity(){
        yield return new WaitForSeconds (0.3f);

        SessionManager.Instance.OpenActivity(DeviceDetails.Instance);
    }
    // IEnumerator OpenNotification(String msg, String deviceId){
    //     yield return new WaitForSeconds (1f);

    //     NotificationSystem.Instance.NotifyMessage("Has Recibido un mensaje en Background!!!\n" + msg + " | "+ deviceId);
    // }

    protected override void ProcessTermination()
    {
        Debug.Log("SessionManager => Terminated");
    }

    protected override void ProcessUpdate()
    {
        
    }

    public void GoToPreviousActivity() {

        if (_activityViewHistory.Count > 1 && _lastActivity.isActivityReady)
        {
            TerminateActivity(_lastActivity);

            // Now that the last activity has been deleted, the previous becomes the last one so that we can reference it.
            IActivity previousActivity = _lastActivity;

            previousActivity.StartActivity();

            Debug.Log("SessionManager => Starting Activity: " + previousActivity.GetType().Name);
        }

    }    

	public bool IsCurrentlyOpened(String activity){
		IActivity currentActivity = _activityViewHistory [_activityViewHistory.Count-1];
		return  activity == currentActivity.GetType ().Name ? true:false;
	}

    public void ClearSession( bool preserveFirstActivityInViewHistory = true ) {

        IActivity firstActivity = null;

        if (preserveFirstActivityInViewHistory && _activityViewHistory.Count > 0) {
            firstActivity = _activityViewHistory[0];
        }  

        _currentSessionData = new SessionData();
        _activityViewHistory.Clear();

        if (firstActivity != null) {
            _activityViewHistory.Add(firstActivity);
        }
                
    }

	public void SetHomeAsRoot(){
		_currentSessionData = new SessionData();
		_activityViewHistory.Clear();

		_activityViewHistory.Add (Home.Instance);
	}

    public void TryShuttingDownAllOpenedActivities() {

        foreach (IActivity activity in _activityViewHistory)
            activity.EndActivity();

    }

//	public void LogActivity(String type,String label,String title,long value){
//		googleAnalytics.LogEvent(type,label,title,value);
//	}

    
}
