﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using LitJson;

using GLIB.Core;

using TuPlan.Models.Database;

public class DataDeserializer : Singleton<DataDeserializer> {

    public int DeserializeDataVersion(string text) {

        JsonData dataVersion = JsonMapper.ToObject(text);

        int version = (int)dataVersion["version_id"];

        return version;

    }

    public List<Brands> DeserializeBrands(string rawString) {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<Brands> brands = new List<Brands>();

            Debug.Log("DataDeserializer => Deserializing Brands string: "+ rawString);

            foreach (JsonData elem in rawData as IList)
            {

                Brands brand = new Brands();

                brand.brand_id = (string)elem["brand_id"];
                brand.brand_name = (string)elem["brand_name"];

                JsonData rawImgs = (JsonData)elem["brand_img"];

                brand.normal_image_url = ResolveHighestDPIImage("type", "normal", rawImgs);
                brand.hover_image_url = ResolveHighestDPIImage("type", "hover", rawImgs);

                brands.Add(brand);
                
            }

            return brands;
        }
        catch (JsonException e) {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }
    }

    public List<Devices> DeserializeDevices(string rawString) {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<Devices> devices = new List<Devices>();

            Debug.Log("DataDeserializer => Deserializing Devices string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

			

                Devices device = new Devices();

                device._id = (int)elem["_id"];
#if DEUR
                device.device_id = (int)elem["device_id"];
#elif R9
                device.device_id = (string)elem["device_id"];
#endif

                device.brand_id = (string)elem["brand_id"];
                device.os_id = (string)elem["os_id"];
                device.model_name = (string)elem["model_name"];
                device.device_name = (string)elem["device_name"];
                device.cpu = (string)elem["cpu"];
                device.snapdragon = (bool)elem["snapdragon"];
                device.lte = (bool)elem["lte"];
                device.cores = (int)elem["cores"];
                device.speed = (string)elem["speed"];
                device.ram = (string)elem["ram"];
                device.storage = (string)elem["storage"];

				int sto = (int)elem["storage_int"];
				device.storage_int = sto;

                device.front_camera = (string)elem["front_camera"];
                device.back_camera = (string)elem["back_camera"];

				int f1 = (int)elem["front_camera_float"];
				int f2 = (int)elem["back_camera_float"];

				device.front_camera_float = f1;
				device.back_camera_float = f2;

				Debug.Log(f1+" :HAS: " + f2);

                device.display = (string)elem["display"];
				int disp = (int)elem["display_int"];
				device.display_int = disp;



                device.prepaid_cost = (int)elem["prepaid_cost"];
                device.version_id = (int)elem["version_id"];
				device.submodel_cpu = (string)elem["submodel_cpu"];

				if((string)elem["description"] != ""){
					device.description = (string)elem["description"];
				}

				if((int)elem["position"] > 0){
				device.position = (int)elem["position"];  
				}

				device.network = (string)elem["network"] != "" ? (string)elem["network"]:"";



                JsonData rawImgs = (JsonData)elem["device_img"];

                device.img_url = ResolveHighestDPIImage("type", "location", rawImgs);
                
                devices.Add(device);

            }

            return devices;
        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<DevicePlans> DeserializeDevicePlans(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<DevicePlans> devicePlans = new List<DevicePlans>();

            Debug.Log("DataDeserializer => Deserializing DevicePlans string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                DevicePlans devicePlan = new DevicePlans();

                devicePlan.device_id = (string)elem["device_id"];
                devicePlan.plan_id = (string)elem["plan_id"];
                devicePlan.months = (int)elem["months"];
                devicePlan.cost = (string)elem["cost"];
                devicePlan.additional_cost = (string)elem["additional_cost"];
                devicePlan.version_id = (int)elem["version_id"];
                                                
                devicePlans.Add(devicePlan);

            }

            return devicePlans;
        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<MobileOSs> DeserializeMobileOSs(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<MobileOSs> mobileOSs = new List<MobileOSs>();

            Debug.Log("DataDeserializer => Deserializing Mobile OSs string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                MobileOSs mobileOS = new MobileOSs();

                mobileOS.os_id = (string)elem["os_id"];
                mobileOS.os_name = (string)elem["os_name"];
                mobileOS.os_version = (string)elem["os_version"];
                
                JsonData rawImgs = (JsonData)elem["os_img"];

                mobileOS.os_img = ResolveHighestDPIImage("type", "location", rawImgs);
                

                mobileOSs.Add(mobileOS);

            }

            return mobileOSs;

        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<Plans> DeserializePlans(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<Plans> plans = new List<Plans>();

            Debug.Log("DataDeserializer => Deserializing Plans string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                Plans plan = new Plans();

                plan.plan_id = (string)elem["plan_id"];
                plan.plan_name = (string)elem["plan_name"];
                plan.monthly_cost = (string)elem["monthly_cost"];
                plan.minutes_inc = (int)elem["minutes_inc"];
                plan.free_numbers = (int)elem["free_numbers"];
                plan.sms_inc = (int)elem["sms_inc"];
                plan.megabytes_inc = (int)elem["megabytes_inc"];
                plan.megabytes_extra_cost = (string)elem["megabytes_extra_cost"];
                plan.minutes_extra_cost = (string)elem["minutes_extra_cost"];
                plan.sms_extra_cost = (string)elem["sms_extra_cost"];
                plan.social_networks = (bool)elem["social_networks"];
                plan.additional_cost = (string)elem["additional_cost"];
#if R9
                plan.additional_cost_iphone = (string)elem["additional_cost_iphone"];
#endif
                plan.sin_frontera = (string)elem["sin_frontera"];
                plan.version_id = (int)elem["version_id"];
                plan.tipo_plan = (string)elem["tipo_plan"];
				plan.promotion = (string)elem["promotion"];
				plan.legals = (string)elem["legals"];
                
                plans.Add(plan);

            }

            return plans;
        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<PlanTypes> DeserializePlanTypes(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<PlanTypes> planTypes = new List<PlanTypes>();

            Debug.Log("DataDeserializer => Deserializing Plan Types string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                PlanTypes planType = new PlanTypes();
                
                planType.nombre_plan = (string)elem["nombre_plan"];
                planType.type_plan = (string)elem["type_plan"];
                planType.eu_canada = (bool)elem["eu_canada"];
                planType.prioridad = (string)elem["prioridad"];
                
                JsonData rawImgs = (JsonData)elem["plan_img"];

                planType.plan_img = ResolveHighestDPIImage("resolution_id", "location", rawImgs);
                
                planTypes.Add(planType);

            }

            return planTypes;

        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<PrepaidPlans> DeserializePrepaidPlans(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<PrepaidPlans> prepaidPlans = new List<PrepaidPlans>();

            Debug.Log("DataDeserializer => Deserializing PrepaidPlans string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                PrepaidPlans prepaidPlan = new PrepaidPlans();

                prepaidPlan.id_amigo_recarga = (int)elem["id_amigo_recarga"];
#if R9
                prepaidPlan.prepaid_type = (string)elem["prepaid_type"];
#endif
                prepaidPlan.nombre_amigo_recarga = (string)elem["nombre_amigo_recarga"];
                prepaidPlan.minutos = (int)elem["minutos"];
                prepaidPlan.sms = (int)elem["sms"];
                prepaidPlan.megabytes = (int)elem["megabytes"];
                prepaidPlan.vigencia = (int)elem["vigencia"];
                prepaidPlan.recarga = (string)elem["recarga"];
                //prepaidPlan.version_id = int.Parse((string)elem["version_id"]);

                prepaidPlans.Add(prepaidPlan);

            }

            return prepaidPlans;
        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<Prepaids> DeserializePrepaids(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<Prepaids> prepaids = new List<Prepaids>();

            Debug.Log("DataDeserializer => Deserializing Plan Types string: " + rawString);
#if DEUR

            Prepaids prepaid = new Prepaids();
           
            prepaid._id_reg_amigo = (int)rawData["_id_reg_amigo"];
            prepaid.free_numbers = (int)rawData["free_numbers"];
            prepaid.minute_carrier_cost = (string)rawData["minute_carrier_cost"];
            prepaid.minute_others_cost = (string)rawData["minute_others_cost"];
            prepaid.minute_world_cost = (string)rawData["minute_world_cost"];
            prepaid.minute_land_cost = (string)rawData["minute_land_cost"];
            prepaid.megabyte_cost = (string)rawData["megabyte_cost"];
            prepaid.sms_cost = (string)rawData["sms_cost"];
            prepaid.frequent_numbers = (int)rawData["frequent_numbers"];
            prepaid.cost_frequent_numbers = (string)rawData["cost_frequent_numbers"];
            //prepaid.prepaid_name = (string)rawData["prepaid_name"];
            //prepaid.prepaid_type = (string)rawData["prepaid_type"];
            //prepaid.value = (string)rawData["value"];

            prepaids.Add(prepaid);

#elif R9           

            foreach (JsonData elem in rawData as IList)
            {

                Prepaids prepaid = new Prepaids();

                prepaid._id_reg_amigo = (int)elem["_id_reg_amigo"];
                prepaid.free_numbers = (int)elem["free_numbers"];
                prepaid.minute_carrier_cost = (string)elem["minute_carrier_cost"];
                prepaid.minute_others_cost = (string)elem["minute_others_cost"];
                prepaid.minute_world_cost = (string)elem["minute_world_cost"];
                prepaid.minute_land_cost = (string)elem["minute_land_cost"];
                prepaid.megabyte_cost = (string)elem["megabyte_cost"];
                prepaid.sms_cost = (string)elem["sms_cost"];
                prepaid.frequent_numbers = (int)elem["frequent_numbers"];
                prepaid.cost_frequent_numbers = (string)elem["cost_frequent_numbers"];
                prepaid.prepaid_name = (string)elem["prepaid_name"];
                prepaid.prepaid_type = (string)elem["prepaid_type"];
                prepaid.value = (string)elem["value"];

                prepaids.Add(prepaid);

            }
#endif

            return prepaids;

        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<Promotions> DeserializePromotions(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<Promotions> promotions = new List<Promotions>();

            Debug.Log("DataDeserializer => Deserializing Promotions string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                Promotions promotion = new Promotions();

                promotion.promotion_id = (int)elem["promotion_id"];
                promotion.title = (string)elem["title"];
                promotion.subtitle = (string)elem["subtitle"];
                promotion.detail = (string)elem["detail"];
                promotion.button_text = (string)elem["button_text"];
                promotion.button_url = (string)elem["button_url"];

                JsonData rawPreviewImgs = (JsonData)elem["promotion_img"];
                promotion.promotion_img = ResolveHighestDPIImage("type", "location", rawPreviewImgs);
				promotion.offer_pdf = (string)elem["offer_pdf"];

                promotions.Add(promotion);

            }

            return promotions;

        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<PrepaidOptPlusPlans> DeserializePrepaidOptPlusPlans(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<PrepaidOptPlusPlans> prepaidOptPlusPlans = new List<PrepaidOptPlusPlans>();

            Debug.Log("DataDeserializer => Deserializing PrepaidOptPlusPlans string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                PrepaidOptPlusPlans prepaidOptPlusPlan = new PrepaidOptPlusPlans();

                prepaidOptPlusPlan._id_reg = (int)elem["_id_reg"];
                prepaidOptPlusPlan.recarga_saldo = (int)elem["recarga_saldo"];
                prepaidOptPlusPlan.saldo_promocional = (int)elem["saldo_promocional"];
                prepaidOptPlusPlan.saldo_total = (int)elem["saldo_total"];
                prepaidOptPlusPlan.redes_sociales = (bool)elem["redes_sociales"];
                prepaidOptPlusPlan.whatsapp = (bool)elem["whatsapp"];
                prepaidOptPlusPlan.vigencia_saldo = (int)elem["vigencia_saldo"];
                prepaidOptPlusPlan.vigencia_redes_sociales = (int)elem["vigencia_redes_sociales"];
                prepaidOptPlusPlan.rate_id = (int)elem["rate_id"];

                prepaidOptPlusPlans.Add(prepaidOptPlusPlan);

            }

            return prepaidOptPlusPlans;
        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    public List<AmigoFacilPlans> DeserializeAmigoFacilPlans(string rawString)
    {

        try
        {
            JsonData rawData = JsonMapper.ToObject(rawString);

            List<AmigoFacilPlans> amigoFacilPlans = new List<AmigoFacilPlans>();

            Debug.Log("DataDeserializer => Deserializing AmigoFacilPlans string: " + rawString);

            foreach (JsonData elem in rawData as IList)
            {

                AmigoFacilPlans amigoFacilPlan = new AmigoFacilPlans();

                amigoFacilPlan._id_reg = (int)elem["_id_reg"];
                amigoFacilPlan.anticipo = (string)elem["anticipo"];
                amigoFacilPlan.renta_mensual = (string)elem["renta_mensual"];
                amigoFacilPlan.renta_anual = ((int)elem["renta_anual"]).ToString();
                amigoFacilPlan.kit_credito = (string)elem["kit_credito"];
                amigoFacilPlan.id_device = (string)elem["id_device"];
                amigoFacilPlan.version_id = int.Parse((string)elem["version_id"]);

                amigoFacilPlans.Add(amigoFacilPlan);

            }

            return amigoFacilPlans;
        }
        catch (JsonException e)
        {
            Debug.LogError("JSON Exception: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }

    }

    string ResolveHighestDPIImage(string serializedImageTypeName, string serializedImageFieldName, JsonData rawObject) {

        try
        {

            // Get the first element as fallback
            string highestDPIImage = (string)rawObject[0][serializedImageFieldName];
		
            /*foreach (JsonData image in rawObject as IList)
            {
                string type = (string)image[serializedImageTypeName];
                if (type == "xxxhdpi")
                    return (string)image[serializedImageFieldName];

            }*/

            foreach (JsonData image in rawObject as IList)
            {
                string type = (string)image[serializedImageTypeName];
                if (type == "xxhdpi")
                    return (string)image[serializedImageFieldName];

            }

            foreach (JsonData image in rawObject as IList)
            {
                string type = (string)image[serializedImageTypeName];
                if (type == "xhdpi")
                    return (string)image[serializedImageFieldName];

            }

            foreach (JsonData image in rawObject as IList)
            {
                string type = (string)image[serializedImageTypeName];
                if (type == "hdpi")
                    return (string)image[serializedImageFieldName];

            }

            foreach (JsonData image in rawObject as IList)
            {
                string type = (string)image[serializedImageTypeName];
                if (type == "mdpi")
                    return (string)image[serializedImageFieldName];

            }

            foreach (JsonData image in rawObject as IList)
            {
                string type = (string)image[serializedImageTypeName];
                if (type == "ldpi")
                    return (string)image[serializedImageFieldName];

            }

            return highestDPIImage;
        }
        catch (JsonException e)
        {
            Debug.LogError("Couldn't retrieve highest DPI image: " + e.Message + "\n" + e.StackTrace);
            throw e;
        }
        catch (ArgumentOutOfRangeException ae) {
            Debug.LogError("No Image found within object given");
            return "";
        }
        

    }

}
