﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;

using TuPlan.Controllers.DataManagement;

using VoxelBusters.NativePlugins;
using VoxelBusters.Utility;

public class AmigoSinLimiteQuotationResult : UIModule<AmigoSinLimiteQuotationResult>, IActivity
{

	public enum AmigoModeValues {
		BY_DEFAULT = 0,
		BY_SIN_LIMITE = 1
	}

	public AmigoModeValues AmigoMode = AmigoModeValues.BY_DEFAULT;
	Prepaids current_prepaid_plan;

    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_AMIGOSINLIMITE_QUOTATIONRESULT_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
            
        }
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void StartActivity()
    {
        Initialize();
    }

    protected override void ProcessInitialization()

	{

		GameObject relatedDevicesWrapper = this.Find("RelatedDevices<Wrapper>", DisplayObject);
		GameObject related = this.Find("Item 1", DisplayObject);


		String LegalesShare = "";

		GameObject featureBB = this.Find ("FeatureBB<Wrapper>", DisplayObject);
		featureBB.SetActive (false);
		

		Image uberIcon = this.FindAndResolveComponent<Image>("Uber<Image>", DisplayObject);
		uberIcon.gameObject.SetActive (false);


	
		if (SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL) {
			AmigoMode = AmigoModeValues.BY_SIN_LIMITE;
		} else {
			AmigoMode = AmigoModeValues.BY_DEFAULT;
		}



        // Get required entities
        Devices device = DatabaseManager.Instance.GetDeviceByDeviceId(SessionManager.Instance.CurrentSessionData.SelectedDeviceId, DatabaseManager.Instance.localDBConnection);

		Brands brand = DatabaseManager.Instance.GetBrandByBrandId(device.brand_id, DatabaseManager.Instance.localDBConnection);
        PrepaidPlans prepaidPlan = DatabaseManager.Instance.GetPrepaidPlanByPrepaidPlanId(int.Parse(SessionManager.Instance.CurrentSessionData.SelectedPlanId), DatabaseManager.Instance.localDBConnection);
        

		PrepaidOptPlusPlans optPlan = DatabaseManager.Instance.GetPrepaidOptPlusPlanByPrepaidOptPlusPlanId(int.Parse(SessionManager.Instance.CurrentSessionData.SelectedPlanId), DatabaseManager.Instance.localDBConnection);

		Text promotionsText = this.FindAndResolveComponent<Text>("Promo<Text>", DisplayObject);

		//REVISAR ESTA INFORMACIÓN SI NO ES PLAN AMIGO
		Plans plan = DatabaseManager.Instance.getBestPlanFromAmigo(device, 24, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);
		List<DevicePlans> devicePlans = plan != null ? DatabaseManager.Instance.GetPlanFromDevice(device, plan, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection):null;

		Image snapchatAppIcon = this.FindAndResolveComponent<Image>("SnapChat<Image>", DisplayObject);
		Image instagramAppIcon = this.FindAndResolveComponent<Image>("Instagram<Image>", DisplayObject);

		snapchatAppIcon.gameObject.SetActive (false);
		instagramAppIcon.gameObject.SetActive (false);



		Text planName = this.FindAndResolveComponent<Text> ("PlanName<Text>", DisplayObject);
        // Initialize activity components
		if (prepaidPlan != null) {
			planName.text = prepaidPlan.nombre_amigo_recarga;

			Debug.Log ("Asignare titulo en:" + prepaidPlan.nombre_amigo_recarga);
		}

	

        Image phoneThumb = this.FindAndResolveComponent<Image>("PhoneThumb<Image>", DisplayObject);

		StartCoroutine(loadAssets(Application.persistentDataPath + device.img_filepath,phoneThumb));

        //StartCoroutine(LoadCacheTextureIntoImage(phoneThumb, DataPaths.WS_URL + device.img_url));

		Image phoneLTE = this.FindAndResolveComponent<Image>("PhoneLTE<Image>", DisplayObject);
		Texture2D tex_net = null;

		switch (device.network) {
		case "VO":
			tex_net = (Texture2D)Resources.Load ("Sprites/volte");
			break;
		case "4G":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		case "":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		}

		if (tex_net != null) {
			Sprite sprite_net = Sprite.Create(tex_net, new Rect(0, 0, tex_net.width, tex_net.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);

			phoneLTE.sprite = sprite_net;
		}
		phoneLTE.gameObject.SetActive(device.lte);

	
        Text phoneName = this.FindAndResolveComponent<Text>("PhoneModel<Text>", DisplayObject);
        phoneName.text = device.model_name;

        Text phoneBrand = this.FindAndResolveComponent<Text>("PhoneBrand<Text>", DisplayObject);
        phoneBrand.text = brand.brand_name;

        Text priceValue = this.FindAndResolveComponent<Text>("PriceValue<Text>", DisplayObject);

		AmigoFacilPlans plan_facil = DatabaseManager.Instance.GetAmigoDevicesPlan (device.device_id, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);

		string pricing = SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL && plan_facil != null ? plan_facil.kit_credito.ToString():device.prepaid_cost.ToString();

        priceValue.text = "$ "+string.Format("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal(pricing));


		if (prepaidPlan != null) {
			Slider costSlider = this.FindAndResolveComponent<Slider> ("Cost<Slider>", DisplayObject);
			costSlider.minValue = 0;
			costSlider.maxValue = float.Parse (DatabaseManager.Instance.GetLimitRecargaValueFromAllPrepaidPlans (false, DatabaseManager.Instance.localDBConnection));
			costSlider.value = float.Parse (prepaidPlan.recarga);
			costSlider.enabled = false;

			Text costValue = this.FindAndResolveComponent<Text> ("CostValue<Text>", DisplayObject);
			costValue.text = string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (prepaidPlan.recarga));

			Slider mbSlider = this.FindAndResolveComponent<Slider> ("MB<Slider>", DisplayObject);
			mbSlider.minValue = 0;
			mbSlider.maxValue = (float)DatabaseManager.Instance.GetLimitMegabytesValueFromAllPrepaidPlans (false, DatabaseManager.Instance.localDBConnection);
			mbSlider.value = (float)prepaidPlan.megabytes;
			mbSlider.enabled = false;

			Text mbValue = this.FindAndResolveComponent<Text> ("MBValue<Text>", DisplayObject);
			mbValue.text = string.Format("{0:#,###}", Convert.ToDecimal(prepaidPlan.megabytes)) + DataMessages.GENERIC_PLAN_MB_LABEL_B;

			//expiration.text = DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_A + prepaidPlan.vigencia + DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_B;
		}
	
        Image twitterIcon = this.FindAndResolveComponent<Image>("Twitter<Image>", DisplayObject);
        Image facebookIcon = this.FindAndResolveComponent<Image>("Facebook<Image>", DisplayObject);
        Image faceMessengerIcon = this.FindAndResolveComponent<Image>("FaceMessenger<Image>", DisplayObject);
        Image whatsappIcon = this.FindAndResolveComponent<Image>("Whatsapp<Image>", DisplayObject);

		Text vigenciaValue = this.FindAndResolveComponent<Text>("ExpirationText<Text>", DisplayObject);

		if(vigenciaValue != null && prepaidPlan != null){
		vigenciaValue.text = "Vigencia de "+ prepaidPlan.vigencia +" días";
		}

		if (AmigoMode == AmigoModeValues.BY_DEFAULT) {

			Text planAdditionalInfo = this.FindAndResolveComponent<Text> ("PlanAdditionalRowTitle<Text>", DisplayObject);
			planAdditionalInfo.text = "Pago inicial en" + plan.plan_name;

			GameObject w12MonthWrapper = this.Find ("12Months<Wrapper>", DisplayObject);
			Text t12MonthText = this.FindAndResolveComponent<Text> ("12MonthValue<Text>", DisplayObject);
			GameObject separator12to24 = this.Find ("Separator12to24<Image>", DisplayObject);
			GameObject w24MonthWrapper = this.Find ("24Months<Wrapper>", DisplayObject);
			Text t24MonthText = this.FindAndResolveComponent<Text> ("24MonthValue<Text>", DisplayObject);
			GameObject separator24to36 = this.Find ("Separator24to36<Image>", DisplayObject);
			GameObject w36MonthWrapper = this.Find ("36Months<Wrapper>", DisplayObject);
			Text t36MonthText = this.FindAndResolveComponent<Text> ("36MonthValue<Text>", DisplayObject);
        

			Text ta12MonthText = this.FindAndResolveComponent<Text>("12AditionalMonthValue<Text>", DisplayObject);
			Text ta24MonthText = this.FindAndResolveComponent<Text>("24AditionalMonthValue<Text>", DisplayObject);
			Text ta36MonthText = this.FindAndResolveComponent<Text>("36AditionalMonthValue<Text>", DisplayObject);
			w12MonthWrapper.SetActive (false);
			separator12to24.SetActive (false);
			w24MonthWrapper.SetActive (false);
			separator24to36.SetActive (false);
			w36MonthWrapper.SetActive (false);
        
			if (devicePlans != null && devicePlans.Count >= 1) {
				w12MonthWrapper.SetActive (true);
				t12MonthText.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (devicePlans [0].cost));
				decimal decimal_value = Convert.ToDecimal (devicePlans [0].cost);
				string string_cost = decimal_value > 0 ? string.Format ("{0:#,### ; #.## ,00##,00}", decimal_value):"0";
				decimal_value = Convert.ToDecimal (devicePlans [0].additional_cost);
				string_cost = decimal_value > 0 ? string.Format ("{0:#,###.## ; ##.## ,00##,00.##}", decimal_value):"0";
				ta12MonthText.text = "$ " + string_cost;

				if (devicePlans.Count >= 2) {
					separator12to24.SetActive (true);
					w24MonthWrapper.SetActive (true);
					t24MonthText.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (devicePlans [1].cost));

					decimal_value = Convert.ToDecimal (devicePlans [1].additional_cost);
					string_cost = decimal_value > 0 ? string.Format ("{0:#,###.## ; ##.## ,00##,00.##}", decimal_value):"0";
					ta24MonthText.text = "$ " + string_cost;

					if (devicePlans.Count >= 3) {
						separator24to36.SetActive (true);
						w36MonthWrapper.SetActive (true);
						t36MonthText.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (devicePlans [2].cost));
						decimal_value = Convert.ToDecimal (devicePlans [2].additional_cost);
						string_cost = decimal_value > 0 ? string.Format ("{0:#,###.## ; ##.## ,00##,00.##}", decimal_value):"0";
						ta36MonthText.text = "$ " + string_cost;
					}

				}

			}

			// For now we turn off Amigo Facil Section
			GameObject amigoFacilRow = this.Find ("AmigoFacilRow<Wrapper>", DisplayObject);
			amigoFacilRow.SetActive (false);

			GameObject planAdditionalCostRow = this.Find ("PlanAdditionalCostRow<Wrapper>", DisplayObject);
			planAdditionalCostRow.SetActive (false);
		
		}else if(AmigoMode == AmigoModeValues.BY_SIN_LIMITE){

			//APAGAR COSTO POR MES
			GameObject planAdditionalCostRow = this.Find ("PlanAdditionalCostRow<Wrapper>", DisplayObject);
			planAdditionalCostRow.SetActive (false);

			AmigoFacilPlans amigo_plan = DatabaseManager.Instance.GetAmigoDevicesPlan (device.device_id, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);
			GameObject planAdditionalRow = this.Find ("PlanAdditionalRow<Wrapper>", DisplayObject);
			planAdditionalRow.SetActive (false);

			if (amigo_plan != null) {
				Text MonthlyPaymentValue = this.FindAndResolveComponent<Text> ("MonthlyPaymentValue<Text>", DisplayObject);
				MonthlyPaymentValue.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (amigo_plan.renta_mensual));

				Text AdvancePaymentValue = this.FindAndResolveComponent<Text> ("AdvancePaymentValue<Text>", DisplayObject);
				AdvancePaymentValue.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (amigo_plan.anticipo));

				Text TermValue = this.FindAndResolveComponent<Text> ("TermValue<Text>", DisplayObject);
				TermValue.text = "12 meses";


				SessionManager.Instance.CurrentSessionData.ShareMessage = "Llévate el " + device.model_name + " en Amigo Fácil con " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (amigo_plan.anticipo)) + "  pesos y 12 pagos mensuales de " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (amigo_plan.renta_mensual)) + "pesos. \n *Precios sujetos a cambio sin previo aviso.";
				SessionManager.Instance.CurrentSessionData.ShareEmail = "Llévate el " + device.model_name + " en Amigo Fácil con " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (amigo_plan.anticipo)) + "  pesos y 12 pagos mensuales de " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (amigo_plan.renta_mensual)) + "pesos. \n *Precios sujetos a cambio sin previo aviso.";
			}
			}





		if(SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme == SessionData.AmigoSchemeSelectionValues.AMIGO_OPTIMO_PLUS){
			GameObject secondarySliders = this.Find ("SecondarySliders<Wrapper>", DisplayObject);
			secondarySliders.SetActive (false);

			GameObject rowC = this.Find("RowC<Wrapper>", DisplayObject);
			rowC.SetActive (false);

			GameObject promotionPrice = this.Find ("PromotionPrice<Wrapper>", DisplayObject);
			promotionPrice.SetActive (false);


		}else{


			GameObject planResume = this.Find("PlanResume<Wrapper>", DisplayObject);
			planResume.SetActive (false);


			List<PrepaidOptPlusPlans> _amigoSinLimitePlans = new List<PrepaidOptPlusPlans>();
			_amigoSinLimitePlans = DatabaseManager.Instance.GetAllPrepaidOptPlusPlans(DatabaseManager.Instance.localDBConnection);


			current_prepaid_plan = DatabaseManager.Instance.GetPrepaidByPrepaidId(optPlan.rate_id,DatabaseManager.Instance.localDBConnection);

			GameObject mainSliders = this.Find ("MainSliders<Wrapper>", DisplayObject);
			mainSliders.SetActive (false);

			Slider costSlider = this.FindAndResolveComponent<Slider> ("RecargaCost<Slider>", DisplayObject);
			costSlider.enabled = false;

			Text RecargaValue = this.FindAndResolveComponent<Text>("RecargaValue<Text>", DisplayObject);
			RecargaValue.text = "$ "+optPlan.recarga_saldo.ToString();

			Text MBXValue = this.FindAndResolveComponent<Text>("MBTextValue<Text>", DisplayObject);
			MBXValue.text = "$ "+current_prepaid_plan.megabyte_cost.ToString();

			Text SMSValue = this.FindAndResolveComponent<Text>("SMSTextValue<Text>", DisplayObject);
			SMSValue.text = "$ "+current_prepaid_plan.sms_cost.ToString();

			Text MinValue = this.FindAndResolveComponent<Text>("MinTextValue<Text>", DisplayObject);
			MinValue.text = "$ "+current_prepaid_plan.minute_carrier_cost.ToString();


			planName.text = "Amigo Óptimo Plus de: $" + optPlan.recarga_saldo;

			int maxCost = int.MinValue;

			foreach (PrepaidOptPlusPlans amigoPlan in _amigoSinLimitePlans) {
				int amigoCost = amigoPlan.recarga_saldo;
				if (amigoCost > maxCost)
					maxCost = amigoCost;
			}



			costSlider.maxValue = maxCost;
			costSlider.value = optPlan.recarga_saldo;

			GameObject quotationFeatures = this.Find ("QuotationMainFeatures<Wrapper>", DisplayObject);
			quotationFeatures.SetActive (false);


			Text expirationSaldoText = this.FindAndResolveComponent<Text>("ExpirationSaldoText<Text>", DisplayObject);
			expirationSaldoText.text = "$"+optPlan.saldo_total.ToString();

			Text expirationVigenciaText = this.FindAndResolveComponent<Text>("ExpirationVigenciaText<Text>", DisplayObject);
			expirationVigenciaText.text = optPlan.vigencia_saldo.ToString() + " días";

			Text expirationRedesVigenciaText = this.FindAndResolveComponent<Text>("ExpirationRedesVigenciaText<Text>", DisplayObject);
			expirationRedesVigenciaText.text = optPlan.vigencia_redes_sociales.ToString() + " días";



			//expiration.text = "Saldo total: $"+optPlan.saldo_total+"\n"+DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_A + optPlan.vigencia_saldo + DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_B+"\n" + "Vigencia de Redes Sociales " + optPlan.vigencia_redes_sociales + DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_B;



		}

		Text planClause = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);

		bool displaySocialNets = false;

		whatsappIcon.gameObject.SetActive(true);

		int recarga =  SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme == SessionData.AmigoSchemeSelectionValues.AMIGO_OPTIMO_PLUS ? int.Parse(prepaidPlan.recarga) : optPlan.recarga_saldo;

		if (recarga <= 30)
		{
			planClause.text = DataMessages.AMIGO_SIN_LIMITE_CLAUSE_LOW;
			displaySocialNets = false;
		}
		else {
			planClause.text = DataMessages.AMIGO_SIN_LIMITE_CLAUSE_HIGH;
			displaySocialNets = true;
		}

		twitterIcon.gameObject.SetActive(displaySocialNets);
		facebookIcon.gameObject.SetActive(displaySocialNets);
		faceMessengerIcon.gameObject.SetActive(displaySocialNets);

		if(SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_KIT){
			SessionManager.Instance.CurrentSessionData.ShareMessage = "Llévate el " +  device.model_name + " en Amigo Kit a solo: $" + string.Format("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal(device.prepaid_cost)) +"\n *Precios sujetos a cambio sin previo aviso.";
			SessionManager.Instance.CurrentSessionData.ShareEmail  = "Llévate el " +  device.model_name + " en Amigo Kit a solo: $" + string.Format("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal(device.prepaid_cost)) +"\n *Precios sujetos a cambio sin previo aviso.";
		}
	
		DateTime dateTime = System.DateTime.Now;

		String clauseText = "";

		if(SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL){
			clauseText = SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme == SessionData.AmigoSchemeSelectionValues.AMIGO_SIN_LIMITE ?  DataMessages.AMIGO_FACIL_SIN_FRONTERA_RESUME_CLAUSE:DataMessages.AMIGO_FACIL_SIN_LIMITE_RESUME_CLAUSE;
		}else{
			clauseText = SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme == SessionData.AmigoSchemeSelectionValues.AMIGO_SIN_LIMITE ?  DataMessages.AMIGO_KIT_SIN_FRONTERA_RESUME_CLAUSE:DataMessages.AMIGO_KIT_SIN_LIMITE_RESUME_CLAUSE;
		}

		if (SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme == SessionData.AmigoSchemeSelectionValues.AMIGO_SIN_LIMITE) {	
			planClause.text = clauseText.Replace ("{MB_DATA}", getRecargaNormalTxt(optPlan.recarga_saldo));
			LegalesShare = clauseText.Replace ("{MB_DATA}", getRecargaNormalTxt(optPlan.recarga_saldo));
		} else {
			planClause.text = clauseText.Replace ("{MB_DATA}", getRecargaText(int.Parse(prepaidPlan.recarga)));
			LegalesShare= clauseText.Replace ("{MB_DATA}", getRecargaText(int.Parse(prepaidPlan.recarga)));
		}

		if (SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL) {
			//ES AMIGO FACIL
			if (SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme != SessionData.AmigoSchemeSelectionValues.AMIGO_SIN_LIMITE) {
				//ES AMIGO SIN LIMITE

				String minutes_inc = prepaidPlan.minutos.ToString () == "1000000" ? "Min. ilimitados" : prepaidPlan.minutos.ToString();
				String sms_inc = prepaidPlan.sms.ToString() == "1000000" ? "SMS ilimitados" : prepaidPlan.sms.ToString();



				SessionManager.Instance.CurrentSessionData.ShareEmail = "¡Hola!\n" +
				"Te comparto la oferta de TELCEL, que es de tu interés\n\n" +
				"Oferta vigente para el " + dateTime.ToString ("dd/MM/yyyy") + "\n\n" +
				"Tarifas de Amigo Sin Límite " + recarga + " (recargando $" + recarga + " pesos):\n" +
				"- " + string.Format ("{0:#,###}", Convert.ToDecimal (prepaidPlan.megabytes)) + " MB\n" +
				"- " + minutes_inc + " mins." + "\n" +
				"- " + sms_inc + "\n" +
				"- Vigencia de " + prepaidPlan.vigencia + " días\n" +
				device.device_name + "\n";

				if (plan_facil != null) {
					SessionManager.Instance.CurrentSessionData.ShareEmail +="Costo del equipo  $" + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (plan_facil.kit_credito)) + "\n\n" +
					"Amigo Fácil\n\n" +
					"Llévate el " + device.device_name + " en Amigo Fácil con $" + plan_facil.anticipo + " pesos y 12 pagos mensuales de $" + plan_facil.renta_mensual + " pesos.\n\n" +
					"Precios sujetos a cambio sin previo aviso \n\n";
				}
				SessionManager.Instance.CurrentSessionData.ShareEmail +="*El cargo sólo aplica en caso de realizar una contratación con equipo celular (plazos 12, 18, 24 ó 36 meses). No aplica para planes en Plazo Libre. Si el usuario lleva su “Pago al Corriente” se realizará un descuento por el monto que corresponda al Cargo Mensual por Equipo aplicado. Entiéndase como “Pago al Corriente” liquidar el 100% del Cargo Mensual Total dentro de los 30 días naturales posteriores a su fecha de corte.\n" + DataMessages.PROMOTION_MB_AMIGO;
			} else {
				//ES AMIGO PLUS

				SessionManager.Instance.CurrentSessionData.ShareEmail = "¡Hola!\n\n" +
				"Te comparto la oferta de TELCEL, que es de tu interés\n\n" +
				"Oferta vigente para el" + dateTime.ToString ("dd/MM/yyyy") + "\n\n" +
				"Tarifas de Amigo Óptimo Plus (recargando $" + recarga + " pesos):\n\n" +
				"- Tarifa por minuto de $" + current_prepaid_plan.minute_carrier_cost + " pesos.\n" +
				"- Tarifa por SMS de $" + current_prepaid_plan.sms_cost + " pesos.\n" +
				"- Tarifa por MB de $" + current_prepaid_plan.megabyte_cost + " pesos.\n" +
				"- Redes Sociales sin costo de navegación los primeros 1,000 MB.\n" +
				"- Aplica saldo promocional, dándote un saldo total de $" + optPlan.saldo_total + " pesos.\n" +
				"- Vigencia de Saldo " + optPlan.vigencia_saldo + " días.\n" +
				"- Vigencia de Redes Sociales " + optPlan.vigencia_redes_sociales + " días.\n\n" +
				device.device_name + "\n\n";

				if (plan_facil != null) {
					SessionManager.Instance.CurrentSessionData.ShareEmail += "Costo del equipo $" + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (plan_facil.kit_credito)) + "\n\n" +
					"Amigo Fácil\n\n" +
					device.device_name + " en Amigo Fácil con $" + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (plan_facil.kit_credito)) + " pesos y 12 pagos mensuales de $" + plan_facil.renta_mensual + " pesos.\n\n"; 
				}
				SessionManager.Instance.CurrentSessionData.ShareEmail +="Precios sujetos a cambio sin previo aviso\n\n" +
				"*El cargo sólo aplica en caso de realizar una contratación con equipo celular (plazos 12, 18, 24 ó 36 meses). No aplica para planes en Plazo Libre. Si el usuario lleva su “Pago al Corriente” se realizará un descuento por el monto que corresponda al Cargo Mensual por Equipo aplicado. Entiéndase como “Pago al Corriente” liquidar el 100% del Cargo Mensual Total dentro de los 30 días naturales posteriores a su fecha de corte.\n" + DataMessages.PROMOTION_MB_AMIGO;
			}

		} else {
			//ES AMIGO KIT
			if (SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme != SessionData.AmigoSchemeSelectionValues.AMIGO_SIN_LIMITE) {
				//ES AMIGO SIN LIMITE


				String minutes_inc = prepaidPlan.minutos.ToString () == "1000000" ? "Min. ilimitados" : prepaidPlan.minutos.ToString();
				String sms_inc = prepaidPlan.sms.ToString() == "1000000" ? "SMS ilimitados" : prepaidPlan.sms.ToString();


				SessionManager.Instance.CurrentSessionData.ShareEmail = "¡Hola!\n\n" +
				"Te comparto la oferta de TELCEL, que es de tu interés\n\n" +
					"Oferta vigente para el " + dateTime.ToString ("dd/MM/yyyy") + "\n\n" +
				"Tarifas de Amigo Sin Límite " + recarga + " (recargando $" + recarga + " pesos):\n\n" +
					"- " + string.Format("{0:#,###}", Convert.ToDecimal(prepaidPlan.megabytes)) + " MB\n" +
					"- "+minutes_inc+"\n"+
					"- " + sms_inc+"\n"+
				"- Vigencia de " + prepaidPlan.vigencia + " días\n\n" +
				device.device_name + "\n" +
				"Costo del equipo $" + device.prepaid_cost + "\n\n" +
				"Precios sujetos a cambio sin previo aviso\n\n" +
					"*El cargo sólo aplica en caso de realizar una contratación con equipo celular (plazos 12, 18, 24 ó 36 meses). No aplica para planes en Plazo Libre. Si el usuario lleva su “Pago al Corriente” se realizará un descuento por el monto que corresponda al Cargo Mensual por Equipo aplicado. Entiéndase como “Pago al Corriente” liquidar el 100% del Cargo Mensual Total dentro de los 30 días naturales posteriores a su fecha de corte.\n"+DataMessages.PROMOTION_MB_AMIGO;
			} else {
				SessionManager.Instance.CurrentSessionData.ShareEmail = "¡Hola!\n\n"+
					"Te comparto la oferta de TELCEL, que es de tu interés\n\n"+
					"Oferta vigente para el" + dateTime.ToString ("dd/MM/yyyy")+"\n\n"+
					"Tarifas de Amigo Óptimo Plus (recargando $"+recarga+" pesos):\n\n"+
					"- Tarifa por minuto de $"+current_prepaid_plan.minute_carrier_cost+" pesos.\n"+
					"- Tarifa por SMS de $"+current_prepaid_plan.sms_cost+" pesos.\n"+
					"- Tarifa por MB de $"+current_prepaid_plan.megabyte_cost+" pesos.\n"+
					"- Redes Sociales sin costo de navegación los primeros 1,000 MB.\n"+
					"- Aplica saldo promocional, dándote un saldo total de $"+optPlan.saldo_total+" pesos.\n"+
					"- Vigencia de Saldo "+optPlan.vigencia_saldo+" días.\n"+
					"- Vigencia de Redes Sociales "+optPlan.vigencia_redes_sociales+" días.\n\n"+
					device.device_name+"\n\n"+
					"Costo del equipo $"+ device.prepaid_cost + "\n\n"+
					"Precios sujetos a cambio sin previo aviso\n\n"+
					"*El cargo sólo aplica en caso de realizar una contratación con equipo celular (plazos 12, 18, 24 ó 36 meses). No aplica para planes en Plazo Libre. Si el usuario lleva su “Pago al Corriente” se realizará un descuento por el monto que corresponda al Cargo Mensual por Equipo aplicado. Entiéndase como “Pago al Corriente” liquidar el 100% del Cargo Mensual Total dentro de los 30 días naturales posteriores a su fecha de corte.\n"+DataMessages.PROMOTION_MB_AMIGO;
			}

		}

		SessionManager.Instance.CurrentSessionData.ShareEmail = SessionManager.Instance.CurrentSessionData.ShareEmail + LegalesShare;

		Button shareButton = this.FindAndResolveComponent<Button>("Share<Button>", DisplayObject);
		shareButton.onClick.AddListener (delegate { ShareMenu.Instance.Initialize();});

        Button goHomeButton = this.FindAndResolveComponent<Button>("GoHome<Button>", DisplayObject);
        goHomeButton.onClick.AddListener(
            delegate {
                SessionManager.Instance.ClearSession(false);
                SessionManager.Instance.OpenActivity(Home.Instance);
                Terminate();
            }            
        );


		Button legalesButton = this.FindAndResolveComponent<Button>("ExpandLegals<Button>", DisplayObject);

		legalesButton.gameObject.SetActive (false);
	
		promotionsText.text = DataMessages.PROMOTION_MB_AMIGO;

		List<Devices> relatedDevices = DatabaseManager.Instance.GetRelatedDevices (device.device_id,DatabaseManager.Instance.localDBConnection);



		if (relatedDevices.Count > 0) {
			foreach (Devices deviceRelated in relatedDevices) {
				Brands relatedBrand = DatabaseManager.Instance.GetBrandByBrandId (deviceRelated.brand_id, DatabaseManager.Instance.localDBConnection);
				GameObject deviceEntry = Instantiate<GameObject> (Resources.Load<GameObject> (DataPaths.ELEMENT_DEVICE_RELATED_LAYOUT_PATH));
				Text modelName = this.FindAndResolveComponent<Text> ("DeviceName<Text>", deviceEntry);
				Text brandName = this.FindAndResolveComponent<Text> ("PhoneBrand<Text>", deviceEntry);
				Text deviceName = this.FindAndResolveComponent<Text> ("PhoneName<Text>", deviceEntry);
				Text priceLabel = this.FindAndResolveComponent<Text> ("PriceText<Text>", deviceEntry);
				if (relatedBrand != null) {
					brandName.text = relatedBrand.brand_name;
					deviceName.text = deviceRelated.device_name;
					modelName.text = deviceRelated.model_name;
				}
				priceLabel.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (deviceRelated.prepaid_cost));

				Image deviceEntryImage = this.FindAndResolveComponent<Image> ("DeviceName<Image>", deviceEntry);


				if (string.IsNullOrEmpty (device.img_filepath))
					StartCoroutine (LoadCacheTextureIntoImage (deviceEntryImage, deviceRelated.img_filepath));
				else {
					StartCoroutine(loadAssets(Application.persistentDataPath + deviceRelated.img_filepath,deviceEntryImage));
					// Max Plan images are in another url
					//StartCoroutine(LoadCacheTextureIntoImage(planTypeEntryImg, DataPaths.WS_URL + planType.plan_img));

				}
				Button deviceEntryTrigger = deviceEntry.GetComponent<Button> ();
				deviceEntryTrigger.onClick.AddListener (delegate {
					SessionManager.Instance.ClearSession ();
					SessionManager.Instance.CurrentSessionData.SelectedDeviceId = deviceRelated.device_id;
					SessionManager.Instance.OpenActivity (DeviceDetails.Instance);
					Terminate ();
				});

				deviceEntry.transform.SetParent (relatedDevicesWrapper.transform);
				deviceEntry.transform.localScale = new Vector3 (1, 1, 1);
			}
			related.SetActive (true);

		} else {
			related.SetActive (false);
		}

		related.SetActive (false);
//		if (!DataPaths.ACTIVITY_RELATED_DEVICES_ENABLED) {
//			related.SetActive (false);
//		}


    }

    protected override void ProcessTermination()
    {
    }

    protected override void ProcessUpdate()
    {
        
    }

	public static string getRecargaNormalTxt(int recarga){
		string mb_included = "";
		if (recarga < 50) {
			mb_included = "500";
		} else {
			mb_included = "1,000";
		}

		return mb_included;
	}

	public static string getRecargaText(int recarga){
		string amigoClause = "*Redes Sociales para uso nacional, sin costo de navegación los primeros ";
		string mb_included = "200";
		string legalMb = "";

		if (recarga == 30) {

			mb_included = "300";
		}

		if (recarga == 50) {

			mb_included = "500";
		}

		if (recarga == 80) {

			mb_included = "1,000";
		}


		if (recarga == 100) {

			mb_included = "1,000";
		}

		if (recarga == 150 || recarga == 200) {

			mb_included = "1,500";
		}

		legalMb = amigoClause + mb_included;

		if (recarga > 299) {
			legalMb = "*Redes sociales incluidas para uso nacional ilimitadas";
		}



		return legalMb;
	}

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);

			image.sprite = sprite;
		}
	}


}


