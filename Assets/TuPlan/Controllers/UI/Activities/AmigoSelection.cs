﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;

using TuPlan.Controllers.DataManagement;

public class AmigoSelection : UIModule<AmigoSelection>, IActivity
{

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_PLANSELECTION_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);

		}
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public void StartActivity()
	{
		Initialize();
	}

	protected override void ProcessInitialization()
	{

		GameObject plansContainer = this.Find("Plans<Wrapper>", DisplayObject);


		List<PlanTypes> planTypes = new List<PlanTypes>();

			planTypes = new List<PlanTypes>();
			PlanTypes amigoPlan = new PlanTypes();

			amigoPlan.type_plan = SessionData.AMIGO_SIN_LIMITE_PLAN_TYPE;
			amigoPlan.nombre_plan = "Amigo Sin Límite";
			amigoPlan.eu_canada = true;
			amigoPlan.prioridad = "1000";
			amigoPlan.plan_img = DataPaths.WSAmigoSinLimiteImgURL;
			amigoPlan.plan_img_filepath = "";

			planTypes.Add(amigoPlan);

			#if R9
			PlanTypes amigoOptPlusPlan = new PlanTypes();

			amigoOptPlusPlan.type_plan = SessionData.AMIGO_OPT_PLUS_PLAN_TYPE;
			amigoOptPlusPlan.nombre_plan = "Amigo Óptimo Plus";
			amigoOptPlusPlan.eu_canada = true;
			amigoOptPlusPlan.prioridad = "2000";
			amigoOptPlusPlan.plan_img = DataPaths.WSAmigoOptPlusImgURL;
			amigoOptPlusPlan.plan_img_filepath = "";

			planTypes.Add(amigoOptPlusPlan);

			#endif


		foreach (PlanTypes planType in planTypes)
		{
			GameObject planTypeEntry = Instantiate<GameObject>(Resources.Load<GameObject>(DataPaths.ELEMENT_PLANENTRY_LAYOUT_PATH));

			planTypeEntry.name = planType.type_plan;

			Image planTypeEntryImg = this.FindAndResolveComponent<Image>("PlanLogo<Image>", planTypeEntry);

			if (string.IsNullOrEmpty(planType.plan_img_filepath))
				StartCoroutine(LoadCacheTextureIntoImage(planTypeEntryImg, planType.plan_img));
			else {
				StartCoroutine(loadAssets(Application.persistentDataPath + planType.plan_img_filepath,planTypeEntryImg));
				// Max Plan images are in another url
				//StartCoroutine(LoadCacheTextureIntoImage(planTypeEntryImg, DataPaths.WS_URL + planType.plan_img));
			}



			Button planTypeEntryTrigger = this.FindAndResolveComponent<Button>("Trigger<Button>", planTypeEntry);

			planTypeEntryTrigger.onClick.AddListener(delegate {
					SessionManager.Instance.CurrentSessionData.SelectedPlanType = planTypeEntry.name;
				if(planTypeEntry.name != SessionData.AMIGO_OPT_PLUS_PLAN_TYPE){
					SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme = SessionData.AmigoSchemeSelectionValues.AMIGO_OPTIMO_PLUS;
					SessionManager.Instance.OpenActivity(AmigoSinLimitePlanAdjustment.Instance);
				}else{
					SessionManager.Instance.CurrentSessionData.SelectedAmigoScheme = SessionData.AmigoSchemeSelectionValues.AMIGO_SIN_LIMITE;
					SessionManager.Instance.OpenActivity(AmigoOptimoAdjustment.Instance);
				}
					Terminate();
			});

			planTypeEntry.transform.SetParent(plansContainer.transform);
			planTypeEntry.transform.localScale = new Vector3(1, 1, 1);

		}



	}

	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);

			image.sprite = sprite;
		}
	}

}
