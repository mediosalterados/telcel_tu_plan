﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;

using TuPlan.Controllers.DataManagement;

public class AmigoFacilQuotationResult : UIModule<AmigoFacilQuotationResult>, IActivity
{

	Button _continueButton;
	Text _continueText;
	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_AMIGOFACIL_QUOTATIONRESULT_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);

		}
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public void StartActivity()
	{
		Initialize();
	}

	protected override void ProcessInitialization()
	{

		// Get required entities
		Devices device = DatabaseManager.Instance.GetDeviceByDeviceId(SessionManager.Instance.CurrentSessionData.SelectedDeviceId, DatabaseManager.Instance.localDBConnection);

		AmigoFacilPlans plan = DatabaseManager.Instance.GetAmigoDevicesPlan (device.device_id, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);

		if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO){
			_continueText = this.FindAndResolveComponent<Text>("Continue<Text>", DisplayObject);
			_continueText.text = "IR A ESQUEMAS DE COBRO";
		}

		Text deviceName = this.FindAndResolveComponent<Text>("DeviceName<Text>", DisplayObject);
		deviceName.text = device.model_name;

		if (plan != null) {

			Text MonthlyPaymentValue = this.FindAndResolveComponent<Text> ("MonthlyPaymentValue<Text>", DisplayObject);
			MonthlyPaymentValue.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (plan.renta_mensual));

			Text RetainerValue = this.FindAndResolveComponent<Text> ("RetainerValue<Text>", DisplayObject);
			RetainerValue.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (plan.anticipo));

			Text PriceValue = this.FindAndResolveComponent<Text> ("PriceValue<Text>", DisplayObject);
			PriceValue.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (plan.kit_credito));
		}

	
		Image phoneThumb = this.FindAndResolveComponent<Image>("PhoneThumb<Image>", DisplayObject);
		StartCoroutine(loadAssets(Application.persistentDataPath + device.img_filepath,phoneThumb));

		//StartCoroutine(LoadCacheTextureIntoImage(phoneThumb, DataPaths.WS_URL + device.img_url));

		Image phoneLTE = this.FindAndResolveComponent<Image>("PhoneLTE<Image>", DisplayObject);
		Texture2D tex_net = null;

		switch (device.network) {
		case "VO":
			tex_net = (Texture2D)Resources.Load ("Sprites/volte");
			break;
		case "4G":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		case "45":
			tex_net = (Texture2D)Resources.Load ("Sprites/GigaRed");
			break;
		case "":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		}

		if (tex_net != null) {
			Sprite sprite_net = Sprite.Create(tex_net, new Rect(0, 0, tex_net.width, tex_net.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);
			phoneLTE.sprite = sprite_net;
		}
		phoneLTE.gameObject.SetActive(device.lte);

		Text planClause = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);
		planClause.text = DataMessages.GENERIC_CLAUSE;

		// Phone Specs
		Text camValue = this.FindAndResolveComponent<Text>("CamValue<Text>", DisplayObject);
		camValue.text = device.back_camera + " / " + device.front_camera;

		Text screenValue = this.FindAndResolveComponent<Text>("ScreenValue<Text>", DisplayObject);
		screenValue.text = device.display;

		Text storageValue = this.FindAndResolveComponent<Text>("StorageValue<Text>", DisplayObject);
		storageValue.text = device.storage;

		Text cpuValue = this.FindAndResolveComponent<Text>("CPUValue<Text>", DisplayObject);
		cpuValue.text = device.cpu + "\n" + device.cores + " cores\n@ " + device.speed;

		Text memoryValue = this.FindAndResolveComponent<Text>("MemoryValue<Text>", DisplayObject);
		memoryValue.text = device.ram;

		GameObject hasSnapdragon = this.Find("HasSnapdragon<Wrapper>", DisplayObject);
		hasSnapdragon.SetActive(device.snapdragon);

		MobileOSs mobileOs = DatabaseManager.Instance.GetMobileOSByOSId(device.os_id, DatabaseManager.Instance.localDBConnection);
		Image osImage = this.FindAndResolveComponent<Image>("OSIcon<Image>", DisplayObject);
		Text osName = this.FindAndResolveComponent<Text> ("OSName<Text>", DisplayObject);
		Text osVersionName = this.FindAndResolveComponent<Text> ("OSVersionName<Text>", DisplayObject);
		if (mobileOs != null) {
			StartCoroutine(loadAssets(Application.persistentDataPath + mobileOs.os_img_filepath,osImage));

			osName.text = mobileOs.os_name;
			osVersionName.text = mobileOs.os_version;

			if (osName.text.Contains ("Android")) {
				Color nColor = new Color ();
				ColorUtility.TryParseHtmlString ("#6ca604", out nColor);
				osName.color = nColor;
			}
		} else {
			osName.text = " ";
			osVersionName.text = " ";

			GameObject osWrapper = this.Find("OSInfo<Wrapper>", DisplayObject);
			osWrapper.gameObject.SetActive (false);
		}



		_continueButton = this.FindAndResolveComponent<Button>("Continue<Button>", DisplayObject);

		if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO){
			_continueButton.onClick.AddListener(delegate {
			SessionManager.Instance.OpenActivity(PaymentSchemeSelection.Instance);
			Terminate();            
		});
		}else{
			_continueButton.onClick.AddListener(delegate {
				SessionManager.Instance.OpenActivity(AmigoSelection.Instance);
				Terminate();            
			});
		}        

	}

	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);


			image.sprite = sprite;
		}
	}
}


