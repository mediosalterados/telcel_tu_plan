﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;
using Paroxe.PdfRenderer;

using TuPlan.Controllers.DataManagement;

public class FullImage : UIModule<FullImage>, IActivity
{
	public PDFViewer m_Viewer;
	public PDFAsset m_PDFAsset;
	public String self_full_path;
	public Offers offers;
//	List<GuiaImage> images;
	Button _continueButton;
	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_FULLIMAGE_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.TOP;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);
		}
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public void StartActivity()
	{
		Initialize();
	}

	IEnumerator loadAsset(){
		yield return new WaitForSeconds (1.0f);
		string full_path = null;

		full_path = self_full_path;

		Debug.Log(self_full_path);

		m_Viewer = this.FindAndResolveComponent<PDFViewer>("PDFViewer", DisplayObject);

		m_Viewer.gameObject.SetActive(true);



		int separator_path = full_path.LastIndexOf('/');

		string file_name = full_path.Substring (separator_path+1);
		string file_path = full_path.Substring (0, separator_path+1);

		m_Viewer.LoadDocumentFromPersistentData(file_path,file_name,"");

		while (!m_Viewer.IsLoaded)
			yield return null;

	}

	protected override void ProcessInitialization()
	{

		Button closeButton = this.FindAndResolveComponent<Button>("CloseButton<Button>", DisplayObject);

		closeButton.onClick.AddListener(delegate {
			SessionManager.Instance.GoToPreviousActivity();     
		});        
		StartCoroutine (loadAsset ());

	}

	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{
		
	}
}


