﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

public class PaymentSchemeSelection : UIModule<PaymentSchemeSelection>, IActivity
{
    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_PAYMENTSCHEMESELECTION_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
        }
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void StartActivity()
    {
        Initialize();
    }

    protected override void ProcessInitialization()
    {
        Button pospaidButton = this.FindAndResolveComponent<Button>("PostPaidScheme<Button>", DisplayObject);
        pospaidButton.onClick.AddListener(delegate {
            SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme = SessionData.PaymentSchemeSelectionValues.POSTPAID;
            SessionManager.Instance.OpenActivity(PlanSelection.Instance);
            Terminate();
        });

        Button prepaidButton = this.FindAndResolveComponent<Button>("PrePaidScheme<Button>", DisplayObject);
        prepaidButton.onClick.AddListener(delegate
        {
            SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme = SessionData.PaymentSchemeSelectionValues.PREPAID;
            SessionManager.Instance.OpenActivity(PlanSelection.Instance);
            Terminate();
        });

    }

    protected override void ProcessTermination()
    {
        
    }

    protected override void ProcessUpdate()
    {
        
    }
}