﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using TuPlan.Controllers.DataManagement;

using System;

public class Info : UIModule<Info>, IActivity {

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_INFO_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);
		}
	}

	protected override void ProcessInitialization()
	{
		
		Text versionText = this.FindAndResolveComponent<Text> ("VersionText<Text>", DisplayObject);
		versionText.text += Application.version+":"+DataManager.Instance.DataVersion;


		Text kiiText = this.FindAndResolveComponent<Text> ("Kii<Text>", DisplayObject);
		kiiText.text = "Kii: "+PlayerPrefs.GetString("kiiUser","N/A");

	}

	protected override void ProcessUpdate()
	{

	}

	protected override void ProcessTermination()
	{
	}

	public void StartActivity()
	{
		Initialize();


	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}
}
