﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GLIB.Interface;
using GLIB.Extended;
using System;

using GLIB.Audio;

using TuPlan.Controllers.DataManagement;


public class PromotionsScreen : UIModule<PromotionsScreen>, IActivity  {

        float swipeSpeed = 0.05F;
        float inputX;
        float inputY;

	GameObject _tutPage1;
	GameObject _tutPage2;
	GameObject _tutPage3;
	GameObject _tutPage4;

	List<GameObject> _pages;

	RectTransform _tutPage1Rect;
	RectTransform _tutPage2Rect;
	RectTransform _tutPage3Rect;
	RectTransform _tutPage4Rect;

        int _currentPage;
        int _maxPages = 4;
		List<Promotions> _promotions;
		List<Promotions> offers;
        Toggle[] _toggles;

        bool _doingTransition {
            get {

			bool rval = false;
			if (_pages != null)
                {
				foreach(GameObject page in _pages){
					if (page != null) {
						AnimateComponent animation = page.ResolveComponent<AnimateComponent>();
						if (animation.isTranslating)
							rval = true;
					}
                }
            }

			return rval;
		}
        }

	public void StartActivity()
	{
		Initialize();
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}


        protected override string DisplayObjectPath
        {
            get
            {
			return DataPaths.ACTIVITY_PROMOTIONS_LAYOUT_PATH;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.MIDDLE;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
			return new Transition(Transition.InOutAnimations.NONE);
                
            }
        }

	protected override void ProcessInitialization()
	{
		Debug.Log("Tutorial Screen Initialized");


		_promotions =  DatabaseManager.Instance.GetAllPromotions(DatabaseManager.Instance.localDBConnection);

		_tutPage1 = this.Find("Page4", DisplayObject);
		_tutPage2 = this.Find("Page1", DisplayObject);
		_tutPage3 = this.Find("Page2", DisplayObject);
		_tutPage4 = this.Find("Page3", DisplayObject);


		_currentPage = 0;

		_tutPage2Rect = _tutPage2.GetComponent<RectTransform>();
		Vector2 tutPage2Position = _tutPage2Rect.anchoredPosition;
		tutPage2Position.x = _tutPage2Rect.rect.width*1;
		_tutPage2Rect.anchoredPosition = tutPage2Position;

		_tutPage3Rect = _tutPage3.GetComponent<RectTransform>();
		Vector2 tutPage3Position = _tutPage3Rect.anchoredPosition;
		tutPage3Position.x = _tutPage3Rect.rect.width*2;
		_tutPage3Rect.anchoredPosition = tutPage3Position;

		_tutPage4Rect = _tutPage4.GetComponent<RectTransform>();
		Vector2 tutPage4Position = _tutPage4Rect.anchoredPosition;
		tutPage4Position.x = _tutPage4Rect.rect.width*3;
		_tutPage4Rect.anchoredPosition = tutPage4Position;


		_pages = new List<GameObject> { _tutPage1, _tutPage2, _tutPage3, _tutPage4 };

		_toggles = DisplayObject.GetComponentsInChildren<Toggle>();

		HandlePageToggles(_currentPage, _toggles);

		int index = 0;

		List<Promotions> promos = new List<Promotions>();
		offers = new List<Promotions>();

	
		foreach (Promotions promotion in _promotions) {
			if (promotion.button_text == "Ofertas") {
				offers.Add (promotion);
			} else {
				promos.Add (promotion);
			}
		}
	
	


		List<GameObject> _pageData = new List<GameObject> { _tutPage2, _tutPage3, _tutPage4 };

		foreach (Promotions promotion in offers) {
			GameObject pageType = Instantiate<GameObject> (Resources.Load<GameObject> (DataPaths.FRAGMENT_PROMOTIONS_PAGE_ROW_PATH));


			Text promotionSubtitle = this.FindAndResolveComponent<Text> ("SubTitle<Text>", pageType);
			promotionSubtitle.text = promotion.subtitle;

			Text promotionDescription = this.FindAndResolveComponent<Text> ("Title<Text>", pageType);
			promotionDescription.text = promotion.detail;

			GameObject _tutPage2Panel = this.Find ("Panel", _tutPage1);

			pageType.transform.SetParent (_tutPage2Panel.transform);
			pageType.transform.localScale = new Vector3 (1, 1, 1);

			GameObject separator = Instantiate<GameObject> (Resources.Load<GameObject> (DataPaths.FRAGMENT_SEPARATOR));

			separator.transform.SetParent (_tutPage2Panel.transform);
			separator.transform.localScale = new Vector3 (1, 1, 1);

			Button detailsButton = this.FindAndResolveComponent<Button> ("ButtonPanel<Button>", pageType);
			detailsButton.name = index.ToString();
			detailsButton.onClick.AddListener (delegate {
				buttonClicker(detailsButton.name);
			});

			index++;
		}

		index = 0;

		foreach (GameObject page in _pageData) {


			if(_promotions[index].button_text != "Ofertas"){

			Image bannerImage = this.FindAndResolveComponent<Image> ("ImageBanner<Image>", page);
			
				StartCoroutine(loadAssets(Application.persistentDataPath + _promotions[index].promotion_img_filepath,bannerImage));

			Text titleText = this.FindAndResolveComponent<Text> ("Title<Text>", page);

			titleText.text = _promotions [index].title;

			Text subTitleText = this.FindAndResolveComponent<Text> ("SubTitle<Text>", page);

			subTitleText.text = _promotions [index].subtitle;

			Text descriptionText = this.FindAndResolveComponent<Text> ("Description<Text>", page);
			descriptionText.text = _promotions [index].detail;


			Text detailButtonText = this.FindAndResolveComponent<Text> ("ButtonTextDetail<Text>", page);
			detailButtonText.text = _promotions [index].button_text.ToUpper();

			Button detailsButton = this.FindAndResolveComponent<Button> ("ButtonPanel<Button>", page);
			detailsButton.onClick.AddListener (delegate {
				Application.OpenURL(_promotions[0].button_url);
			});


			index++;
			}
		}

	}

	protected void buttonClicker(String name){

		Promotions promotion = offers[int.Parse (name)];
		FullImage.Instance.self_full_path = promotion.offer_pdf;
		SessionManager.Instance.OpenActivity (FullImage.Instance);
		Terminate ();


	}

        protected override void ProcessUpdate()
        {

            if (!_doingTransition)
            {

                /*if (Input.GetMouseButton(0))
                {

                    inputX = Input.GetAxis("Mouse X");

                }

                if (Input.GetKeyDown(KeyCode.D) || inputX < -0.5f)
                    NextPage();
                else if (Input.GetKeyDown(KeyCode.A) || inputX > 0.5f)
                    PrevPage();


                Debug.Log(inputX);


                inputX = 0;

                if (Input.GetKeyDown(KeyCode.D))
                    NextPage();
                else if (Input.GetKeyDown(KeyCode.A))
                    PrevPage();*/
            }

//            if (Input.GetKeyDown(KeyCode.Escape))
//            {
//              
//                Terminate();
//            }

        }

        protected override void ProcessTermination()
        {
            Debug.Log("Tutorial Screen Terminated");
        }

//        void FixedUpdate()
//        {
//            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
//            {
//                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
//                inputX += touchDeltaPosition.x * swipeSpeed;
//                inputY += touchDeltaPosition.y * swipeSpeed;
//                Debug.Log("X, Y: " + touchDeltaPosition.x + ", " + touchDeltaPosition.y);
//
//                /*Text inputXDebug = this.FindAndResolveComponent<Text>("inputXDebug<Text>", DisplayObject);
//                inputXDebug.text = inputX.ToString();
//                */
//
//                if (inputX < -1)
//                    NextPage();
//                else if (inputX > 1)
//                    PrevPage();
//
//
//                Debug.Log(inputX);
//
//
//                inputX = 0;
//            }
//
//        }

        void NextPage() {

            if (_doingTransition)
                return;

            // Check which is the leftmost page
            float leftMostX = 0;
            GameObject leftMostPage = null;

            foreach (GameObject page in _pages)
            {
			
                RectTransform rect = page.ResolveComponent<RectTransform>();

                if (rect.anchoredPosition.x < leftMostX) {
                    leftMostPage = page;
                    leftMostX = rect.anchoredPosition.x;
                }

            }

            foreach (GameObject page in _pages)  {

                // chek the leftmost page and change its position to the rightmost
                RectTransform rect = page.ResolveComponent<RectTransform>();

                if (page == leftMostPage)
                {
                    // Place it on the rightmost position
                    Vector2 nPos = rect.anchoredPosition;
                    nPos.x = rect.rect.width * (_maxPages - 1);
                    rect.anchoredPosition = nPos;
                }

                Vector3 tPos = rect.anchoredPosition;
                tPos.x -= rect.rect.width;

                page.ResolveComponent<AnimateComponent>().TranslateObject(tPos, 0.5f, null);


            }

            _currentPage++;

            _currentPage %= _maxPages;

            HandlePageToggles(_currentPage, _toggles);
        }

        void PrevPage() {

            if (_doingTransition)
                return;

            // Check which is the righmost page
            float rightMostX = 0;
            GameObject rightMostPage = null;

            foreach (GameObject page in _pages) {

                RectTransform rect = page.ResolveComponent<RectTransform>();

                if (rect.anchoredPosition.x > rightMostX) {
                    rightMostPage = page;
                    rightMostX = rect.anchoredPosition.x;
                }

            }

            foreach (GameObject page in _pages)
            {

                // chek the rightmost page and change its position to the leftmost
                RectTransform rect = page.ResolveComponent<RectTransform>();

                if (page == rightMostPage)
                {
                    // Place it on the leftmost position
                    Vector2 nPos = rect.anchoredPosition;
                    nPos.x = -rect.rect.width * (_maxPages - 1);
                    rect.anchoredPosition = nPos;
                }

                Vector3 tPos = rect.anchoredPosition;
                tPos.x += rect.rect.width;

                page.ResolveComponent<AnimateComponent>().TranslateObject(tPos, 0.5f, null);


            }

            _currentPage--;

            if (_currentPage < 0)
                _currentPage = _maxPages - 1;

            HandlePageToggles(_currentPage, _toggles);
        }

        void HandlePageToggles(int currentPage, Toggle[] toggles) {

            // shutdown all toggles
            foreach (Toggle toggle in toggles) {

                toggle.isOn = false;

            }

            if (currentPage >= 0 && currentPage < toggles.Length)
                toggles[currentPage].isOn = true;

        }

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);

			image.sprite = sprite;
		}
	}

    }
