﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using System;

using TuPlan.Controllers.DataManagement;

public class DeviceBrandFilter : UIModule<DeviceBrandFilter>, IActivity
{
    GameObject _brandsContainer;

    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModule<DeviceBrandFilter>.UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_DEVICEBRANDFILTER_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);

        }
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void StartActivity()
    {
        Initialize();
    }

    protected override void ProcessInitialization()
    {
		Debug.Log ("DevicesBrandFilter iniciado <---");
        // Fill the menu with the brands
        bool visible = true;

		bool? fee = SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.NO_ADVANCE_PAYMENT ? true : (bool?)null;

		bool? lte = SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.LTE || SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.LTE_SNAPDRAGON ? true : (bool?)null;

		bool? snapdragon = SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.SNAPDRAGON || SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.LTE_SNAPDRAGON ? true : (bool?)null;

		string plan_string = SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.POSTPAID && SessionManager.Instance.CurrentSessionData.SelectedPlanId != null ? SessionManager.Instance.CurrentSessionData.SelectedPlanId:null;

		StartCoroutine (loadData(visible,lte,snapdragon,fee,plan_string));



    }

    protected override void ProcessTermination()
    {
        
    }

    protected override void ProcessUpdate()
    {
        
    }

	IEnumerator loadData(bool visible,bool? lte, bool? snapdragon, bool? fee, string plan_string){
		yield return new WaitForSeconds (0.3f);
		List<Brands> brands = DatabaseManager.Instance.GetBrandsByTerm(DatabaseManager.Instance.localDBConnection, visible, lte, snapdragon,fee,plan_string);

	_brandsContainer = this.Find("Content<Wrapper>", DisplayObject);

	foreach (Brands brand in brands) {

		GameObject brandElement = Instantiate<GameObject>(Resources.Load<GameObject>(DataPaths.ELEMENT_BRANDENTRY_LAYOUT_PATH));

		brandElement.name = brand.brand_id.ToString();

		brandElement.transform.SetParent(_brandsContainer.transform);
		brandElement.transform.localPosition = new Vector3();
		brandElement.transform.localScale = new Vector3(1, 1, 1);
		brandElement.transform.localRotation = Quaternion.identity;

		// Set the image
		Image brandElementImage = this.FindAndResolveComponent<Image>("BrandIcon<Image>", brandElement);

		StartCoroutine(loadAssets(Application.persistentDataPath + brand.hover_image_filepath,brandElementImage));


		// Set Button
		Button brandElementButton = this.FindAndResolveComponent<Button>("BrandButton", brandElement);
		brandElementButton.onClick.AddListener(delegate {
			Debug.Log ("************ Click  <---");
			SessionManager.Instance.CurrentSessionData.SelectedBrandId = brandElement.name;
			SessionManager.Instance.OpenActivity(DeviceSelection.Instance);
			Terminate();
		});
}
	}

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);
			image.sprite = sprite;
		}
	}
}