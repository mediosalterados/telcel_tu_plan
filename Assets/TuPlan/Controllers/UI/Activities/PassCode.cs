﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using TuPlan.Controllers.DataManagement;
using System;

public class PassCode : UIModule<PassCode>, IActivity {

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_PASSCODE_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);
		}
	}

	protected override void ProcessInitialization()
	{

		Text textCode = this.FindAndResolveComponent<Text>("ValorTexto<Text>", DisplayObject);
		textCode.text = "";
		Button continueButton = this.FindAndResolveComponent<Button>("Accept<Button>", DisplayObject);
		continueButton.onClick.AddListener (delegate { 

			if(textCode.text == "7654"){
				PlayerPrefs.SetInt("first_time", 1);
				DataManager.Instance.Initialize ();
				SessionManager.Instance.Initialize ();
				SessionManager.Instance.OpenActivity (Home.Instance);
				SessionManager.Instance.SetHomeAsRoot();
				MainMenu.Instance.Initialize ();
				Terminate();

			}else{
				Debug.Log("no es el codigo?");
			}
		}
		);




	}

	protected override void ProcessUpdate()
	{

	}

	protected override void ProcessTermination()
	{
		Debug.Log("Home Activity => Terminated");
	}

	public void StartActivity()
	{
		Initialize();
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}
}
