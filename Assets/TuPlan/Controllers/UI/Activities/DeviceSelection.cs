using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;

using KiiCorp.Cloud.Analytics;
using System;

using TuPlan.Controllers.DataManagement;

public class DeviceSelection : UIModule<DeviceSelection>, IActivity
{

    UIScrollComponent _uiScrollComponent;
    List<Devices> _devices;

    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModule<DeviceSelection>.UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_DEVICESELECTION_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
        }
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void StartActivity()
    {
        Initialize();
    }

    protected override void ProcessInitialization()
    {

        Brands selectedBrand = DatabaseManager.Instance.GetBrandByBrandId(
            SessionManager.Instance.CurrentSessionData.SelectedBrandId, 
            DatabaseManager.Instance.localDBConnection
            );

        if (selectedBrand == null)
        {
            Debug.LogError("No brand found within brand id");
            return;
        }

        #region Analytics
        // Send analytics
        if (Application.isMobilePlatform && KiiPushService.Instance.isRunning)
        {
            // Create a KiiEvent instance
            KiiEvent ev = KiiAnalytics.NewEvent("selectedBrand");

            // Set key-value pairs
            ev["brandName"] = selectedBrand.brand_name;

            // Upload Event Data to Kii Cloud
            KiiAnalytics.Upload((Exception e) =>
            {
                if (e != null)
                {
                    string message = "Failed to upload events " + e.ToString();
                    Debug.Log(message);
                    return;
                }
                Debug.Log("event upload succeeded");
            }, ev);
        }
        #endregion

        Debug.Log("DeviceSelection => Displaying phones from brand: " + selectedBrand.brand_name);

        // Set the brand image
        Image brandImage = this.FindAndResolveComponent<Image>("BrandImage<Image>", DisplayObject);

		StartCoroutine(loadAssets(Application.persistentDataPath +  selectedBrand.hover_image_filepath,brandImage));

		bool fee = SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.NO_ADVANCE_PAYMENT;
		bool snap = SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.SNAPDRAGON || SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.LTE_SNAPDRAGON ? true : false;
		bool lte = SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.LTE || SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter == SessionData.DeviceMainFilterSelectionValues.LTE_SNAPDRAGON ? true : false;
		string plan_string = SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.POSTPAID && SessionManager.Instance.CurrentSessionData.SelectedPlanId != null ? SessionManager.Instance.CurrentSessionData.SelectedPlanId:null;
		GameObject snapDragonFilter = this.Find("FilterLabel<Wrapper>", DisplayObject);
			snapDragonFilter.SetActive(snap);

		_devices = DatabaseManager.Instance.GetDevicesByBrand(selectedBrand.brand_id, lte,snap,fee,plan_string,DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);

        _uiScrollComponent = DisplayObject.AddComponent<UIScrollComponent>();
        _uiScrollComponent.enabled = false;
        _uiScrollComponent.EntryDisplayObjectPath = DataPaths.ELEMENT_DEVICEENTRY_LAYOUT_PATH;
        _uiScrollComponent.WriteEntryData = WriteEntryData;
        _uiScrollComponent.InitialNumberOfEntries = _devices.Count;
        _uiScrollComponent.enabled = true;

    }

    protected override void ProcessTermination()
    {
        
    }

    protected override void ProcessUpdate()
    {
        
    }

    public void WriteEntryData(GameObject entry)
    {

        try
        {

            // Get Device details
            Devices device = _devices[int.Parse(entry.name)];
            
            // Get Entry fields
            Image deviceThumbnail = this.FindAndResolveComponent<Image>("Thumb<Image>", entry);      
            Text modelName = this.FindAndResolveComponent<Text>("DeviceName<Text>", entry);
            Text deviceName = this.FindAndResolveComponent<Text>("ModelName<Text>", entry);
            Button trigger = this.FindAndResolveComponent<Button>("Trigger<Button>", entry);
            trigger.name = device.device_id;

			StartCoroutine(loadAssets(Application.persistentDataPath +  device.img_filepath,deviceThumbnail));

            modelName.text = device.model_name;
            deviceName.text = device.device_name;
            
            trigger.onClick.AddListener(delegate {
                SessionManager.Instance.CurrentSessionData.SelectedDeviceId = trigger.name;

                if(SessionManager.Instance.CurrentSessionData.SelectedPlanType == "INT_HOME"){
                    SessionManager.Instance.OpenActivity(MaxSinLimiteQuotationResult.Instance);
                }else if(SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL){
					SessionManager.Instance.OpenActivity(AmigoFacilQuotationResult.Instance);

				}else if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO){
                SessionManager.Instance.OpenActivity(AmigoFacilQuotationResult.Instance);
				}else{
                    SessionManager.Instance.OpenActivity(DeviceDetails.Instance);
                }
                Terminate();
            });

            //StartCoroutine(LoadCacheTextureIntoImage(deviceThumbnail, DataPaths.WS_URL + device.img_url));

        }
        catch (System.NullReferenceException e)
        {
            Debug.LogError(e.Message + "\nCould not write contact data into entry." + e.StackTrace);
        }


    }

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);

			image.sprite = sprite;
		}
	}

}

