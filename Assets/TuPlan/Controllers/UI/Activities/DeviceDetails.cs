using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;

using TuPlan.Controllers.DataManagement;

using System;

public class DeviceDetails : UIModule<DeviceDetails>, IActivity
{
	GameObject descriptionLine;
	GameObject description;
	GameObject related;
	GameObject osWrapper;
	Text featuredHeader;
	Text featuredText;  
	Text cpuValue;
	GameObject hasSnapdragon;
	Image phoneLTE;
	GameObject row3;
	GameObject cpu;
	GameObject empty;
	Button goToPlanSelection;
	Text goToPlanSelectionLabel;
    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;  
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_DEVICEDETAILS_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
            
        }
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void StartActivity()
    {
        Initialize();
    }

    protected override void ProcessInitialization()
    {
       

		 descriptionLine = this.Find("UnderlineDescription<Image>", DisplayObject);
		 description = this.Find("Item 0", DisplayObject);
		 related = this.Find("Item 1", DisplayObject);
		 cpuValue = this.FindAndResolveComponent<Text>("CPUValue<Text>", DisplayObject);
		 hasSnapdragon = this.Find("HasSnapdragon<Wrapper>", DisplayObject);
		 phoneLTE = this.FindAndResolveComponent<Image>("PhoneLTE<Image>", DisplayObject);
		 row3 = this.Find("Row3<Wrapper>", DisplayObject);
		cpu = this.Find("CPUDetail<Wrapper>", DisplayObject);
		empty = this.Find("Empty<Wrapper>", DisplayObject);
		featuredHeader = this.FindAndResolveComponent<Text>("FeaturedHeader<Text>", DisplayObject);
		featuredText = this.FindAndResolveComponent<Text> ("FeaturedText<Text>", DisplayObject);  


		goToPlanSelection = this.FindAndResolveComponent<Button>("Continue<Button>", DisplayObject);
		goToPlanSelectionLabel = this.FindAndResolveComponent<Text>("Continue<Text>", DisplayObject);

		goToPlanSelection.onClick.AddListener(delegate
			{

				if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep != SessionData.InitialStepsValues.EQUIPO){
					if(SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.PREPAID){
						SessionManager.Instance.OpenActivity(AmigoSinLimiteQuotationResult.Instance);
					}else{
						SessionManager.Instance.OpenActivity(MaxSinLimiteQuotationResult.Instance);
					}
				}else{
					SessionManager.Instance.OpenActivity(PaymentSchemeSelection.Instance);
				}

				Terminate();
			});

		goToPlanSelectionLabel.text = SessionManager.Instance.CurrentSessionData.SelectedInitialStep != SessionData.InitialStepsValues.EQUIPO ?   DataMessages.DEVICE_DETAILS_COTINUE_PLAN_LABEL : DataMessages.DEVICE_DETAILS_CONTINUE_LABEL;

		ChangeDevice ();
   }

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0f, 0f));

			image.sprite = sprite;
		}
	}

	protected void ChangeDevice(){
		Debug.Log("DeviceDetails => Initialized with device: "+SessionManager.Instance.CurrentSessionData.SelectedDeviceId);
		GameObject relatedDevicesWrapper = this.Find("RelatedDevices<Wrapper>", DisplayObject);
		Devices device = DatabaseManager.Instance.GetDeviceByDeviceId(SessionManager.Instance.CurrentSessionData.SelectedDeviceId, DatabaseManager.Instance.localDBConnection);
		Brands brand = DatabaseManager.Instance.GetBrandByBrandId(device.brand_id, DatabaseManager.Instance.localDBConnection);
		MobileOSs mobileOs = DatabaseManager.Instance.GetMobileOSByOSId(device.os_id, DatabaseManager.Instance.localDBConnection);

		Text descriptionText = this.FindAndResolveComponent<Text>("DeviceDescription<Text>", description);


		if (device.description == null) {
			descriptionLine.SetActive (false);
			description.SetActive (false);
		}else{
			descriptionLine.SetActive (true);
			description.SetActive (true);
			descriptionText.text = device.description.Replace("|",System.Environment.NewLine);
		}



		Image phoneThumb = this.FindAndResolveComponent<Image>("PhoneThumb<Image>", DisplayObject);
		StartCoroutine(loadAssets(Application.persistentDataPath + device.img_filepath,phoneThumb));

		Texture2D tex_net = null;

		switch (device.network) {
		case "VO":
			tex_net = (Texture2D)Resources.Load ("Sprites/volte");
			break;
		case "4G":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		case "45":
			tex_net = (Texture2D)Resources.Load ("Sprites/GigaRed");
			break;
		case "":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		}

		Sprite sprite_net = Sprite.Create(tex_net, new Rect(0, 0, tex_net.width, tex_net.height), new Vector2(.5f, .5f), 100F, 0, SpriteMeshType.FullRect);

		phoneLTE.sprite = sprite_net;
		phoneLTE.gameObject.SetActive(device.lte);


		Text phoneName = this.FindAndResolveComponent<Text>("PhoneName<Text>", DisplayObject);
		phoneName.text = device.model_name;

		Text phoneBrand = this.FindAndResolveComponent<Text>("PhoneBrand<Text>", DisplayObject);
		phoneBrand.text = brand.brand_name;

		Image osImage = this.FindAndResolveComponent<Image>("OSIcon<Image>", DisplayObject);
		Text osName = this.FindAndResolveComponent<Text> ("OSName<Text>", DisplayObject);
		Text osVersionName = this.FindAndResolveComponent<Text> ("OSVersionName<Text>", DisplayObject);
		if (mobileOs != null) {
			StartCoroutine(loadAssets(Application.persistentDataPath + mobileOs.os_img_filepath,osImage));
			osName.text = mobileOs.os_name;
			osVersionName.text = mobileOs.os_version;
			if (osName.text.Contains ("Android")) {
				Color nColor = new Color ();
				ColorUtility.TryParseHtmlString ("#6ca604", out nColor);
				osName.color = nColor;
			}
		} else {
			osName.text = " ";
			osVersionName.text = " ";

			osWrapper = this.Find("OSInfo<Wrapper>", DisplayObject);
			osWrapper.gameObject.SetActive (false);
		}

		  
		int months = 24;        


		DevicePlans bestDevicePlan = DatabaseManager.Instance.GetBestPlanFromDevice(device, months, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);



		if (bestDevicePlan != null) {

			Plans bestPlan = DatabaseManager.Instance.GetPlanByPlanId (bestDevicePlan.plan_id, DatabaseManager.Instance.localDBConnection);

			if (bestDevicePlan.cost == "0") {
				featuredHeader.text = DataMessages.DEVICE_DETAILS_NO_COSTO;
			} else {
				featuredHeader.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (bestDevicePlan.cost));
			}



			if (SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.PREPAID) {
				featuredText.text = "Pago inicial en " + bestPlan.plan_name + " a " + months + " " + DataMessages.DEVICE_DETAILS_MONTHS;
			} else {
				featuredText.text = "en " + bestPlan.plan_name + "\na " + months + " " + DataMessages.DEVICE_DETAILS_MONTHS;
			}


		} else {
			featuredHeader.gameObject.active = false;
			featuredText.gameObject.active = false;
		}

		Text labelAText = this.FindAndResolveComponent<Text> ("InfoAText<Text>", DisplayObject);
		labelAText.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (device.prepaid_cost));

		// Phone Specs
		Text camValue = this.FindAndResolveComponent<Text>("CamValue<Text>", DisplayObject);
		camValue.text = device.back_camera + " / " + device.front_camera;

		Text screenValue = this.FindAndResolveComponent<Text>("ScreenValue<Text>", DisplayObject);
		screenValue.text = device.display;

		Text storageValue = this.FindAndResolveComponent<Text>("StorageValue<Text>", DisplayObject);
		storageValue.text = device.storage;


		cpuValue.text = device.cpu + " "+ device.submodel_cpu;

		Text memoryValue = this.FindAndResolveComponent<Text>("MemoryValue<Text>", DisplayObject);
		memoryValue.text = device.ram;


		hasSnapdragon.SetActive(device.snapdragon);


		row3.SetActive(device.snapdragon);




		cpu.SetActive(false);


		empty.SetActive(false);

		if (device.snapdragon == true) {
			cpu.SetActive (true);
			empty.SetActive (true);

			Button qualcolmButton = this.FindAndResolveComponent<Button> ("DeviceFilterIcon<Image>", DisplayObject);

			qualcolmButton.onClick.AddListener (delegate {
				SessionManager.Instance.OpenActivity (Qualcom.Instance);
				Terminate ();
			});
		} else {
			cpu.SetActive (false);
			empty.SetActive (false);
		}

		Text clauses = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);
		clauses.text = DataMessages.GENERIC_CLAUSE;
		           

		List<Devices> relatedDevices = DatabaseManager.Instance.GetRelatedDevices (device.device_id,DatabaseManager.Instance.localDBConnection);


		int childs = relatedDevicesWrapper.transform.childCount;
		for (int i = childs - 1; i >= 0; i--)
		{
			GameObject.Destroy(relatedDevicesWrapper.transform.GetChild(i).gameObject);
		}

		if (relatedDevices.Count > 0) {
			foreach (Devices deviceRelated in relatedDevices) {
				Brands relatedBrand = DatabaseManager.Instance.GetBrandByBrandId (deviceRelated.brand_id, DatabaseManager.Instance.localDBConnection);
				GameObject deviceEntry = Instantiate<GameObject> (Resources.Load<GameObject> (DataPaths.ELEMENT_DEVICE_RELATED_LAYOUT_PATH));
				Text modelName = this.FindAndResolveComponent<Text> ("DeviceName<Text>", deviceEntry);
				Text brandName = this.FindAndResolveComponent<Text> ("PhoneBrand<Text>", deviceEntry);
				Text deviceName = this.FindAndResolveComponent<Text> ("PhoneName<Text>", deviceEntry);
				Text priceLabel = this.FindAndResolveComponent<Text> ("PriceText<Text>", deviceEntry);

				if (relatedBrand != null) {
					brandName.text = relatedBrand.brand_name;
					deviceName.text = deviceRelated.device_name;
					modelName.text = deviceRelated.model_name;
				}
				priceLabel.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (deviceRelated.prepaid_cost));

				Image deviceEntryImage = this.FindAndResolveComponent<Image> ("DeviceName<Image>", deviceEntry);


				if (string.IsNullOrEmpty (device.img_filepath)) {
					StartCoroutine (LoadCacheTextureIntoImage (deviceEntryImage, deviceRelated.img_filepath));
				}else {
					StartCoroutine(loadAssets(Application.persistentDataPath + deviceRelated.img_filepath,deviceEntryImage));
				}
				Button deviceEntryTrigger = deviceEntry.GetComponent<Button> ();
				deviceEntryTrigger.onClick.AddListener (delegate {
					SessionManager.Instance.CurrentSessionData.SelectedDeviceId = deviceRelated.device_id;
					ChangeDevice ();
				});

				deviceEntry.transform.SetParent (relatedDevicesWrapper.transform);
				deviceEntry.transform.localScale = new Vector3 (1, 1, 1);
			}
			related.SetActive (true);
		} else {
			related.SetActive (false);
		}

		if (!DataPaths.ACTIVITY_EXTRA_FEATURES_ENABLED) {
			description.SetActive (false);
		}

		if (!DataPaths.ACTIVITY_RELATED_DEVICES_ENABLED) {
			related.SetActive (false);
		}

	}

    protected override void ProcessTermination()
    {
        Debug.Log("DeviceDetails => Terminated");
    }


		

    protected override void ProcessUpdate()
    {
        
    }

}

