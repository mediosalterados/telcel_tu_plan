﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GLIB.Interface;
using GLIB.Extended;
using System;

using GLIB.Audio;

using TuPlan.Controllers.DataManagement;


public class Qualcom : UIModule<Qualcom>, IActivity  {

	float swipeSpeed = 0.05F;
	float inputX;
	float inputY;

	GameObject _tutPage1;
	GameObject _tutPage2;
	GameObject _tutPage3;

	List<GameObject> _pages;

	RectTransform _tutPage1Rect;
	RectTransform _tutPage2Rect;
	RectTransform _tutPage3Rect;

	int _currentPage;
	int _maxPages = 3;
	List<Promotions> _promotions;

	Toggle[] _toggles;

	bool _doingTransition {
		get {

			bool rval = false;
			if (_pages != null)
			{
				foreach(GameObject page in _pages){
					if (page != null) {
						AnimateComponent animation = page.ResolveComponent<AnimateComponent>();
						if (animation.isTranslating)
							rval = true;
					}
				}
			}

			return rval;
		}
	}

	public void StartActivity()
	{
		Initialize();
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}


	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_QUALCOM_LAYOUT_PATH;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);

		}
	}

	protected override void ProcessInitialization()
	{
		Debug.Log("Tutorial Screen Initialized");



		_tutPage1 = this.Find("Page1", DisplayObject);
		_tutPage2 = this.Find("Page2", DisplayObject);
		_tutPage3 = this.Find("Page3", DisplayObject);

		// Set number one page as current
		_tutPage1Rect = _tutPage1.GetComponent<RectTransform>();
		_currentPage = 0;

		_tutPage2Rect = _tutPage2.GetComponent<RectTransform>();
		Vector2 tutPage2Position = _tutPage2Rect.anchoredPosition;
		tutPage2Position.x = _tutPage2Rect.rect.width;
		_tutPage2Rect.anchoredPosition = tutPage2Position;

		_tutPage3Rect = _tutPage3.GetComponent<RectTransform>();
		Vector2 tutPage3Position = _tutPage3Rect.anchoredPosition;
		tutPage3Position.x = -_tutPage3Rect.rect.width;
		_tutPage3Rect.anchoredPosition = tutPage3Position;

		_pages = new List<GameObject> { _tutPage1, _tutPage2, _tutPage3 };

		_toggles = DisplayObject.GetComponentsInChildren<Toggle>();

		HandlePageToggles(_currentPage, _toggles);

		int index = 0;


	

	}

	protected override void ProcessUpdate()
	{

		if (!_doingTransition)
		{

			/*if (Input.GetMouseButton(0))
                {

                    inputX = Input.GetAxis("Mouse X");

                }

                if (Input.GetKeyDown(KeyCode.D) || inputX < -0.5f)
                    NextPage();
                else if (Input.GetKeyDown(KeyCode.A) || inputX > 0.5f)
                    PrevPage();


                Debug.Log(inputX);


                inputX = 0;*/

			if (Input.GetKeyDown(KeyCode.D))
				NextPage();
			else if (Input.GetKeyDown(KeyCode.A))
				PrevPage();
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{

			Terminate();
		}

	}

	protected override void ProcessTermination()
	{
		Debug.Log("Tutorial Screen Terminated");
	}

	void FixedUpdate()
	{
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			inputX += touchDeltaPosition.x * swipeSpeed;
			inputY += touchDeltaPosition.y * swipeSpeed;
			Debug.Log("X, Y: " + touchDeltaPosition.x + ", " + touchDeltaPosition.y);

			/*Text inputXDebug = this.FindAndResolveComponent<Text>("inputXDebug<Text>", DisplayObject);
                inputXDebug.text = inputX.ToString();
                */

			if (inputX < -1)
				NextPage();
			else if (inputX > 1)
				PrevPage();


			Debug.Log(inputX);


			inputX = 0;
		}

	}

	void NextPage() {

		if (_doingTransition)
			return;

		// Check which is the leftmost page
		float leftMostX = 0;
		GameObject leftMostPage = null;

		foreach (GameObject page in _pages)
		{

			RectTransform rect = page.ResolveComponent<RectTransform>();

			if (rect.anchoredPosition.x < leftMostX) {
				leftMostPage = page;
				leftMostX = rect.anchoredPosition.x;
			}

		}

		foreach (GameObject page in _pages)  {

			// chek the leftmost page and change its position to the rightmost
			RectTransform rect = page.ResolveComponent<RectTransform>();

			if (page == leftMostPage)
			{
				// Place it on the rightmost position
				Vector2 nPos = rect.anchoredPosition;
				nPos.x = rect.rect.width * (_maxPages - 1);
				rect.anchoredPosition = nPos;
			}

			Vector3 tPos = rect.anchoredPosition;
			tPos.x -= rect.rect.width;

			page.ResolveComponent<AnimateComponent>().TranslateObject(tPos, 0.5f, null);


		}

		_currentPage++;

		_currentPage %= _maxPages;

		HandlePageToggles(_currentPage, _toggles);
	}

	void PrevPage() {

		if (_doingTransition)
			return;

		// Check which is the righmost page
		float rightMostX = 0;
		GameObject rightMostPage = null;

		foreach (GameObject page in _pages) {

			RectTransform rect = page.ResolveComponent<RectTransform>();

			if (rect.anchoredPosition.x > rightMostX) {
				rightMostPage = page;
				rightMostX = rect.anchoredPosition.x;
			}

		}

		foreach (GameObject page in _pages)
		{

			// chek the rightmost page and change its position to the leftmost
			RectTransform rect = page.ResolveComponent<RectTransform>();

			if (page == rightMostPage)
			{
				// Place it on the leftmost position
				Vector2 nPos = rect.anchoredPosition;
				nPos.x = -rect.rect.width * (_maxPages - 1);
				rect.anchoredPosition = nPos;
			}

			Vector3 tPos = rect.anchoredPosition;
			tPos.x += rect.rect.width;

			page.ResolveComponent<AnimateComponent>().TranslateObject(tPos, 0.5f, null);


		}

		_currentPage--;

		if (_currentPage < 0)
			_currentPage = _maxPages - 1;

		HandlePageToggles(_currentPage, _toggles);
	}

	void HandlePageToggles(int currentPage, Toggle[] toggles) {

		// shutdown all toggles
		foreach (Toggle toggle in toggles) {

			toggle.isOn = false;

		}

		if (currentPage >= 0 && currentPage < toggles.Length)
			toggles[currentPage].isOn = true;

	}

}
