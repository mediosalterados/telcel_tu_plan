﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using System;

using TuPlan.Controllers.DataManagement;

public class AmigoSinLimitePlanAdjustment : UIModule<AmigoSinLimitePlanAdjustment>, IActivity
{
    public enum PlanAdjustmentModeValues {
        BY_MB = 0,
        BY_COST = 1
    }

    // Currently only the Cost mode is available so we initialize it by default
    public PlanAdjustmentModeValues AdjustmentMode = PlanAdjustmentModeValues.BY_COST;

    List<PrepaidPlans> _amigoSinLimitePlans = new List<PrepaidPlans>();
    PrepaidPlans _amigoSinLimiteSelectedPlan;

    // Cost Mode components
    Text _planCost;
    Slider _planMBSlider;
    Text _MBValue;

    // Shared components between adjustment modes
    Text _expiration;
    Image _twitterIcon;
    Image _facebookIcon;
    Image _faceMessengerIcon;
    Image _whatsAppIcon;
    Text _clauses;
    Button _continueButton;
	Text _promotionsText;
    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_MAXSINLIMITE_PLANADJUSTMENT_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
            
        }
    }

    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModule<AmigoSinLimitePlanAdjustment>.UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override void ProcessInitialization()
    {


		Image uberIcon = this.FindAndResolveComponent<Image>("Uber<Image>", DisplayObject);
		uberIcon.gameObject.SetActive (false);
        _amigoSinLimitePlans = DatabaseManager.Instance.GetAllPrepaidPlans(DatabaseManager.Instance.localDBConnection);


		Image snapchatAppIcon = this.FindAndResolveComponent<Image>("SnapChat<Image>", DisplayObject);
		Image instagramAppIcon = this.FindAndResolveComponent<Image>("Instagram<Image>", DisplayObject);

		snapchatAppIcon.gameObject.SetActive (false);
		instagramAppIcon.gameObject.SetActive (false);

        // Get shared component references
        _expiration = this.FindAndResolveComponent<Text>("ExpirationText<Text>", DisplayObject);
        _twitterIcon = this.FindAndResolveComponent<Image>("Twitter<Image>", DisplayObject);
        _facebookIcon = this.FindAndResolveComponent<Image>("Facebook<Image>", DisplayObject);
        _faceMessengerIcon = this.FindAndResolveComponent<Image>("FaceMessenger<Image>", DisplayObject);
        _whatsAppIcon = this.FindAndResolveComponent<Image>("Whatsapp<Image>", DisplayObject);
        _clauses = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);
		_promotionsText = this.FindAndResolveComponent<Text>("Promo<Text>", DisplayObject);
		Text _buttonContinue = this.FindAndResolveComponent<Text>("Continue<Text>", DisplayObject);
		_buttonContinue.text = SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO ? "IR A RESUMEN":"IR A SELECCIÓN DE EQUIPO";
		_buttonContinue.text = "IR A RESUMEN";
        _continueButton = this.FindAndResolveComponent<Button>("Continue<Button>", DisplayObject);
        _continueButton.onClick.AddListener(delegate {
        SessionManager.Instance.CurrentSessionData.SelectedPlanId = _amigoSinLimiteSelectedPlan.id_amigo_recarga.ToString();

			if(SessionManager.Instance.CurrentSessionData.SelectedDeviceId != null){
            SessionManager.Instance.OpenActivity(AmigoSinLimiteQuotationResult.Instance);
			}else{
				SessionManager.Instance.OpenActivity(DeviceMainFilter.Instance);
			}
            Terminate();            
        });        

        if (AdjustmentMode == PlanAdjustmentModeValues.BY_COST) {
            
            // Get lowest value
            int lowestCost = int.MaxValue;
            
            foreach (PrepaidPlans amigoPlan in _amigoSinLimitePlans) {

                int amigoCost = int.Parse(amigoPlan.recarga);

                if (amigoCost < lowestCost)
                    lowestCost = amigoCost;
            }

            Text minValueText = this.FindAndResolveComponent<Text>("MinValue<Text>", DisplayObject);
            minValueText.text = "$"+lowestCost.ToString();


			Text titleTextValue = this.FindAndResolveComponent<Text>("TitleText<Text>", DisplayObject);
			titleTextValue.text = "Recarga desde:";


            // Get max value
            int maxCost = int.MinValue;

            foreach (PrepaidPlans amigoPlan in _amigoSinLimitePlans)
            {
                Debug.Log("*************");
                Debug.Log(int.Parse(amigoPlan.recarga));

                int amigoCost = int.Parse(amigoPlan.recarga);

                if (amigoCost > maxCost)
                    maxCost = amigoCost;
            }

            Text maxValueText = this.FindAndResolveComponent<Text>("MaxValue<Text>", DisplayObject);
            maxValueText.text = "$"+maxCost.ToString();
                        
            _planCost = this.FindAndResolveComponent<Text>("PlanCost<Text>", DisplayObject);
            _planMBSlider = this.FindAndResolveComponent<Slider>("PlanMB<Slider>", DisplayObject);
            _MBValue = this.FindAndResolveComponent<Text>("MBValue<Text>", DisplayObject);

            // Set maximum MB value
            int maxMBValue = int.MinValue;

            foreach (PrepaidPlans amigoPlan in _amigoSinLimitePlans)
            {
                int amigoMB = amigoPlan.megabytes;

                if (amigoMB > maxMBValue)
                    maxMBValue = amigoMB;
            }
            _planMBSlider.maxValue = maxMBValue;
            _planMBSlider.enabled = false;
            
            Slider costSlider = this.FindAndResolveComponent<Slider>("PlanAdjust<Slider>", DisplayObject);
            costSlider.maxValue = _amigoSinLimitePlans.Count - 1;
            costSlider.onValueChanged.AddListener(delegate (float value) {

                OnCostSliderValueChange(value);

            });
            costSlider.value = 0;
            OnCostSliderValueChange(costSlider.value);

        }        

    }

    protected override void ProcessTermination()
    {
        
    }

    protected override void ProcessUpdate()
    {
        
    }

    public void StartActivity()
    {
        Initialize();
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void OnCostSliderValueChange(float value) {

        if (_amigoSinLimitePlans.Count < 1)
            return;

        _amigoSinLimiteSelectedPlan = _amigoSinLimitePlans[(int)value];

        if (_amigoSinLimiteSelectedPlan != null) {

            _planCost.text = _amigoSinLimiteSelectedPlan.recarga.ToString();
            _planMBSlider.value = _amigoSinLimiteSelectedPlan.megabytes;
			_MBValue.text = string.Format("{0:#,###}", Convert.ToDecimal(_amigoSinLimiteSelectedPlan.megabytes)) + DataMessages.GENERIC_PLAN_MB_LABEL_B;

            _expiration.text = DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_A + _amigoSinLimiteSelectedPlan.vigencia + DataMessages.GENERIC_PLAN_EXPIRATION_VALUE_B;

            _whatsAppIcon.gameObject.SetActive(true);
                        
            bool enableSocialNet = true;
			string amigoClause = "*Redes Sociales para uso nacional, sin costo de navegación los primeros ";
			string mbIncluded = "200 MB";

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) <= 30) {
				enableSocialNet = false;
			} else {
				enableSocialNet = true;
			}

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) == 30) {
				mbIncluded = "300 MB";
			}

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) == 80) {
				mbIncluded = "1,000 MB";
			}

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) == 50) {

				mbIncluded = "500 MB";
			}

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) == 100) {

				mbIncluded = "1,000 MB";
			}

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) == 150 || int.Parse (_amigoSinLimiteSelectedPlan.recarga) == 200) {

				mbIncluded = "1,500 MB";
			}

			string amigo_facil_clause = "";
			string amigo_kit_clause = "";

			if (int.Parse (_amigoSinLimiteSelectedPlan.recarga) > 299) {
				amigo_facil_clause = (DataMessages.AMIGO_FACIL_SIN_LIMITE_RECARGA_CLAUSE).Replace("{MB_DATA}","*Redes sociales incluidas para uso nacional ilimitadas");
				amigo_kit_clause = (DataMessages.AMIGO_KIT_SIN_LIMITE_RECARGA_CLAUSE).Replace ("{MB_DATA}", "*Redes sociales incluidas para uso nacional ilimitadas");
			} else {
				amigo_facil_clause = (DataMessages.AMIGO_FACIL_SIN_LIMITE_RECARGA_CLAUSE).Replace("{MB_DATA}",amigoClause + mbIncluded);
				amigo_kit_clause = (DataMessages.AMIGO_KIT_SIN_LIMITE_RECARGA_CLAUSE).Replace ("{MB_DATA}", amigoClause + mbIncluded);
			}



            _twitterIcon.gameObject.SetActive(enableSocialNet);
            _facebookIcon.gameObject.SetActive(enableSocialNet);
            _faceMessengerIcon.gameObject.SetActive(enableSocialNet);




			_clauses.text = SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL == SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme ? amigo_facil_clause : amigo_kit_clause;
        }


		_promotionsText.text = DataMessages.PROMOTION_MB_AMIGO;
		
    }
}