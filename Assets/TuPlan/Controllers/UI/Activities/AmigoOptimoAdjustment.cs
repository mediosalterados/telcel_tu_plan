﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using System;

using TuPlan.Controllers.DataManagement;

public class AmigoOptimoAdjustment : UIModule<AmigoOptimoAdjustment>, IActivity
{


	List<PrepaidOptPlusPlans> _amigoSinLimitePlans = new List<PrepaidOptPlusPlans>();
	List<Prepaids> _amigoPrepaidPlans = new List<Prepaids>();
	PrepaidOptPlusPlans _maxSinLimiteSelectedPlan;

	// Cost Mode components
	Text _planCost;
	Slider _planMBSlider;
	Slider _planMinSlider;
	Slider _planSMSSlider;
	Text _MBValue;
	Text _MinValue;
	Text _SMSValue;

	// Shared components between adjustment modes
	Text _expiration;
	Image _twitterIcon;
	Image _facebookIcon;
	Image _faceMessengerIcon;
	Image _whatsAppIcon;
	Text _clauses;
	Button _continueButton;
	Text _promotionsText;
	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_AMIGO_OPTIMO_PLANADJUSTMENT_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);

		}
	}

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override void ProcessInitialization()
	{


		_amigoSinLimitePlans = DatabaseManager.Instance.GetAllPrepaidOptPlusPlans(DatabaseManager.Instance.localDBConnection);


		Text _buttonContinue = this.FindAndResolveComponent<Text>("Continue<Text>", DisplayObject);
		_buttonContinue.text = SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO || SessionManager.Instance.CurrentSessionData.SelectedDeviceId != null ? "IR A RESUMEN":"IR A SELECCIÓN DE EQUIPO";


		// Get shared component references
		_expiration = this.FindAndResolveComponent<Text>("ExpirationText<Text>", DisplayObject);
		_twitterIcon = this.FindAndResolveComponent<Image>("Twitter<Image>", DisplayObject);
		_facebookIcon = this.FindAndResolveComponent<Image>("Facebook<Image>", DisplayObject);
		_faceMessengerIcon = this.FindAndResolveComponent<Image>("FaceMessenger<Image>", DisplayObject);
		_whatsAppIcon = this.FindAndResolveComponent<Image>("Whatsapp<Image>", DisplayObject);
		_clauses = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);
		_promotionsText = this.FindAndResolveComponent<Text>("Promo<Text>", DisplayObject);

		Debug.Log("Status inicial"+SessionManager.Instance.CurrentSessionData.SelectedInitialStep);

		_continueButton = this.FindAndResolveComponent<Button>("Continue<Button>", DisplayObject);
		_continueButton.onClick.AddListener(delegate {
	


		//VAMOS PARA RESULTADO DE SIN FRONTERA
			if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO || SessionManager.Instance.CurrentSessionData.SelectedDeviceId != null){
				_buttonContinue.text = "IR A RESUMEN";
				SessionManager.Instance.OpenActivity(AmigoSinLimiteQuotationResult.Instance);
			}else{
				SessionManager.Instance.OpenActivity(DeviceMainFilter.Instance);
			}
			Terminate();            
		});        

		_amigoPrepaidPlans = new List<Prepaids>();


		foreach (PrepaidOptPlusPlans amigoPlan in _amigoSinLimitePlans) {

			Prepaids plan = DatabaseManager.Instance.GetPrepaidByPrepaidId(amigoPlan.rate_id,DatabaseManager.Instance.localDBConnection);
			_amigoPrepaidPlans.Add(plan);
		}

	
			Text titleValueText = this.FindAndResolveComponent<Text> ("TitleText<Text>", DisplayObject);
			titleValueText.text = "Recarga desde";
			// Get lowest value
			int lowestCost = int.MaxValue;
		foreach (PrepaidOptPlusPlans amigoPlan in _amigoSinLimitePlans) {
			int amigoCost = amigoPlan.recarga_saldo;
				if (amigoCost < lowestCost)
					lowestCost = amigoCost;
			}

			Text minValueText = this.FindAndResolveComponent<Text> ("MinValue<Text>", DisplayObject);
			minValueText.text = "$" + lowestCost.ToString ();

			// Get max value
			int maxCost = int.MinValue;

		foreach (PrepaidOptPlusPlans amigoPlan in _amigoSinLimitePlans) {
			int amigoCost = amigoPlan.recarga_saldo;
				if (amigoCost > maxCost)
					maxCost = amigoCost;
			}

		_MBValue = this.FindAndResolveComponent<Text> ("MBTextValue<Text>", DisplayObject);
		_MinValue = this.FindAndResolveComponent<Text> ("MinTextValue<Text>", DisplayObject);
		_SMSValue = this.FindAndResolveComponent<Text> ("SMSTextValue<Text>", DisplayObject);

			Text maxValueText = this.FindAndResolveComponent<Text> ("MaxValue<Text>", DisplayObject);
			maxValueText.text = "$" + maxCost.ToString ();

			_planCost = this.FindAndResolveComponent<Text> ("PlanCost<Text>", DisplayObject);


			Slider costSlider = this.FindAndResolveComponent<Slider> ("PlanAdjust<Slider>", DisplayObject);
		costSlider.maxValue = _amigoSinLimitePlans.Count - 1;
			costSlider.onValueChanged.AddListener (delegate (float value) {
				OnCostSliderValueChange (value);
			});
			costSlider.value = 0;
			OnCostSliderValueChange (costSlider.value);

		}


	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

	public void StartActivity()
	{
		Initialize();
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public void OnCostSliderValueChange(float value) {

		if (_amigoSinLimitePlans.Count < 1)
			return;

		_maxSinLimiteSelectedPlan = _amigoSinLimitePlans [(int)value];
		Prepaids prepaid_plan = _amigoPrepaidPlans [(int)value];

		_promotionsText.text = DataMessages.PROMOTION_MB_AMIGO;

		if (_amigoSinLimitePlans != null) {
			_planCost.text = _maxSinLimiteSelectedPlan.recarga_saldo.ToString ();
			_MBValue.text = "$ " + prepaid_plan.megabyte_cost.ToString ();
			_MinValue.text = "$ " + prepaid_plan.minute_carrier_cost.ToString ();
			_SMSValue.text = "$ " + prepaid_plan.sms_cost.ToString ();


			bool enableSocialNet = true;

			if (_maxSinLimiteSelectedPlan.recarga_saldo <= 30)
				enableSocialNet = false;
			else
				enableSocialNet = true;


			Text expirationSaldo = this.FindAndResolveComponent<Text> ("ExpirationSaldoText<Text>", DisplayObject);
			Text expirationVigencia = this.FindAndResolveComponent<Text> ("ExpirationVigenciaText<Text>", DisplayObject);
			Text expirationRedesVigencia = this.FindAndResolveComponent<Text> ("ExpirationRedesVigenciaText<Text>", DisplayObject);
		
			expirationSaldo.text = "$"+_maxSinLimiteSelectedPlan.saldo_total.ToString ();
			expirationVigencia.text = _maxSinLimiteSelectedPlan.vigencia_saldo.ToString () + " días";
			expirationRedesVigencia.text = _maxSinLimiteSelectedPlan.vigencia_redes_sociales.ToString () + " días";

			SessionManager.Instance.CurrentSessionData.SelectedPlanId = _maxSinLimiteSelectedPlan.ID.ToString ();
			_whatsAppIcon.gameObject.SetActive (true);
			_twitterIcon.gameObject.SetActive (enableSocialNet);
			_facebookIcon.gameObject.SetActive (enableSocialNet);
			_faceMessengerIcon.gameObject.SetActive (enableSocialNet);
			if(SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme == SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL){				
				_clauses.text = (DataMessages.AMIGO_FACIL_SIN_FRONTERA_RECARGA_CLAUSE).Replace("{MB_DATA}",AmigoSinLimiteQuotationResult.getRecargaNormalTxt(_maxSinLimiteSelectedPlan.recarga_saldo));
			}else{
				//CAMBIAR ESTE EN KIT
				_clauses.text = (DataMessages.AMIGO_KIT_SIN_LIMITE_CLAUSE).Replace("{MB_DATA}",AmigoSinLimiteQuotationResult.getRecargaNormalTxt(_maxSinLimiteSelectedPlan.recarga_saldo));
			}
		}
		

		}

	
}