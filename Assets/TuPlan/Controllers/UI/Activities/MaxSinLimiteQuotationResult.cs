using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;

using TuPlan.Controllers.DataManagement;

public class MaxSinLimiteQuotationResult : UIModule<MaxSinLimiteQuotationResult>, IActivity
{

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_AMIGOSINLIMITE_QUOTATIONRESULT_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);
		}
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public void StartActivity()
	{
		Initialize();
	}

	protected override void ProcessInitialization()
	{
		Debug.Log ("P inicializado");

		String LegalesShare = "";

		GameObject relatedDevicesWrapper = this.Find("RelatedDevices<Wrapper>", DisplayObject);
		GameObject related = this.Find("Item 1", DisplayObject);

		//APAGO EL SLIDER QUE SOLO SIRVE EN AMIGO
		GameObject secondarySliders = this.Find ("SecondarySliders<Wrapper>", DisplayObject);
		secondarySliders.SetActive (false);

		GameObject optPlusResume = this.Find("RowC<Wrapper>", DisplayObject);
		optPlusResume.SetActive (false);



		// Get required entities
		Devices device = DatabaseManager.Instance.GetDeviceByDeviceId(SessionManager.Instance.CurrentSessionData.SelectedDeviceId, DatabaseManager.Instance.localDBConnection);
		Brands brand = DatabaseManager.Instance.GetBrandByBrandId(device.brand_id, DatabaseManager.Instance.localDBConnection);

		Plans currentPlan = DatabaseManager.Instance.GetPlanByPlanId (SessionManager.Instance.CurrentSessionData.SelectedPlanId, DatabaseManager.Instance.localDBConnection);

		List<DevicePlans> devicePlans = DatabaseManager.Instance.GetPlanFromDevice(device, currentPlan, DataManager.Instance.DataVersion, DatabaseManager.Instance.localDBConnection);


		// Initialize activity components
		Text planName = this.FindAndResolveComponent<Text>("PlanName<Text>", DisplayObject);
		planName.text = currentPlan.plan_name;

		Image phoneThumb = this.FindAndResolveComponent<Image>("PhoneThumb<Image>", DisplayObject);

		StartCoroutine(loadAssets(Application.persistentDataPath + device.img_filepath,phoneThumb));

		Image phoneLTE = this.FindAndResolveComponent<Image>("PhoneLTE<Image>", DisplayObject);
		Texture2D tex_net = null;

		switch (device.network) {
		case "VO":
			tex_net = (Texture2D)Resources.Load ("Sprites/volte");
			break;
		case "4G":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		case "45":
			tex_net = (Texture2D)Resources.Load ("Sprites/GigaRed");
			break;
		case "":
			tex_net = (Texture2D)Resources.Load ("Sprites/4g");
			break;
		}

		Sprite sprite_net = Sprite.Create(tex_net, new Rect(0, 0, tex_net.width, tex_net.height), new Vector2(0f, 0f));

		phoneLTE.sprite = sprite_net;
		phoneLTE.gameObject.SetActive(device.lte);

		Text phoneName = this.FindAndResolveComponent<Text>("PhoneModel<Text>", DisplayObject);
		phoneName.text = device.model_name;

		Text phoneBrand = this.FindAndResolveComponent<Text>("PhoneBrand<Text>", DisplayObject);
		phoneBrand.text = brand.brand_name;

		Text priceValue = this.FindAndResolveComponent<Text>("PriceValue<Text>", DisplayObject);
		priceValue.text = "$ "+string.Format("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal(device.prepaid_cost));

		Text promotionsText = this.FindAndResolveComponent<Text>("Promo<Text>", DisplayObject);

		Slider costSlider = this.FindAndResolveComponent<Slider>("Cost<Slider>", DisplayObject);
		costSlider.minValue = 0;
		costSlider.maxValue = float.Parse(DatabaseManager.Instance.GetLimitCostoValueFromPlan(false,currentPlan,DatabaseManager.Instance.localDBConnection));
		costSlider.value = float.Parse(currentPlan.monthly_cost);
		costSlider.enabled = false;

		Text costValue = this.FindAndResolveComponent<Text>("CostValue<Text>", DisplayObject);
		costValue.text = string.Format("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal(currentPlan.monthly_cost));

		Slider mbSlider = this.FindAndResolveComponent<Slider>("MB<Slider>", DisplayObject);
		mbSlider.minValue = 0;
		mbSlider.maxValue = (float)DatabaseManager.Instance.GetLimitMBValueFromPlan(false,currentPlan, DatabaseManager.Instance.localDBConnection);
		mbSlider.value = (float)currentPlan.megabytes_inc;
		mbSlider.enabled = false;

		Text mbValue = this.FindAndResolveComponent<Text>("MBValue<Text>", DisplayObject);
		mbValue.text =  string.Format("{0:#,###}", Convert.ToDecimal(currentPlan.megabytes_inc)) + DataMessages.GENERIC_PLAN_MB_LABEL_B;



		Image twitterIcon = this.FindAndResolveComponent<Image>("Twitter<Image>", DisplayObject);
		Image facebookIcon = this.FindAndResolveComponent<Image>("Facebook<Image>", DisplayObject);
		Image faceMessengerIcon = this.FindAndResolveComponent<Image>("FaceMessenger<Image>", DisplayObject);
		Image whatsappIcon = this.FindAndResolveComponent<Image>("Whatsapp<Image>", DisplayObject);
		Image uberIcon = this.FindAndResolveComponent<Image>("Uber<Image>", DisplayObject);
		Image snapchatAppIcon = this.FindAndResolveComponent<Image>("SnapChat<Image>", DisplayObject);
		Image instagramAppIcon = this.FindAndResolveComponent<Image>("Instagram<Image>", DisplayObject);
		Text planClause = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);

		bool displaySocialNets = false;

		whatsappIcon.gameObject.SetActive(true);
		if(currentPlan.tipo_plan == "MAX_SL_PYME"){
			if(currentPlan.megabytes_inc > 3500){
				uberIcon.gameObject.SetActive (true);	
			}else{
				uberIcon.gameObject.SetActive (false);	
			}
		}else{
			uberIcon.gameObject.SetActive (HasUber (currentPlan));
		}

		snapchatAppIcon.gameObject.SetActive (HasUber (currentPlan));
		instagramAppIcon.gameObject.SetActive (HasUber (currentPlan));

	


		if (devicePlans.Count > 0) {
			String pymeMessage = (DataMessages.MAX_SIN_LIMITE_PYME_RESUME_CLAUSE).Replace ("{PAYMENT}",currentPlan.additional_cost);

			String mbIncluded = "1,000 MB";

			if (currentPlan.megabytes_inc >= 2000 && currentPlan.megabytes_inc < 8000 && currentPlan.tipo_plan != "MAX") {
				mbIncluded = "2,000 MB";
			}else if(currentPlan.megabytes_inc >= 8000 && currentPlan.tipo_plan != "MAX"){
				mbIncluded = "3,000 MB";
			}
			String messageSl = "\n*Redes sociales sin costo los primeros {MB_DATA} \n*Costo MB adicional a $0.25";



			if(SessionManager.Instance.CurrentSessionData.SelectedPlanType == "INT_HOME"){
				planClause.text = DataMessages.INTERNET_HOME_CLAUSE;
				GameObject DataAmigo = this.Find ("DataAmigoPlus<Wrapper>", DisplayObject);
				DataAmigo.SetActive(false);
			}else{
				planClause.text = currentPlan.tipo_plan == "MAX_SL" || currentPlan.tipo_plan == "MAX" ? DataMessages.MAX_SIN_LIMITE_RESUME_CLAUSE +  messageSl : pymeMessage;
			}
			

//			if ((currentPlan.tipo_plan == "MAX_SL_PYME" && currentPlan.megabytes_inc > 2500) ||  (currentPlan.tipo_plan == "MAX_SL" && currentPlan.megabytes_inc > 2000)) {
			if ((currentPlan.tipo_plan == "MAX_SL_PYME" && currentPlan.megabytes_inc > 1000) || (currentPlan.tipo_plan == "MAX_SL" && currentPlan.megabytes_inc >= 2000)) {
				planClause.text = DataMessages.MAX_SIN_LIMITE_MB_SC_CLAUSE;
			}

			planClause.text = planClause.text.Replace("{MB_DATA}",mbIncluded);
			LegalesShare = planClause.text.Replace("{MB_DATA}",mbIncluded);

		} else {
			planClause.text = DataMessages.GENERIC_CLAUSE;

		}
		displaySocialNets = true;



		twitterIcon.gameObject.SetActive(displaySocialNets);
		facebookIcon.gameObject.SetActive(displaySocialNets);
		faceMessengerIcon.gameObject.SetActive(displaySocialNets);

		Text planAdditionalInfo = this.FindAndResolveComponent<Text>("PlanAdditionalRowTitle<Text>", DisplayObject);
		planAdditionalInfo.text = "Pago inicial en "+currentPlan.plan_name;

		GameObject w12MonthWrapper = this.Find("12Months<Wrapper>", DisplayObject);
		Text t12MonthText = this.FindAndResolveComponent<Text>("12MonthValue<Text>", DisplayObject);
		GameObject separator12to24 = this.Find("Separator12to24<Image>", DisplayObject);
		GameObject w24MonthWrapper = this.Find("24Months<Wrapper>", DisplayObject);
		Text t24MonthText = this.FindAndResolveComponent<Text>("24MonthValue<Text>", DisplayObject);
		GameObject separator24to36 = this.Find("Separator24to36<Image>", DisplayObject);
		GameObject w36MonthWrapper = this.Find("36Months<Wrapper>", DisplayObject);
		Text t36MonthText = this.FindAndResolveComponent<Text>("36MonthValue<Text>", DisplayObject);

		Text ta12MonthText = this.FindAndResolveComponent<Text>("12AditionalMonthValue<Text>", DisplayObject);
		Text ta24MonthText = this.FindAndResolveComponent<Text>("24AditionalMonthValue<Text>", DisplayObject);
		Text ta36MonthText = this.FindAndResolveComponent<Text>("36AditionalMonthValue<Text>", DisplayObject);


		w12MonthWrapper.SetActive(false);
		separator12to24.SetActive(false);
		w24MonthWrapper.SetActive(false);
		separator24to36.SetActive(false);
		w36MonthWrapper.SetActive(false);

		if (devicePlans.Count >= 1) {
			w12MonthWrapper.SetActive(true);
			decimal decimal_value = Convert.ToDecimal (devicePlans [0].cost);
			string string_cost = decimal_value > 0 ? string.Format ("{0:#,### ; #.## ,00##,00}", decimal_value):"0";
			t12MonthText.text = "$ " + string_cost;
			decimal_value = Convert.ToDecimal (devicePlans [0].additional_cost);

			Debug.Log ("costo adicional"+decimal_value);

			string_cost = decimal_value > 0 ? string.Format ("{0:#,###.## ; ##.## ,00##,00.##}", decimal_value):"0";
			ta12MonthText.text = "$ " + string_cost;
			if (devicePlans.Count >= 2) {
				separator12to24.SetActive(true);
				w24MonthWrapper.SetActive(true);

				decimal decimal_value2 = Convert.ToDecimal (devicePlans [1].cost);
				string string_cos2t = decimal_value2 > 0 ? string.Format ("{0:#,### ; #.## ,00##,00}", decimal_value2):"0";

				t24MonthText.text = "$ " + string_cos2t;

				decimal_value2 = Convert.ToDecimal (devicePlans [1].additional_cost);
				string_cost = decimal_value2 > 0 ? string.Format ("{0:#,###.## ; ##.## ,00##,00.##}", decimal_value2):"0";
				ta24MonthText.text = "$ " + string_cost;

				if (devicePlans.Count >= 3) {
					separator24to36.SetActive(true);
					w36MonthWrapper.SetActive(true);

					decimal decimal_value3 = Convert.ToDecimal (devicePlans [2].cost);



					string string_cos3t = decimal_value3 > 0 ? string.Format ("{0:#,### ; #.## ,00##,00}", decimal_value3):"0";

					t36MonthText.text = "$ " + string_cos3t;

					decimal_value3 = Convert.ToDecimal (devicePlans [2].additional_cost);
					string_cost = decimal_value3 > 0 ? string.Format ("{0:#,###.## ; ##.## ,00##,00.##}", decimal_value3):"0";
					ta36MonthText.text = "$ " + string_cost;


				}

			}

		}

		if (devicePlans.Count > 0) {
			SessionManager.Instance.CurrentSessionData.ShareMessage = "Llévate el " + device.model_name + " a 24 meses con el plan " + currentPlan.plan_name + " por $ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (devicePlans [0].cost)) + "\n *Precios sujetos a cambio sin previo aviso.";
			SessionManager.Instance.CurrentSessionData.ShareEmail = "Llévate el " + device.model_name + " a 24 meses con el plan " + currentPlan.plan_name + " por $ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (devicePlans [0].cost)) + "\n *Precios sujetos a cambio sin previo aviso.";
		} else {
			GameObject rowWrapper = this.Find ("PlanAdditionalRow<Wrapper>", DisplayObject);
			rowWrapper.SetActive (false);
		}
		// For now we turn off Amigo Facil Section
		GameObject amigoFacilRow = this.Find("AmigoFacilRow<Wrapper>", DisplayObject);
		amigoFacilRow.SetActive(false);

		DateTime dateTime = System.DateTime.Now;

		String free_numbers = currentPlan.free_numbers.ToString() == "1000000" ? "No aplica núm. gratis" : currentPlan.free_numbers.ToString();
		String minutes_inc = currentPlan.minutes_inc.ToString () == "1000000" ? "Min. ilimitados" : currentPlan.minutes_inc.ToString();
		String sms_inc = currentPlan.sms_inc.ToString() == "1000000" ? "SMS ilimitados" : currentPlan.sms_inc.ToString();


		String social="";
		if (currentPlan.social_networks == true) {
			if (currentPlan.plan_name != "Telcel Max Sin Límite 1500" && currentPlan.plan_name != "Telcel Max Sin Límite 2000") {
				social = "Facebook, Twitter, WhatsApp, Instagram, Snapchat y Uber ilimitados";
			}else{
				social = "Facebook, Twitter y WhatsApp ilimitados";
			}	
		} else {
			social = "";
		}
			
		String message = "¡Hola!\n" +
			"Te comparto la oferta de TELCEL, que es de tu interés\n\n" +
			"Oferta vigente para el " + dateTime.ToString ("dd/MM/yyyy") + "\n\n" +
			currentPlan.plan_name + "\n\n" +
			"- $ " + currentPlan.monthly_cost + " de renta mensual ***\n" +
			"- " + free_numbers + " **\n" +
			"- " + minutes_inc + " *\n" +
			"- " + currentPlan.megabytes_inc + " MB *\n" +
			"* " + social + " **\n" +
			"- " + sms_inc + "*\n\n" +
			device.device_name + "\n"
			+ "en " + currentPlan.plan_name + "\n" +
			"Contratando a 12 meses\n";

		if (devicePlans.Count > 0) {
			message= message + "$ "+devicePlans[0].cost+"\n"+
				"Contratando a 18 meses\n"+
				"$ "+devicePlans[1].cost+"\n"+
				"Contratando a 24 meses\n"+
				"$ "+devicePlans[2].cost+"\n\n\n";
		}


		message = message + "Precios sujetos a cambio sin previo aviso\n" +
			"Recuerda que el pago inicial por equipo, lo puedes diferir y pagar mensualmente con cargo a tu factura Telcel o pregunta por las tarjetas bancarias participantes para pagos a MSI\n" +
			"* Indistintos en México, E.U.A. y Canadá" +
			"** Indistintos nacionales" +
			"$ " + currentPlan.additional_cost + "/mes\n" +
			"*El cargo sólo aplica en caso de realizar una contratación con equipo celular (plazos 12, 18, 24 ó 36 meses). No aplica para planes en Plazo Libre. Si el usuario lleva su “Pago al Corriente” se realizará un descuento por el monto que corresponda al Cargo Mensual por Equipo aplicado. Entiéndase como “Pago al Corriente” liquidar el 100% del Cargo Mensual Total dentro de los 30 días naturales posteriores a su fecha de corte.\n"+
			LegalesShare;

		

		if (currentPlan.legals != "") {			
			message += "\n" + currentPlan.legals.Replace("|",System.Environment.NewLine);
		}

	
		SessionManager.Instance.CurrentSessionData.ShareEmail = message;

		Button shareButton = this.FindAndResolveComponent<Button>("Share<Button>", DisplayObject);
		shareButton.onClick.AddListener (delegate {ShareMenu.Instance.Initialize();});


		Button legalesButton = this.FindAndResolveComponent<Button>("ExpandLegals<Button>", DisplayObject);
		legalesButton.onClick.AddListener (delegate {LegalesExpanded.Instance.Initialize();});

		if (currentPlan.tipo_plan == "MAX_SL") {
			legalesButton.gameObject.SetActive (true);
		} else {
			legalesButton.gameObject.SetActive (false);
		}



		Button goHomeButton = this.FindAndResolveComponent<Button>("GoHome<Button>", DisplayObject);
		goHomeButton.onClick.AddListener(
			delegate {
				SessionManager.Instance.ClearSession(false);
				SessionManager.Instance.OpenActivity(Home.Instance);
				Terminate();
			}            
		);

		GameObject phonePrice = this.Find ("PhonePrice<Wrapper>", DisplayObject);
		//phonePrice.SetActive (false);

		GameObject featureBB = this.Find ("FeatureBB<Wrapper>", DisplayObject);


		GameObject expirationWrapper = this.Find ("FeatureB<Wrapper>", DisplayObject);


		if (currentPlan.minutes_inc == 1000000 && currentPlan.sms_inc == 1000000) {
			featureBB.active = false;
		} else {
			Text _smsText = this.FindAndResolveComponent<Text>("SMS<Text>", DisplayObject);
			Text _minText = this.FindAndResolveComponent<Text>("Minutos<Text>", DisplayObject);

			_smsText.text = "SMS incluidos: " + currentPlan.sms_inc;
			_minText.text = "Minutos incluidos: " + currentPlan.minutes_inc;


			expirationWrapper.active = false;
		}



		if (currentPlan.tipo_plan == "MAX_SL_PYME") {
			if(currentPlan.megabytes_inc > 3500){
				planClause.text += "\n" + DataMessages.UBER_GENERIC_CLAUSE;	
			}
		} else {
			planClause.text += "\n" + DataMessages.UBER_GENERIC_CLAUSE;
		}



		GameObject promotionPrice = this.Find ("PromotionPrice<Wrapper>", DisplayObject);
		Text priceTxt = this.FindAndResolveComponent<Text> ("PromotionValue<Text>", DisplayObject);

		if (float.Parse (currentPlan.promotion) > 0) {
			promotionPrice.gameObject.SetActive (false);
			priceTxt.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", float.Parse (currentPlan.promotion)) + " al mes";
			//planClause.text = DataMessages.GENERIC_PROMOTION+planClause.text;
		} else {
			promotionPrice.gameObject.SetActive (false);
		}

//		Retiro consumo de DataMessages para promociones
//		if (currentPlan.tipo_plan == "MAX" || currentPlan.tipo_plan == "MAX_SL") {
//			promotionsText.text = DataMessages.PROMOTION_MB_PLAN;
//		}else if(currentPlan.tipo_plan == "MAX_PYME"){
//			promotionsText.text = DataMessages.PROMOTION_SMS_PYME;
//		} else {
//			promotionsText.text = "";
//		}

		promotionsText.text = currentPlan.legals;

		List<Devices> relatedDevices = DatabaseManager.Instance.GetRelatedDevices (device.device_id,DatabaseManager.Instance.localDBConnection);

		if(relatedDevices.Count > 0){
			foreach (Devices deviceRelated in relatedDevices) {
				Brands relatedBrand = DatabaseManager.Instance.GetBrandByBrandId (deviceRelated.brand_id, DatabaseManager.Instance.localDBConnection);
				GameObject deviceEntry = Instantiate<GameObject> (Resources.Load<GameObject> (DataPaths.ELEMENT_DEVICE_RELATED_LAYOUT_PATH));
				Text modelName = this.FindAndResolveComponent<Text> ("DeviceName<Text>", deviceEntry);
				Text brandName = this.FindAndResolveComponent<Text> ("PhoneBrand<Text>", deviceEntry);
				Text deviceName = this.FindAndResolveComponent<Text> ("PhoneName<Text>", deviceEntry);
				Text priceLabel = this.FindAndResolveComponent<Text> ("PriceText<Text>", deviceEntry);
				if (relatedBrand != null) {
					brandName.text = relatedBrand.brand_name;
					deviceName.text = deviceRelated.device_name;
					modelName.text = deviceRelated.model_name;
				}

				priceLabel.text = "$ " + string.Format ("{0:#,### ; #.## ,00##,00}", Convert.ToDecimal (deviceRelated.prepaid_cost));

				Image deviceEntryImage = this.FindAndResolveComponent<Image> ("DeviceName<Image>", deviceEntry);


				if (string.IsNullOrEmpty (device.img_filepath))
					StartCoroutine (LoadCacheTextureIntoImage (deviceEntryImage, deviceRelated.img_filepath));
				else {
					StartCoroutine(loadAssets(Application.persistentDataPath + deviceRelated.img_filepath,deviceEntryImage));
					//StartCoroutine(LoadCacheTextureIntoImage(planTypeEntryImg, DataPaths.WS_URL + planType.plan_img));
				}
				Button deviceEntryTrigger = deviceEntry.GetComponent<Button> ();
				deviceEntryTrigger.onClick.AddListener (delegate {
					SessionManager.Instance.ClearSession ();
					SessionManager.Instance.CurrentSessionData.SelectedDeviceId = deviceRelated.device_id;
					SessionManager.Instance.OpenActivity (DeviceDetails.Instance);
					Terminate ();

				});

				deviceEntry.transform.SetParent (relatedDevicesWrapper.transform);
				deviceEntry.transform.localScale = new Vector3 (1, 1, 1);
			}
			related.SetActive (true);
			}else{
				related.SetActive (false);
			}


		if (!DataPaths.ACTIVITY_RELATED_DEVICES_ENABLED) {
			related.SetActive (false);
		}


	
		if (currentPlan.tipo_plan == "MAX_SL_PYME") {
			instagramAppIcon.gameObject.SetActive (false);
			snapchatAppIcon.gameObject.SetActive(false);
			faceMessengerIcon.gameObject.SetActive (false);
		}

	}

	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

	public static bool HasUber(Plans plan){
		bool val = false;
		if ((plan.tipo_plan == "MAX_SL_PYME" || plan.tipo_plan == "MAX_SL") && plan.megabytes_inc >= 2000) {
			val = true;
		}

		return val;
	}


	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0f, 0f));

			image.sprite = sprite;
		}
	}


}


