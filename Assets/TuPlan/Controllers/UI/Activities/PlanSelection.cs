﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;

using TuPlan.Controllers.DataManagement;

public class PlanSelection : UIModule<PlanSelection>, IActivity
{
    
    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_PLANSELECTION_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
        }
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }

    public void StartActivity()
    {
        Initialize();
    }

    protected override void ProcessInitialization()
    {

        GameObject plansContainer = this.Find("Plans<Wrapper>", DisplayObject);


        List<PlanTypes> planTypes = new List<PlanTypes>();

	

		if (SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.POSTPAID || SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.NONE) {
			planTypes = DatabaseManager.Instance.GetAllPlanTypes (DatabaseManager.Instance.localDBConnection);  
			//HAY QUE VERIFICAR SI HAY ALGUN DISPOSITIVO SELECCIONADO

			if(SessionManager.Instance.CurrentSessionData.SelectedDeviceId != null){

				planTypes = DatabaseManager.Instance.GetAllPlansTypeAndByDevice(SessionManager.Instance.CurrentSessionData.SelectedDeviceId,DataManager.Instance.DataVersion,DatabaseManager.Instance.localDBConnection);

			}
		}
        
		if (SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.PREPAID || SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.NONE) {


			planTypes = new List<PlanTypes>();
			PlanTypes amigoFacil = new PlanTypes();

			amigoFacil.type_plan = SessionData.AMIGO_FACIL_TYPE;
			amigoFacil.nombre_plan = "Amigo Fácil";
			amigoFacil.eu_canada = true;
			amigoFacil.prioridad = "1000";
			amigoFacil.plan_img = DataPaths.WSAmigoFacilImgURL;
			amigoFacil.plan_img_filepath = "";

			planTypes.Add(amigoFacil);

			PlanTypes amigoKit = new PlanTypes();

			amigoKit.type_plan = SessionData.AMIGO_KIT_TYPE;
			amigoKit.nombre_plan = "Amigo Kit";
			amigoKit.eu_canada = true;
			amigoKit.prioridad = "1000";
			amigoKit.plan_img = DataPaths.WSAmigoKitImgURL;
			amigoKit.plan_img_filepath = "";

			planTypes.Add(amigoKit);
		}



	

        foreach (PlanTypes planType in planTypes)
        {
            GameObject planTypeEntry = Instantiate<GameObject>(Resources.Load<GameObject>(DataPaths.ELEMENT_PLANENTRY_LAYOUT_PATH));

            planTypeEntry.name = planType.type_plan;
	
            Image planTypeEntryImg = this.FindAndResolveComponent<Image>("PlanLogo<Image>", planTypeEntry);



            if (string.IsNullOrEmpty(planType.plan_img_filepath))
                StartCoroutine(LoadCacheTextureIntoImage(planTypeEntryImg, planType.plan_img));
            else {

				StartCoroutine(loadAssets(Application.persistentDataPath + planType.plan_img_filepath,planTypeEntryImg));
                // Max Plan images are in another url
                //StartCoroutine(LoadCacheTextureIntoImage(planTypeEntryImg, DataPaths.WS_URL + planType.plan_img));

            }
            Button planTypeEntryTrigger = this.FindAndResolveComponent<Button>("Trigger<Button>", planTypeEntry);
            planTypeEntryTrigger.onClick.AddListener(delegate {

				if (SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.PREPAID)
                {
					//MUESTRO PRE RESUMEN DE VALORES
					if(planTypeEntry.name == SessionData.AMIGO_FACIL_TYPE){
						SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme = SessionData.PaymentPrepaidSelectionValues.AMIGO_FACIL;

						if(SessionManager.Instance.CurrentSessionData.SelectedDeviceId != null){
                            if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO){
                                SessionManager.Instance.OpenActivity(AmigoSelection.Instance);
                                }else{
                                    SessionManager.Instance.OpenActivity(AmigoFacilQuotationResult.Instance);
                                }
						}else{
							SessionManager.Instance.OpenActivity(DeviceMainFilter.Instance);
						}
					}else{
						SessionManager.Instance.CurrentSessionData.SelectedPrepaidPaymentScheme = SessionData.PaymentPrepaidSelectionValues.AMIGO_KIT;
						SessionManager.Instance.OpenActivity(AmigoSelection.Instance);
					}

					Terminate();
				}else{
					SessionManager.Instance.CurrentSessionData.SelectedPlanType = planTypeEntry.name;
					SessionManager.Instance.OpenActivity(MaxSinLimitePlanAdjustment.Instance);
					Terminate();
				}

            });

            planTypeEntry.transform.SetParent(plansContainer.transform);
            planTypeEntry.transform.localScale = new Vector3(1, 1, 1);

        }



    }

    protected override void ProcessTermination()
    {
        
    }

    protected override void ProcessUpdate()
    {
        
    }

	IEnumerator loadAssets(String file,Image image){
		yield return new WaitForSeconds (0.3f);
		if (System.IO.File.Exists(file))
		{
			byte[] bytes = System.IO.File.ReadAllBytes(file);

			Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, false, true);
			tex.Apply();
			tex.LoadImage(bytes);

			Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0f, 0f));

			image.sprite = sprite;
		}
	}
    
}
