﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

public class DeviceMainFilter : UIModule<DeviceMainFilter>, IActivity {

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_DEVICEMAINFILTER_LAYOUT_PATH;           
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
            
        }
    }
      

    protected override void ProcessInitialization()
    {
        Debug.Log("DeviceMainFilter => Started");

	

        Button snapDragonFilterButton = this.FindAndResolveComponent<Button>("SnapdragonFilter<Button>", DisplayObject);
        snapDragonFilterButton.onClick.AddListener(delegate {
            SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter = SessionData.DeviceMainFilterSelectionValues.SNAPDRAGON;
            SessionManager.Instance.OpenActivity(DeviceBrandFilter.Instance);
            Terminate();
        });

        Button byBrandFilterButton = this.FindAndResolveComponent<Button>("ByBrand<Button>", DisplayObject);
        byBrandFilterButton.onClick.AddListener(delegate
        {
            SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter = SessionData.DeviceMainFilterSelectionValues.NO_FILTER;
            SessionManager.Instance.OpenActivity(DeviceBrandFilter.Instance);
            Terminate();
        });

		Button noFeeButton = this.FindAndResolveComponent<Button>("Nofee<Button>", DisplayObject);
		noFeeButton.onClick.AddListener(delegate
			{
				SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter = SessionData.DeviceMainFilterSelectionValues.NO_ADVANCE_PAYMENT;
				SessionManager.Instance.OpenActivity(DeviceBrandFilter.Instance);
				Terminate();
			});

		if (SessionManager.Instance.CurrentSessionData.SelectedPaymentScheme == SessionData.PaymentSchemeSelectionValues.POSTPAID) {
			noFeeButton.gameObject.SetActive(true);
		
		} else {
			noFeeButton.gameObject.SetActive(false);

		}

 

    }

    protected override void ProcessUpdate()
    {

    }

    protected override void ProcessTermination()
    {
        Debug.Log("DeviceMainFilter => Terminated");
    }

    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    public void StartActivity()
    {
        Initialize();
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }
}
