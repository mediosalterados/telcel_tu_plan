﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;

using System;

public class Home : UIModule<Home>, IActivity {

    public bool isActivityReady
    {
        get
        {
            if (UIModuleCurrentStatus == UIModule<Home>.UIModuleStatusModes.STAND_BY)
                return true;
            else
                return false;
        }
    }

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.ACTIVITY_HOME_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
        }
    }

    protected override void ProcessInitialization()
    {
        Debug.Log("Home Activity => Started");


		Button superEquipos = this.FindAndResolveComponent<Button>("UpperRow<Wrapper>", DisplayObject);

		superEquipos.onClick.AddListener(delegate { 
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.ClearSession();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.EQUIPO;
			SessionManager.Instance.CurrentSessionData.SelectedDeviceMainFilter = SessionData.DeviceMainFilterSelectionValues.LTE_SNAPDRAGON;
			SessionManager.Instance.OpenActivity(DeviceBrandFilter.Instance);
			Terminate(); });

        Button tuEquipoButton = this.FindAndResolveComponent<Button>("TuEquipo<Button>", DisplayObject);

        tuEquipoButton.onClick.AddListener(delegate { 
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.ClearSession();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.EQUIPO;
			SessionManager.Instance.OpenActivity(DeviceMainFilter.Instance); 
			Terminate(); });

        Button tuPlanButton = this.FindAndResolveComponent<Button>("TuPlan<Button>", DisplayObject);

		tuPlanButton.onClick.AddListener(delegate { 
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.ClearSession();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_PLAN;
			SessionManager.Instance.OpenActivity(PaymentSchemeSelection.Instance); Terminate(); });
	
        Button tusPromosButton = this.FindAndResolveComponent<Button>("TusPromociones<Button>", DisplayObject);
        tusPromosButton.onClick.AddListener(delegate { 
			SessionManager.Instance.OpenActivity(PromotionsScreen.Instance); Terminate(); });

        Button tuCostoMButton = this.FindAndResolveComponent<Button>("TuCosto<Button>", DisplayObject);
		tuCostoMButton.onClick.AddListener(delegate { 
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.ClearSession();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_COSTO;
			SessionManager.Instance.OpenActivity(PaymentSchemeSelection.Instance); Terminate(); });
    }

    protected override void ProcessUpdate()
    {
        
    }

    protected override void ProcessTermination()
    {
        Debug.Log("Home Activity => Terminated");
    }

    public void StartActivity()
    {
        Initialize();
    }

    public void EndActivity(bool force = false)
    {
        Terminate(force);
    }
}
