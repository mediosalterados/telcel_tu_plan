using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using System;

using TuPlan.Controllers.DataManagement;

public class MaxSinLimitePlanAdjustment : UIModule<MaxSinLimitePlanAdjustment>, IActivity
{
	public enum PlanAdjustmentModeValues {
		BY_MB = 0,
		BY_COST = 1
	}



	// Currently only the Cost mode is available so we initialize it by default
	public PlanAdjustmentModeValues AdjustmentMode = PlanAdjustmentModeValues.BY_COST;

	List<Plans> _maxSinLimitePlans = new List<Plans>();
	Plans _maxSinLimiteSelectedPlan;


	// Cost Mode components
	Text _planCost;
	Slider _planMBSlider;
	Text _MBValue;

	// Shared components between adjustment modes
	//Text _expiration;
	Image _twitterIcon;
	Image _facebookIcon;
	Image _faceMessengerIcon;
	Image _whatsAppIcon;
	Image _snapchatAppIcon;
	Image _instagramAppIcon;
	Image _uberIcon;
	Text _clauses;
	Button _continueButton;
	GameObject _FeatureB;
	GameObject _FeatureBB;
	Text _smsText;
	Text _minText;
	Text _promotionsText;
	Text _speedText;
	Text textA;
	Text _speedValue;
	GameObject SocialRow;

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.ACTIVITY_MAXSINLIMITE_PLANADJUSTMENT_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.MIDDLE;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);
		}
	}

	public bool isActivityReady
	{
		get
		{
			if (UIModuleCurrentStatus == UIModuleStatusModes.STAND_BY)
				return true;
			else
				return false;
		}
	}

	protected override void ProcessInitialization()
	{


		Debug.Log ("Tipo de plan seleccionado " +SessionManager.Instance.CurrentSessionData.SelectedPlanType);

		AdjustmentMode = SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.TU_PLAN || SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO ? PlanAdjustmentModeValues.BY_MB : PlanAdjustmentModeValues.BY_COST;

		_maxSinLimitePlans = SessionManager.Instance.CurrentSessionData.SelectedDeviceId != null ?  DatabaseManager.Instance.GetAllPlansByTypeAndByDevice(SessionManager.Instance.CurrentSessionData.SelectedPlanType,DataManager.Instance.DataVersion,SessionManager.Instance.CurrentSessionData.SelectedDeviceId,DatabaseManager.Instance.localDBConnection) : DatabaseManager.Instance.GetAllPlansByType(SessionManager.Instance.CurrentSessionData.SelectedPlanType,DataManager.Instance.DataVersion,DatabaseManager.Instance.localDBConnection);

		Text _buttonContinue = this.FindAndResolveComponent<Text>("Continue<Text>", DisplayObject);

		_buttonContinue.text = SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO ? "IR A RESUMEN":"IR A SELECCIÓN DE EQUIPO";


		// Get shared component references
		//_expiration = this.FindAndResolveComponent<Text>("ExpirationText<Text>", DisplayObject);
		_twitterIcon = this.FindAndResolveComponent<Image>("Twitter<Image>", DisplayObject);
		_facebookIcon = this.FindAndResolveComponent<Image>("Facebook<Image>", DisplayObject);
		_faceMessengerIcon = this.FindAndResolveComponent<Image>("FaceMessenger<Image>", DisplayObject);
		_whatsAppIcon = this.FindAndResolveComponent<Image>("Whatsapp<Image>", DisplayObject);
		_snapchatAppIcon = this.FindAndResolveComponent<Image>("SnapChat<Image>", DisplayObject);
		_instagramAppIcon = this.FindAndResolveComponent<Image>("Instagram<Image>", DisplayObject);
		_uberIcon = this.FindAndResolveComponent<Image>("Uber<Image>", DisplayObject);
		_clauses = this.FindAndResolveComponent<Text>("Clauses<Text>", DisplayObject);
		_FeatureB = this.Find ("FeatureB<Wrapper>",DisplayObject);
		_FeatureBB = this.Find ("FeatureBB<Wrapper>",DisplayObject);
		_smsText = this.FindAndResolveComponent<Text>("SMS<Text>", DisplayObject);
		_minText = this.FindAndResolveComponent<Text>("Minutos<Text>", DisplayObject);
		_promotionsText = this.FindAndResolveComponent<Text>("Promo<Text>", DisplayObject);
		_speedText = this.FindAndResolveComponent<Text>("TextC<Text>", DisplayObject);
		textA = this.FindAndResolveComponent<Text>("TextA<Text>", DisplayObject);
		_speedValue = this.FindAndResolveComponent<Text>("TextB<Text>", DisplayObject);
		SocialRow = this.Find ("RowC<Wrapper>",DisplayObject);
		
		_speedText.gameObject.SetActive(false);


		_continueButton = this.FindAndResolveComponent<Button>("Continue<Button>", DisplayObject);
		_continueButton.onClick.AddListener(delegate {
			SessionManager.Instance.CurrentSessionData.SelectedPlanId = _maxSinLimiteSelectedPlan.plan_id.ToString();

			if(SessionManager.Instance.CurrentSessionData.SelectedInitialStep == SessionData.InitialStepsValues.EQUIPO){
				SessionManager.Instance.OpenActivity(MaxSinLimiteQuotationResult.Instance);
			}else{
				SessionManager.Instance.OpenActivity(DeviceMainFilter.Instance);
			}
			Terminate();            
		});        
			

		if (AdjustmentMode == PlanAdjustmentModeValues.BY_COST) {
			Text titleValueText = this.FindAndResolveComponent<Text> ("TitleText<Text>", DisplayObject);
			titleValueText.text = "Plan por costo mensual";
			// Get lowest value
			int lowestCost = int.MaxValue;
			foreach (Plans amigoPlan in _maxSinLimitePlans) {
				int amigoCost = int.Parse (amigoPlan.monthly_cost);
				if (amigoCost < lowestCost)
					lowestCost = amigoCost;
			}

			Text minValueText = this.FindAndResolveComponent<Text> ("MinValue<Text>", DisplayObject);
			minValueText.text = "$" + lowestCost.ToString ();

			// Get max value
			int maxCost = int.MinValue;

			foreach (Plans amigoPlan in _maxSinLimitePlans) {
				int amigoCost = int.Parse (amigoPlan.monthly_cost);
				if (amigoCost > maxCost)
					maxCost = amigoCost;
			}

			Text maxValueText = this.FindAndResolveComponent<Text> ("MaxValue<Text>", DisplayObject);
			maxValueText.text = "$" + maxCost.ToString ();

			_planCost = this.FindAndResolveComponent<Text> ("PlanCost<Text>", DisplayObject);
			_planMBSlider = this.FindAndResolveComponent<Slider> ("PlanMB<Slider>", DisplayObject);
			_MBValue = this.FindAndResolveComponent<Text> ("MBValue<Text>", DisplayObject);

			// Set maximum MB value
			int maxMBValue = int.MinValue;

			foreach (Plans amigoPlan in _maxSinLimitePlans) {
				int amigoMB = amigoPlan.megabytes_inc;

				if (amigoMB > maxMBValue)
					maxMBValue = amigoMB;
			}
			_planMBSlider.maxValue = maxMBValue;
			_planMBSlider.enabled = false;

			Slider costSlider = this.FindAndResolveComponent<Slider> ("PlanAdjust<Slider>", DisplayObject);
			costSlider.maxValue = _maxSinLimitePlans.Count - 1;
			costSlider.onValueChanged.AddListener (delegate (float value) {
				OnCostSliderValueChange (value);
			});
			costSlider.value = 0;
			OnCostSliderValueChange (costSlider.value);

		} else {
			Text titleValueText = this.FindAndResolveComponent<Text> ("TitleText<Text>", DisplayObject);
			titleValueText.text = "Plan por megabytes";  


			// Get lowest value
			int lowestCost = int.MaxValue;

			foreach (Plans amigoPlan in _maxSinLimitePlans) {

				int amigoCost = int.Parse (amigoPlan.monthly_cost);

				if (amigoCost < lowestCost)
					lowestCost = amigoCost;
			}



			// Get max value
			int maxCost = int.MinValue;

			foreach (Plans amigoPlan in _maxSinLimitePlans) {

				int amigoCost = int.Parse (amigoPlan.monthly_cost);

				if (amigoCost > maxCost)
					maxCost = amigoCost;
			}


			_planCost = this.FindAndResolveComponent<Text> ("PlanCost<Text>", DisplayObject);
			_planMBSlider = this.FindAndResolveComponent<Slider> ("PlanMB<Slider>", DisplayObject);
			_MBValue = this.FindAndResolveComponent<Text> ("MBValue<Text>", DisplayObject);

			// Set maximum MB value
			int maxMBValue = int.MinValue;

			foreach (Plans amigoPlan in _maxSinLimitePlans) {
				int amigoMB = amigoPlan.megabytes_inc;

				if (amigoMB > maxMBValue)
					maxMBValue = amigoMB;
			}
			_planMBSlider.maxValue = maxMBValue;
			_planMBSlider.enabled = false;


			// Get lowest value
			int lowMBValue = int.MaxValue;

			foreach (Plans plan in _maxSinLimitePlans) {
				int planMB = plan.megabytes_inc;
				if (planMB < lowMBValue)
					lowMBValue = planMB;
			}

			Text minValueText = this.FindAndResolveComponent<Text> ("MinValue<Text>", DisplayObject);
			minValueText.text =  lowMBValue.ToString ("N0");

			Text maxValueText = this.FindAndResolveComponent<Text> ("MaxValue<Text>", DisplayObject);
			maxValueText.text = maxMBValue.ToString ("N0");


			Slider costSlider = this.FindAndResolveComponent<Slider> ("PlanAdjust<Slider>", DisplayObject);
			costSlider.maxValue = _maxSinLimitePlans.Count - 1;
			costSlider.onValueChanged.AddListener (delegate (float value) {
				OnCostSliderValueChange(value);
			});
			costSlider.value = 0;
			OnCostSliderValueChange (costSlider.value);

		}



		if (AdjustmentMode == PlanAdjustmentModeValues.BY_MB) {
			SessionManager.Instance.CurrentSessionData.byMb = true;
		} else {
			SessionManager.Instance.CurrentSessionData.byMb = false;
		}

		// INTERNET EN TU CASA
	 	if(SessionManager.Instance.CurrentSessionData.SelectedPlanType == "INT_HOME"){
	 		_MBValue.text = "ilimitados";
	 		textA.gameObject.SetActive(false);
	 		_speedText.gameObject.SetActive(true);
	 		SocialRow.SetActive(false);
	 		_FeatureBB.SetActive(false);

	 		foreach(Plans item in _maxSinLimitePlans){
	 			Debug.Log("Mins: "+item.minutes_inc);
	 		}
		}

	

	}

	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

	public void StartActivity()
	{
		Initialize();
	}

	public void EndActivity(bool force = false)
	{
		Terminate(force);
	}

	public void OnCostSliderValueChange(float value) {

	

		if (_maxSinLimitePlans.Count < 1)
			return;

		_maxSinLimiteSelectedPlan = _maxSinLimitePlans[(int)value];
		Debug.Log ("Tipo de plan "+_maxSinLimiteSelectedPlan.tipo_plan);
		if (_maxSinLimitePlans != null) {
			
			_planCost.text = string.Format("{0:#,###}", Convert.ToDecimal(_maxSinLimiteSelectedPlan.monthly_cost));

			_planMBSlider.value = _maxSinLimiteSelectedPlan.megabytes_inc;
			String extra_mb = "1,000 MB";
			//Verifico que el plan sea distinto a internet en tu casa
			if(_maxSinLimiteSelectedPlan.tipo_plan != "INT_HOME"){
				_MBValue.text = string.Format("{0:#,###}", Convert.ToDecimal(_maxSinLimiteSelectedPlan.megabytes_inc)) + DataMessages.GENERIC_PLAN_MB_LABEL_B;
				_whatsAppIcon.gameObject.SetActive(true);
				_twitterIcon.gameObject.SetActive(true);
				_facebookIcon.gameObject.SetActive(true);
				_faceMessengerIcon.gameObject.SetActive(true);
				_uberIcon.gameObject.SetActive (MaxSinLimiteQuotationResult.HasUber(_maxSinLimiteSelectedPlan));
			

				

				if (_maxSinLimiteSelectedPlan.megabytes_inc >= 2000 && _maxSinLimiteSelectedPlan.megabytes_inc < 8000 &&  _maxSinLimiteSelectedPlan.tipo_plan != "MAX") {
					extra_mb = "2,000 MB";
				} else if (_maxSinLimiteSelectedPlan.megabytes_inc >= 8000 &&  _maxSinLimiteSelectedPlan.tipo_plan != "MAX") {
					extra_mb = "3,000 MB";
				}

				if (_maxSinLimiteSelectedPlan.tipo_plan != "MAX") {
					_clauses.text = _maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL" ? DataMessages.MAX_SIN_LIMITE_MB_CLAUSE : DataMessages.MAX_SIN_LIMITE_PYME_MB_CLAUSE;
				} else {
					_clauses.text = DataMessages.MAX_SIN_LIMITE_MB_CLAUSE;
				}

				if (_maxSinLimiteSelectedPlan.megabytes_inc > 2000) {
					_instagramAppIcon.gameObject.SetActive (true);
					_snapchatAppIcon.gameObject.SetActive (true);
				} else {
					_instagramAppIcon.gameObject.SetActive (false);
					_snapchatAppIcon.gameObject.SetActive (false);
					_uberIcon.gameObject.SetActive (false);
				}

			

			if ((_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL_PYME" && _maxSinLimiteSelectedPlan.megabytes_inc > 1000) || (_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL" && _maxSinLimiteSelectedPlan.megabytes_inc >= 2000)) {
				//PRENDER SNAPCHAT & INSTAGRAM
			
//				_clauses.text = _clauses.text.Replace ("*Redes sociales sin costo los primeros {MB_DATA}\n", "");
				_clauses.text = DataMessages.MAX_SIN_LIMITE_MB_SC_CLAUSE;
			}

			_clauses.text = _clauses.text.Replace("{MB_DATA}",extra_mb);

			if (_maxSinLimiteSelectedPlan.minutes_inc == 1000000 && _maxSinLimiteSelectedPlan.sms_inc == 1000000) {
				_FeatureBB.active = false;
			} else {
				_FeatureB.active = false;
			}


			_smsText.text = "SMS incluidos: " + _maxSinLimiteSelectedPlan.sms_inc;
			_minText.text = "Minutos incluidos: " + _maxSinLimiteSelectedPlan.minutes_inc;

			}else if(_maxSinLimiteSelectedPlan.tipo_plan == "INT_HOME"){
				_clauses.text = DataMessages.INTERNET_HOME_CLAUSE;
				_speedValue.text = _maxSinLimiteSelectedPlan.minutes_inc.ToString() + " Mbps";
			}
		}

		if (MaxSinLimiteQuotationResult.HasUber (_maxSinLimiteSelectedPlan)) {
			if(_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL_PYME" && _maxSinLimiteSelectedPlan.megabytes_inc > 1000){
				
			}
			if (_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL_PYME") {
				if(_maxSinLimiteSelectedPlan.megabytes_inc > 3500){
					_clauses.text += "\n" + DataMessages.UBER_GENERIC_CLAUSE;	
				}
			} else {
				_clauses.text += "\n" + DataMessages.UBER_GENERIC_CLAUSE;
			}

		}


		_promotionsText.text = _maxSinLimiteSelectedPlan.legals.Replace("|",System.Environment.NewLine);

		//		***** Deshabilito promotionText que consume de DataMessages
//		if(_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL"){
//			if (_maxSinLimiteSelectedPlan.megabytes_inc >= 3000 && _maxSinLimiteSelectedPlan.megabytes_inc <= 12000) {
//				_promotionsText.text = DataMessages.PROMOTION_MB_PLAN;	
//			} else {
//				_promotionsText.text = "";	
//			}
//		}
//
//		if(_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL_PYME"){
//			if (_maxSinLimiteSelectedPlan.megabytes_inc >= 3000) {
//				_promotionsText.text = _maxSinLimiteSelectedPlan.legals;
//			} else {
//				_promotionsText.text = "";	
//			}
//		}


//		if (_maxSinLimiteSelectedPlan.tipo_plan == "MAX" || _maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL") {
//			_promotionsText.text = DataMessages.PROMOTION_MB_PLAN;
//		}else if(_maxSinLimiteSelectedPlan.tipo_plan == "MAX_PYME"){
//			_promotionsText.text = DataMessages.PROMOTION_SMS_PYME;
//		} else {
//			_promotionsText.text = "";
//		}

		if (_maxSinLimiteSelectedPlan.tipo_plan == "MAX_SL_PYME") {
			_instagramAppIcon.gameObject.SetActive (false);
			_snapchatAppIcon.gameObject.SetActive(false);
			_faceMessengerIcon.gameObject.SetActive (false);
			if (_maxSinLimiteSelectedPlan.megabytes_inc < 5000) {
				_uberIcon.gameObject.SetActive (false);	
			} else {
				_uberIcon.gameObject.SetActive (true);	
			}
		}

	}
}
