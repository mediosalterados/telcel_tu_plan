﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

public class MainMenu : UIModule<MainMenu> {

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.FRAGMENT_MAINMENU_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.MIDDLE;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);
            
        }
    }

    protected override void ProcessInitialization()
    {
        Debug.Log("MainMenu => Started");

        Button backButton = this.FindAndResolveComponent<Button>("Back<Button>", DisplayObject);
        backButton.onClick.AddListener(delegate { SessionManager.Instance.GoToPreviousActivity(); });

        Button menuButton = this.FindAndResolveComponent<Button>("Menu<Button>", DisplayObject);
        menuButton.onClick.AddListener(delegate { SideMenu.Instance.Initialize(); });
                
    }

    protected override void ProcessUpdate()
    {

    }

    protected override void ProcessTermination()
    {
        Debug.Log("MainMenu => Terminated");
    }

}
