﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;

using VoxelBusters.NativePlugins;
using VoxelBusters.Utility;

using System;

public class LegalesExpanded : UIModule<LegalesExpanded>
{
	

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.FRAGMENT_LEGALES_EXPANDED_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.TOP;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);

		}
	}

	protected override void ProcessInitialization()
	{
		
		Button exitTrigger = this.FindAndResolveComponent<Button>("ExitTrigger<Button>", DisplayObject);
		exitTrigger.onClick.AddListener(delegate { Terminate(); });

		Text legalesText = this.FindAndResolveComponent<Text> ("Text", DisplayObject);
		legalesText.text = DataMessages.MAX_SIN_LIMITE_EXPANDED_CLAUSE;

	}



	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

}
