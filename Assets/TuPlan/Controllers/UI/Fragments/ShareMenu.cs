﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;

using VoxelBusters.NativePlugins;
using VoxelBusters.Utility;

using System;

public class ShareMenu : UIModule<ShareMenu>
{
	private 	eShareOptions[]	m_excludedOptions	= new eShareOptions[0];
	String messageShare = "Tu Plan";

	protected override Transform DisplayObjectParent
	{
		get
		{
			return null;
		}
	}

	protected override string DisplayObjectPath
	{
		get
		{
			return DataPaths.FRAGMENT_SHARE_MENU_LAYOUT_PATH;
		}
	}

	protected override Vector2? DisplayObjectPosition
	{
		get
		{
			return null;
		}
	}

	protected override int DisplayObjectZIndex
	{
		get
		{
			return (int)ZIndexPlacement.TOP;
		}
	}

	protected override Transition InOutTransition
	{
		get
		{
			return new Transition(Transition.InOutAnimations.NONE);

		}
	}

	protected override void ProcessInitialization()
	{
		Button exitTrigger = this.FindAndResolveComponent<Button>("ExitTrigger<Button>", DisplayObject);
		exitTrigger.onClick.AddListener(delegate { Terminate(); });


		Button whatsappButton = this.FindAndResolveComponent<Button>("WhatsApp<Button>", DisplayObject);
		whatsappButton.onClick.AddListener(delegate {
			this.messageShare = SessionManager.Instance.CurrentSessionData.ShareEmail;
			ShareTextMessageOnWhatsApp();
			Terminate(); 
		});


		Button emailButton = this.FindAndResolveComponent<Button>("Email<Button>", DisplayObject);
		emailButton.onClick.AddListener(delegate {
			this.messageShare = SessionManager.Instance.CurrentSessionData.ShareEmail;
			ShareTextMessageUsingEmail();
			Terminate(); 
		});


	}

	private void ShareTextMessageOnWhatsApp ()
	{
		// Create composer
		WhatsAppShareComposer _composer    = new WhatsAppShareComposer();
		_composer.Text                    = this.messageShare;

		// Show share view
		NPBinding.Sharing.ShowView(_composer, FinishedSharing);            
	}

	private void ShareTextMessageUsingEmail ()
	{

		Debug.Log ("Texto para compartir"+this.messageShare);

		MailShareComposer    _composer    = new MailShareComposer();
		_composer.Body                    = this.messageShare;
		_composer.IsHTMLBody            = false;
		NPBinding.Sharing.ShowView(_composer, FinishedSharing);
	}


	private void FinishedSharing (eShareResult _result)
	{
	}

	protected override void ProcessTermination()
	{

	}

	protected override void ProcessUpdate()
	{

	}

}
