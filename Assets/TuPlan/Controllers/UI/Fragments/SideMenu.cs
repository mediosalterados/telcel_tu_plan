﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using TuPlan.Controllers.DataManagement;
using System;

public class SideMenu : UIModule<SideMenu>
{

    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return DataPaths.FRAGMENT_SIDEMENU_LAYOUT_PATH;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.TOP;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
			return new Transition(Transition.InOutAnimations.NONE);

        }
    }

    protected override void ProcessInitialization()
    {
        Button exitTrigger = this.FindAndResolveComponent<Button>("ExitTrigger<Button>", DisplayObject);
        exitTrigger.onClick.AddListener(delegate { Terminate(); });

		/*
        Button aboutButton = this.FindAndResolveComponent<Button>("About<Button>", DisplayObject);
        aboutButton.onClick.AddListener(delegate { NotificationSystem.Instance.NotifyMessage(DataMessages.GENERIC_COMING_SOON); Terminate(); });
        */

        Button tuEquipoButton = this.FindAndResolveComponent<Button>("TuEquipo<Button>", DisplayObject);
        tuEquipoButton.onClick.AddListener(delegate {
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.EQUIPO;
			if(SessionManager.Instance.IsCurrentlyOpened(DeviceMainFilter.Instance.name) == false){
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.SetHomeAsRoot();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.EQUIPO;
            SessionManager.Instance.OpenActivity(DeviceMainFilter.Instance);
			}
			Terminate(); 
	
		});

        Button tuPlanButton = this.FindAndResolveComponent<Button>("TuPlan<Button>", DisplayObject);
        tuPlanButton.onClick.AddListener(delegate { 
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_PLAN;
			if(SessionManager.Instance.IsCurrentlyOpened(PaymentSchemeSelection.Instance.name) == false){
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.SetHomeAsRoot();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_PLAN;
			SessionManager.Instance.OpenActivity(PaymentSchemeSelection.Instance);
			}
			Terminate(); 
		});

        Button tusPromosButton = this.FindAndResolveComponent<Button>("TusPromociones<Button>", DisplayObject);
		tusPromosButton.onClick.AddListener(delegate { 
			if(SessionManager.Instance.IsCurrentlyOpened(PromotionsScreen.Instance.name) == false){
				SessionManager.Instance.TryShuttingDownAllOpenedActivities();
				SessionManager.Instance.SetHomeAsRoot();
				SessionManager.Instance.OpenActivity(PromotionsScreen.Instance);  
			}
			Terminate(); 
		});

        Button tuCostoMButton = this.FindAndResolveComponent<Button>("TuCostoMensual<Button>", DisplayObject);
        tuCostoMButton.onClick.AddListener(delegate { 
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_COSTO;
			if(SessionManager.Instance.IsCurrentlyOpened(PaymentSchemeSelection.Instance.name) == false){
			SessionManager.Instance.TryShuttingDownAllOpenedActivities();
			SessionManager.Instance.SetHomeAsRoot();
			SessionManager.Instance.CurrentSessionData.SelectedInitialStep = SessionData.InitialStepsValues.TU_COSTO;
			SessionManager.Instance.OpenActivity(PaymentSchemeSelection.Instance);
			}
			Terminate(); 
		});


		Button infoButton = this.FindAndResolveComponent<Button>("Info<Button>", DisplayObject);
		infoButton.onClick.AddListener(delegate { 
			
			if(SessionManager.Instance.IsCurrentlyOpened(Info.Instance.name) == false){
				SessionManager.Instance.TryShuttingDownAllOpenedActivities();
				SessionManager.Instance.SetHomeAsRoot();
				SessionManager.Instance.OpenActivity(Info.Instance);
			}
			Terminate(); 
		});

		Button updateButton = this.FindAndResolveComponent<Button>("Update<Button>", DisplayObject);
		updateButton.onClick.AddListener(delegate { 
			PlayerPrefs.DeleteAll();
			DataManager.Instance.ForceUpdate();
			DataManager.Instance.setUpdated();
			Terminate();
		});

    }

    protected override void ProcessTermination()
    {
        
    }

    protected override void ProcessUpdate()
    {
        
    }

}
