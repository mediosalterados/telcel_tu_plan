﻿using UnityEngine;
using System.Collections;

using GLIB.Utils;

using TuPlan.Controllers.DataManagement;

public class MainApp : MonoBehaviour {

	public GoogleAnalyticsV3 googleAnalytics;

	// Use this for initialization
	void Start () {        	




		googleAnalytics.StartSession ();
		SessionManager.Instance.googleAnalytics = googleAnalytics;

        NotificationSystem.Instance.TemplatePath = DataPaths.FRAGMENT_GRIMOIRECUSTOMNOTIFICATION_LAYOUT_PATH;

        Screen.fullScreen = false;
        Application.targetFrameRate = 60;

		int first_time = PlayerPrefs.GetInt("first_time", 0);
		int version = PlayerPrefs.GetInt("version_1_4", 0);
		if (first_time == 1 && version == 1) {
			DataManager.Instance.Initialize ();
			SessionManager.Instance.Initialize ();
			SessionManager.Instance.OpenActivity (Home.Instance);
			MainMenu.Instance.Initialize ();
		}else if (first_time == 1 && version == 0) {
			PlayerPrefs.DeleteAll();
			DataManager.Instance.Initialize ();
			SessionManager.Instance.Initialize ();
			SessionManager.Instance.OpenActivity (Home.Instance);
			MainMenu.Instance.Initialize ();
			PlayerPrefs.SetInt("first_time", 1);
			PlayerPrefs.SetInt("version_1_4", 1);
		} else {
			SessionManager.Instance.Initialize ();
			SessionManager.Instance.OpenActivity (PassCode.Instance);
		}

        if (Application.isMobilePlatform)
            KiiPushService.Instance.Initialize();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.C))
        {
            PlayerPrefs.DeleteAll();
            Debug.LogError("PLAYER PREFS CLEARED");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SessionManager.Instance.GoToPreviousActivity();
        }


#if DEUR
        //Debug.Log("Doing DEUR");
#elif R9
        //Debug.Log("Doing R9");
#endif

    }
}
